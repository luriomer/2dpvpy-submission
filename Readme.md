Omer Luria  
Tel-Aviv University  
[luriomer@gmail.com  ](mailto:luriomer@gmail.com  )  

2DPVpy
------

A simulation and data analysis software for photovoltaics with 2D materials.
This software was developed as part of my M.Sc research, summarized in a thesis named  
*"Photovoltaics with 2D Materials: Optoelectronic Aspects of Large Scale MoS2/Si heterojunctions"*.  
(under the supervision of Prof. Abraham Kribus and Dr. Ariel Ismach, Faculty of Engineering, Tel-Aviv University)  

The thesis text is available in the PDF or via  
https://drive.google.com/file/d/1-kLz4P4Ft-2uuTP-9p8omZFGYyrfXHas/view?usp=sharing
  
Details concerning the use of the package can be found in section 3.7 *'2DPVpy: simulation and data analysis using Python'*.
  
The example files are attached in analysis/ and can be run using any python IDE (Spyder is recommended).
  
Prerequisites (and the versions tested):  
Python3  (v3.7.6)  
numpy (v1.18.1)  
scipy (v1.4.1)  
matplotlib (v3.1.3)  
Lmfit (v1.0.1)  

How to cite this work:
Luria, O., 2DPVpy, 2020, https://bitbucket.org/luriomer/2dpvpy-submission/src/master/