#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 14:57:20 2018

@author: omer
"""

from core.importer import *
sys.path.insert(0,'../..')


plt.close("all")
print str(time.ctime())+"\n"
#%% Global parameters

name = "grid_patterened_Ti_coating" #name of the sample
lmd = np.arange(220,820,1.0,dtype=float) # spectral range [nm]


AoIs = [70,75,80,85]


#%% Preparation of participating instances

# exps
exp = exp_cls(name="exp")
exp.add_ellipso_data(name+"_ellipso.dat")

# layers
Void = layer_cls(name="Void" , lmd=lmd , d=11.8203224 , disp_type="Sum" , disp_params={"Baseline":1-0j} , disp_vary=False)

Si = layer_cls(name="Si", lmd=lmd , d=0.3e6 , disp_type="Sum" , 
               disp_params={"Tab":"../../data/Si/c-Si_bulk_Aspnes1983.txt"} , disp_vary=False)

SiO2 = layer_cls(name="SiO2", lmd=lmd , d=1.44789174 , disp_type="Sum" , 
                 disp_params={"Tab":"../../data/SiO2/SiO2_thinfilm_Rodriguez2016.txt"} , disp_vary=False)

Si_interface = layer_cls(name="Si_interface",lmd=lmd,d=0.1,disp_type="EMA Bruggeman",disp_params={Si:0.5,SiO2:0.5},mu=1.0)

Ti = layer_cls(name="Ti", lmd=lmd , d=1.53 , disp_type="Sum" , 
                 disp_params={"Tab":"../../data/Ti/Ti_bulk_JohnsonChristy1974.txt"} , disp_vary=False)

TiO2 = layer_cls(name="TiO2", lmd=lmd , d=4.85 , disp_type="Sum" , 
                 disp_params={"Tab":"../../data/TiO2/TiO2_thinfilm_Siefke2016.txt"} , disp_vary=False)

Mixture = layer_cls(name="Mixture",lmd=lmd,d=6.08381925,disp_type="EMA General",disp_params={Ti:0.93062875,TiO2:0.06937125},mu=0.09644850)

Roughness = layer_cls(name="Roughness",lmd=lmd,d=5.73650314,disp_type="EMA General",disp_params={TiO2:0.5,Void:0.5},mu=0.99957438)


# stacks
stack1 = stack_cls(name="stack1",layers=[Void,Roughness,Mixture,SiO2,Si],lmd=lmd,AoIs=AoIs)
stack2 = stack_cls(name="stack2",layers=[Void,Void,SiO2,Si],lmd=lmd,AoIs=AoIs)
# model
model = model_cls(name="model",lmd=lmd,AoIs=AoIs,stacks={stack1:0.77190272,stack2:0.22809728})
model.calc_all()
#%% Fit recipe

model.fit_method = "leastsq"
model.logging = False
model.exp = exp


# Specify what attributes to fit. Use model.add_parameter(iden_dic), where
# iden_dic is in the form of {"obj":,"attr":,"min":,"max":,"constr":}
# note that sometimes optimizers don't like zero min due to NaNs, so we set some small positive value as a threshold.



model.add_parameter({"obj":Roughness,"attr":"d","min":0.0001,"max":1000.0})
model.add_parameter({"obj":Roughness,"attr":"mu","min":0.0001,"max":1.0})

model.add_parameter({"obj":Mixture,"attr":"d","min":0.0001,"max":1000.0})
model.add_parameter({"obj":Mixture,"attr":"mu","min":0.0,"max":1.0})
model.add_parameter({"obj":Mixture,"attr":"f_Ti","min":0.0,"max":1.0})
model.add_parameter({"obj":Mixture,"attr":"f_TiO2","min":0.0,"max":1.0,"constr":"1-Mixture_f_Ti"})

model.add_parameter({"obj":model,"attr":"S_stack1","min":0.0001,"max":1.0})
model.add_parameter({"obj":model,"attr":"S_stack2","constr":"1-model_S_stack1"})

model.add_parameter({"obj":Void,"attr":"d","constr":"Roughness_d+Mixture_d"})

# Figure of Merit type - MSE or CHI2
model.FOM_type = "CHI2"
model.data_types_to_fit = ["Ellipsometry"]

#PbP options
model.PbP_layer = Ti
model.PbP_start_from = "First"

#leastsq minimizer options
model.local_maxfev = 100000 #number of iterations for leastsq minimizer. default is 100
model.ftol = 1.0e-6 # tolerance of MSE value. default is 1.0e-6, but can be tuned depending on MSE value.
model.epsfcn = 1.49-8 #stepsize for local minimization, defulat is 1.49e-8

#basinhopping minimizer options. 
model.local_basin_method = "Nelder-Mead"
model.global_niter = 100 #number of total passes for basinhopping, default is 100
model.Tmprtr = 1.0 #Annealing temperature for random jump, default is 1.0
model.stepsize = 0.5 #random jump for global basinhopping, default is 0.5

#Brute minimizer options
model.brute_Ns = 1000

# Fitting call
model.load_fit_recipe()
model.fit()
model.calc_all()
#%% Results
plt.close("all")
plot_optical_config(model)
ellipsoplot = plot_ellipso(model,exp,selected_AoIs=AoIs,horunits="eV",alpha=1.0)
fluxplot = plot_flux(model)
plot_dispersion(Ti,TiO2,Mixture,Roughness)
print_all_messages()