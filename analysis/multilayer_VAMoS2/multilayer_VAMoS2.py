#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 14:57:20 2018

@author: omer
"""

from core.importer import *
sys.path.insert(0,'../..')


plt.close("all")
print str(time.ctime())+"\n"
#%% Global parameters

name = "multilayer_VAMoS2" #name of the sample
lmd = np.arange(300,780,1.0,dtype=float) # spectral range [nm]


AoIs = [0,60,65,70,75,80]


#%% Preparation of participating instances

# exps
exp = exp_cls(name="exp")
exp.add_ellipso_data(name+"_ellipso.dat")

# layers
Void = layer_cls(name="Void" , lmd=lmd , d=1.0 , disp_type="Sum" , disp_params={"Baseline":1-0j} , disp_vary=False)

Si = layer_cls(name="Si", lmd=lmd , d=0.3e6 , disp_type="Sum" , 
               disp_params={"Tab":"../../data/Si/c-Si_bulk_Aspnes1983.txt"} , disp_vary=False)

SiO2 = layer_cls(name="SiO2", lmd=lmd , d=7.38875701 , disp_type="Sum" , 
                 disp_params={"Tab":"../../data/SiO2/SiO2_thinfilm_Rodriguez2016.txt"} , disp_vary=False)

Mo = layer_cls(name="Mo", lmd=lmd , d=1.99409671 , disp_type="Sum" , 
                 disp_params={"Tab":"../../data/Mo/Mo_bulk_Querry1987.txt"} , disp_vary=False)


MoS2_bulk_BealHuges1979 = layer_cls(name="MoS2_bulk_BealHuges1979",lmd=lmd,d=63.5,disp_type="Sum",
                                    disp_params={"Tab":"../../data/MoS2/MoS2_bulk_BealHuges1979.txt"},disp_vary=False)

MoS2_PbP = layer_cls(name="MoS2_PbP",lmd=np.arange(300,760,10.0),d=57.60,disp_type="Sum",
                                     disp_params={"Tab":"multilayer_VAMoS2_MoS2_PbP_N.txt"},disp_vary=False)

MoS2 = layer_cls(name="MoS2",lmd=lmd ,d=33.3846347,disp_type="Sum" ,
                 disp_params={"Baseline":2.31249072-0j,
                              "Lorentz_1":(1.83993028,1.38529398,0.10813590),
                              "Lorentz_2":(1.98939249,1.80471593,0.20240289),
                              "Lorentz_3":(2.19054994,0.88832751,0.58105342),
                              "Lorentz_4":(2.69949773,6.80913884,0.62708363),
                              "Lorentz_5":(3.05453925,2.97601360,0.68858143),
                              "Lorentz_6":(4.78512823,5.87308123,2.99520415)
                              })

Roughness = layer_cls(name="Roughness",lmd=lmd,d=1.0,disp_type="EMA Bruggeman",disp_params={MoS2:0.5,Void:0.5},mu=1.0)

Interface = layer_cls(name="Interface",lmd=lmd,d=8.15735461,disp_type="EMA General",disp_params={Si:0.61994390,SiO2:0.38005610},mu=0.04099572)

# stacks
stack1 = stack_cls(name="stack1",layers=[Void,MoS2,SiO2,Interface,Si],lmd=lmd,AoIs=AoIs)
# model
model = model_cls(name="model",lmd=lmd,AoIs=AoIs,stacks={stack1:1.0})
model.calc_all()
#%% Fit recipe

model.fit_method = "dryrun"
model.logging = False
model.exp = exp


# Specify what attributes to fit. Use model.add_parameter(iden_dic), where
# iden_dic is in the form of {"obj":,"attr":,"min":,"max":,"constr":}
# note that sometimes optimizers don't like zero min due to NaNs, so we set some small positive value as a threshold.

model.add_parameter({"obj":MoS2,"attr":"d","min":0.01})

model.add_parameter({"obj":SiO2,"attr":"d","min":0.01})

model.add_parameter({"obj":Interface,"attr":"d","min":0.01})
model.add_parameter({"obj":Interface,"attr":"mu","min":0.0,"max":1.0})
model.add_parameter({"obj":Interface,"attr":"f_Si","min":0.0,"max":1.0})
model.add_parameter({"obj":Interface,"attr":"f_SiO2","min":0.0,"max":1.0,"constr":"1-Interface_f_Si"})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_1_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_1_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_1_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_2_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_2_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_2_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_3_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_3_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_3_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_4_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_4_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_4_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_5_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_5_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_5_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_6_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_6_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_6_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Baseline_Re","min":1.0})

# Figure of Merit type - MSE or CHI2
model.FOM_type = "CHI2"
model.data_types_to_fit = ["Ellipsometry"]

#PbP options
model.PbP_layer = MoS2
model.PbP_start_from = "First"

#leastsq minimizer options
model.local_maxfev = 1000000 #number of iterations for leastsq minimizer. default is 100
model.ftol = 1.0e-4 # tolerance of MSE value. default is 1.0e-6, but can be tuned depending on MSE value.
model.epsfcn = 1.49-8 #stepsize for local minimization, defulat is 1.49e-8

#basinhopping minimizer options. 
model.local_basin_method = "Nelder-Mead"
model.global_niter = 100 #number of total passes for basinhopping, default is 100
model.Tmprtr = 1.0 #Annealing temperature for random jump, default is 1.0
model.stepsize = 0.5 #random jump for global basinhopping, default is 0.5

#Brute minimizer options
model.brute_Ns = 1000

# Fitting call
model.load_fit_recipe()
model.fit()
model.calc_all()
#%% Results
model.calc_all()
plt.close("all")
plot_optical_config(model)
ellipsoplot = plot_ellipso(model,exp,selected_AoIs=AoIs,horunits="eV",alpha=1.0,markevery=2)
#fluxplot = plot_flux(model)
plot_dispersion(MoS2,MoS2_PbP)
plot_dispersion_components(MoS2)
#dispersionplot = plot_dispersion(MoS2_bulk_BealHuges1979,MoS2_PbP)
#oscplot = plot_dispersion_components(MoS2)
#taucplot = plot_Tauc(MoS2,m=2.0,reg_range=[1.8,1.83])
print_all_messages()