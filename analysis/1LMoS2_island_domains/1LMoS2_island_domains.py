#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 14:57:20 2018

@author: omer
"""

from core.importer import *
sys.path.insert(0,'../..')


plt.close("all")
print str(time.ctime())+"\n"
#%% Global parameters

name = "1LMoS2_island_domains" #name of the sample
lmd = np.arange(300,820,1.0,dtype=float) # spectral range [nm]


AoIs = [80]


#%% Preparation of participating instances

# exps
exp = exp_cls(name="exp")
exp.add_ellipso_data(name+"_ellipso.dat")
exp.add_reflect_data(name+"_reflect.dat")

# layers
Void = layer_cls(name="Void" , lmd=lmd , d=0.62857151 , disp_type="Sum" , disp_params={"Baseline":1-0j} , disp_vary=False)

Si = layer_cls(name="Si", lmd=lmd , d=1.0e6 , disp_type="Sum" , 
               disp_params={"Tab":"../../data/Si/c-Si_bulk_Aspnes1983.txt"} , disp_vary=False)

SiO2 = layer_cls(name="SiO2", lmd=lmd , d=302.28 , disp_type="Sum" , 
                 disp_params={"Tab":"../../data/SiO2/SiO2_thinfilm_Rodriguez2016.txt"} , disp_vary=False)

MoS2_bulk_BealHuges1979 = layer_cls(name="MoS2_bulk_BealHuges1979",lmd=np.arange(350,825,1),d=1.0,disp_type="Sum",
                                    disp_params={"Tab":"../../data/MoS2/MoS2_bulk_BealHuges1979.txt"},disp_vary=False)

MoS2_thinfilm2nm_Yim2014 = layer_cls(name="MoS2_thinfilm2nm_Yim2014",lmd=np.arange(383,825,1),d=1.0,disp_type="Sum",
                                     disp_params={"Tab":"../../data/MoS2/MoS2_thinfilm2nm_Yim2014.txt"},disp_vary=False)
MoS2_1L_Zhang2015 = layer_cls(name="MoS2_monolayer_Zhang2015",lmd=np.arange(400,750,1),d=1.0,disp_type="Sum",
                                     disp_params={"Tab":"../../data/MoS2/MoS2_1L_Zhang2015.txt"},disp_vary=False)

MoS2_1L_Ermolaev2020 = layer_cls(name="MoS2_1L_Ermolaev2020",lmd=np.arange(365,825,1),d=1.0,disp_type="Sum",
                                    disp_params={"Tab":"../../data/MoS2/MoS2_1L_Ermolaev2020.txt"},disp_vary=False)

MoS2_1L_Hsu2019 = layer_cls(name="MoS2_1L_Hsu2019",lmd=np.arange(397,825,1),d=1.0,disp_type="Sum",
                                    disp_params={"Tab":"../../data/MoS2/MoS2_1L_Hsu2019.txt"},disp_vary=False)

MoS2_2L_Hsu2019 = layer_cls(name="MoS2_2L_Hsu2019",lmd=np.arange(397,825,1),d=1.0,disp_type="Sum",
                                    disp_params={"Tab":"../../data/MoS2/MoS2_2L_Hsu2019.txt"},disp_vary=False)

MoS2_3L_Hsu2019 = layer_cls(name="MoS2_3L_Hsu2019",lmd=np.arange(397,825,1),d=1.0,disp_type="Sum",
                                    disp_params={"Tab":"../../data/MoS2/MoS2_3L_Hsu2019.txt"},disp_vary=False)


ref_layers = [MoS2_bulk_BealHuges1979,MoS2_thinfilm2nm_Yim2014,MoS2_1L_Ermolaev2020,MoS2_1L_Hsu2019,MoS2_2L_Hsu2019,MoS2_3L_Hsu2019]

MoS2 = layer_cls(name="MoS2",lmd=lmd ,d=0.62857151,disp_type="Sum" ,
                 disp_params={"Baseline":7.01419247-0j,
                              "Lorentz_1":(1.92000000,4.13728777,0.05000000),
                              "Lorentz_2":(2.05838772,10.1774136,0.15740943),
                              "Lorentz_3":(2.41734278,4.63272964,0.18347886),
                              "Lorentz_4":(2.60013054,4.34979139,0.31233556),
                              "Lorentz_5":(2.76251393,2.81090837,0.50690582)
                              })

# stacks
stack1 = stack_cls(name="stack1",layers=[Void,MoS2,SiO2,Si],lmd=lmd,AoIs=AoIs)
stack2 = stack_cls(name="stack2",layers=[Void,Void,SiO2,Si],lmd=lmd,AoIs=AoIs)
# model
model = model_cls(name="model",lmd=lmd,AoIs=AoIs,stacks={stack1:0.48330681,stack2:0.51669319})
model.calc_all()
#%% Fit recipe

model.fit_method = "dryrun"
model.logging = False
model.exp = exp


# Specify what attributes to fit. Use model.add_parameter(iden_dic), where
# iden_dic is in the form of {"obj":,"attr":,"min":,"max":,"constr":}
# note that sometimes optimizers don't like zero min due to NaNs, so we set some small positive value as a threshold.

model.add_parameter({"obj":MoS2,"attr":"d","min":0.2})
model.add_parameter({"obj":Void,"attr":"d","constr":"MoS2_d"})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_1_C","min":1.85,"max":1.92001})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_1_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_1_G","min":0.03,"max":0.05001})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_2_C","min":2.0,"max":2.08})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_2_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_2_G","min":0.03,"max":0.2})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_3_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_3_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_3_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_4_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_4_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_4_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Lorentz_5_C","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_5_A","min":0.0})
model.add_parameter({"obj":MoS2,"attr":"Lorentz_5_G","min":0.0})

model.add_parameter({"obj":MoS2,"attr":"Baseline_Re","min":1.0})

model.add_parameter({"obj":model,"attr":"S_stack1","min":0.0,"max":1.0})
model.add_parameter({"obj":model,"attr":"S_stack2","constr":"1-model_S_stack1"})

# Figure of Merit type - MSE or CHI2
model.FOM_type = "CHI2"
model.data_types_to_fit = ["Ellipsometry","Reflectance"]

#PbP options
model.PbP_layer = MoS2
model.PbP_start_from = "First"

#leastsq minimizer options
model.local_maxfev = 100000 #number of iterations for leastsq minimizer. default is 100
model.ftol = 1.0e-4 # tolerance of MSE value. default is 1.0e-6, but can be tuned depending on MSE value.
model.epsfcn = 1.49-8 #stepsize for local minimization, defulat is 1.49e-8

#basinhopping minimizer options. 
model.local_basin_method = "Nelder-Mead"
model.global_niter = 100 #number of total passes for basinhopping, default is 100
model.Tmprtr = 10.0 #Annealing temperature for random jump, default is 1.0
model.stepsize = 20.0 #random jump for global basinhopping, default is 0.5

#brute minimizer options
model.brute_Ns = 50 #number of grid points for each direction. default is 20


# Fitting call
model.load_fit_recipe()
model.fit()

#%% Results
plt.close("all")
model.calc_all()
plot_optical_config(model)
#ellipsoplot = plot_ellipso(model,exp,selected_AoIs=AoIs,horunits="eV",alpha=1.0)
#fluxplot = plot_flux(model,exp,selected_AoIs=AoIs,show_errorbars=True,markevery=3,errorevery=3,alpha=1.0)
#dispersionplot = plot_dispersion(MoS2,*ref_layers)
oscplot = plot_dispersion_components(MoS2)
taucplot = plot_Tauc(MoS2,m=0.5,reg_range=[1.9,1.915])
#step_plot = plot_Sigmoid(MoS2,reg_range=[1.52,1.925],form="arctan",show_reg_range=True,print_report=True)
plot_CP(MoS2,form="alpha",CP_guesses={3:[1.9,1.92,1.94,2.05,2.15,2.25,2.3,2.32,2.4,2.48,2.6,2.8],2:[1.91]},plot_limits=[1.89,1.93])
print_all_messages()