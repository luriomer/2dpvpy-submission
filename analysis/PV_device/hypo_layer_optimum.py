#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 12:57:07 2020

@author: omer
"""
from core.importer import *
sys.path.insert(0,'../..')

from scipy.optimize import fsolve
from scipy.special import lambertw
from scipy.interpolate import interp1d

global Imodel

plt.close("all")
plt.rcParams['font.family'] = 'serif'
plt.rcParams['mathtext.fontset'] = 'dejavuserif'
default_fontsize = 14
plt.rc('xtick',labelsize=default_fontsize)
plt.rc('ytick',labelsize=default_fontsize)

def single_solar_cell(V,Is,n,T,Rs,Rsh,Iph):
    ''' Analytical solution of I(V) for a single solar cell using the Lambert W function '''
    Vth = consts.k*T/consts.elementary_charge
    term1 = (Iph+Is-V/Rsh)/(1+Rs/Rsh)
    term2 = n*Vth/Rs
    term3 = (Is*Rs)/(n*Vth*(1+Rs/Rsh))
    term4 = V/(n*Vth) * (1-Rs/(Rs+Rsh))
    term5 = (Iph+Is)*Rs / (n*Vth*(1+Rs/Rsh))
    I = term1 - term2 * lambertw(term3 * np.exp(term4 + term5))
    #if any(np.imag(I)):
        #raise ValueError("complex values in single_solar_cell: check the range and paramters!")
    #else:
    I = np.real(I)
    return -I

#%% Global parameters

name = "PVdevice" #name of the sample
lmd = np.arange(280,4000,1.0,dtype=float) # spectral range [nm]
AoIs = [0]
Eg_MoS2 = 1.9 #eV
Eg_Si = 1.14 #eV
consistency_factor = 0.0775 # The ratio between the optical modeled photocurrent and the electrically measured one 

plot_reverse = True

area = 1.5e-05 # in cm^2
Vmodel = np.linspace(-6,6,100000)
ms = range(1,500,1)


#%% Preparing all needed instances
Pph_mp = np.array([])
Pd2_mp = np.array([])
Ps_mp = np.array([])
Psh_mp = np.array([])
P_mp = np.array([])

Isc = np.array([])
Voc = np.array([])
FF = np.array([])
PCE = np.array([])


incA_MoS2_useful = np.array([])
incA_Si_useful = np.array([])
incA_all_useful = np.array([])

phCnt_MoS2_suff = np.array([])
phCnt_Si_suff = np.array([])
phCnt_all_suff = np.array([])

J_ph = np.array([])
for m in ms:
    print "Running for "+str(m) + " layers:"
    #%% Preparation of participating instances
    
    # layers
    Void = layer_cls(name="Void" , lmd=lmd , d=1.0 , disp_type="Sum" , disp_params={"Baseline":1-0j} , disp_vary=False)
    
    Si = layer_cls(name="Si", lmd=lmd , d=1.0e6 , disp_type="Sum" , 
                   disp_params={"Tab":"../../data/Si/c-Si_bulk_Schinke2015_forsolar.txt"} , disp_vary=False)
    
    MgF2 = layer_cls(name="MgF2", lmd=lmd , d=75.0 , disp_type="Sum" , 
               disp_params={"Tab":"../../data/MgF2/MgF2_thinfilm_Rodriguez2017.txt"} , disp_vary=False)
    
    MoS2 = layer_cls(name="MoS2",lmd=lmd ,d=0.62857151*m,disp_type="Sum" ,
                     disp_params={"Baseline":7.01419247-0j,
                                  "Lorentz_1":(1.92000000,4.13728777,0.05000000),
                                  "Lorentz_2":(2.05838772,10.1774136,0.15740943),
                                  "Lorentz_3":(2.41734278,4.63272964,0.18347886),
                                  "Lorentz_4":(2.60013054,4.34979139,0.31233556),
                                  "Lorentz_5":(2.76251393,2.81090837,0.50690582)
                                  })
    
    # stacks
    device_stack = stack_cls(name="device_stack",layers=[Void,MgF2,MoS2,Si,Void],lmd=lmd,AoIs=AoIs)
    # model
    model = model_cls(name="model",lmd=lmd,AoIs=AoIs,stacks={device_stack:1.0})
    model.calc_all()
    
    #%% Use the incident spectrum and calculate the optical performance
    solar_data = np.loadtxt("solar_AM1.5G_global_tilt.txt")
    spectrum_wavelength_data = solar_data[:,0]
    spectrum_intensity_data = solar_data[:,1]
    
    
    # Interpolate and sample to match the spectrum
    incident_spectrum = interp1d(spectrum_wavelength_data,spectrum_intensity_data)(lmd)
    
    
    flux = 100.03693557817755 #mW/cm^2
    
    E_per_photon = (consts.h*1e3)*(consts.c*1e+9)/lmd # Energy of photon in units of mJ, as a fuction of wavelength
    
    
    
    incA = (device_stack.A_glob[0] * incident_spectrum)
    incR = (device_stack.R[0] * incident_spectrum)
    # Integrating the spectral fluxes to obtain the total and partial
    # fluxes and number of photons.
    # The integration must be with Gaussian quadrature of high order
    # since the data has noise and the function is not smooth. 
    # Simpler methods yeild poor results.
    integ_order = 1000
    incA_MoS2_total = (calc_integral(lmd,incA[1],lmd[0],lmd[-1],method="Gauss",order=integ_order))
    incA_MoS2_suff = (calc_integral(lmd,incA[1],lmd[0],eV_nm(Eg_MoS2),method="Gauss",order=integ_order))
    incA_Si_total = (calc_integral(lmd,incA[2],lmd[0],lmd[-1],method="Gauss",order=integ_order))
    incA_Si_suff = (calc_integral(lmd,incA[2],lmd[0],eV_nm(Eg_Si),method="Gauss",order=integ_order))
    incA_all_total = (incA_MoS2_total+incA_Si_total)
    incA_all_suff = (incA_MoS2_suff+incA_Si_suff)
    incR_total = (calc_integral(lmd,incR,lmd[0],lmd[-1]))
    
    # Photons
    phCnt_spectrum_total = (calc_integral(lmd,incident_spectrum/E_per_photon,lmd[0],lmd[-1]))
    phCnt_MoS2_total = (calc_integral(lmd,incA[2]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
    phCnt_MoS2_suff = np.append(phCnt_MoS2_suff,calc_integral(lmd,incA[2]/E_per_photon,lmd[0],eV_nm(Eg_MoS2),method="Gauss",order=integ_order))
    phCnt_Si_total = (calc_integral(lmd,incA[3]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
    phCnt_Si_suff = np.append(phCnt_Si_suff,calc_integral(lmd,incA[3]/E_per_photon,lmd[0],eV_nm(Eg_Si),method="Gauss",order=integ_order))
    phCnt_R_total = (calc_integral(lmd,incR/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
    phCnt_all_total = (phCnt_MoS2_total+phCnt_Si_total)
    phCnt_all_suff = np.append(phCnt_all_suff,phCnt_MoS2_suff[-1]+phCnt_Si_suff[-1])
    
    incA_MoS2_useful = np.append(incA_MoS2_useful,phCnt_MoS2_suff[-1]*Eg_MoS2*consts.elementary_charge*1e3)
    incA_Si_useful = np.append(incA_Si_useful,phCnt_Si_suff[-1]*Eg_Si*consts.elementary_charge*1e3)
    incA_all_useful = np.append(incA_all_useful,incA_MoS2_useful[-1]+incA_Si_useful[-1])
    
    J_ph = np.append(J_ph,phCnt_all_suff[-1] * consts.elementary_charge)
        
    
    #%% Calculate electrical response
    
    Is2 = 4.6370e-09
    n2 = 0.36357485
    Rsh = 736489.301
    
    Rs = 339654.733/m
    Iph = J_ph[-1]*area/consistency_factor
    
    
    T = 298.0
    Vth = consts.k*T/consts.elementary_charge
    
    # Go over all data sets and create a corresponding model sets
    
    Imodel = single_solar_cell(Vmodel,Is2,n2,T,Rs,Rsh,Iph)
    V2model = Vmodel - Rs*Imodel
    
    Pmodel = Imodel*Vmodel
    
    Pd2model = Is2*(np.exp(V2model/n2/Vth)-1) * V2model
    
    Psmodel = Rs * Imodel**2
    Pshmodel = V2model **2 / Rsh
    Pphmodel = -Iph * V2model
    
    Precomb = Pd2model
    Pohmic = Psmodel + Pshmodel
    
    # Interpolate to calculate all the quantities at the MPP, SC, OC etc
    
    i_mp = np.argmin(Pmodel)
    Vmp = Vmodel[i_mp]
    Imp = Imodel[i_mp]
    Pmp = Pmodel[i_mp]
    P_mp = np.append(P_mp,Pmp)
    
    V_interp = (interp1d(Imodel,Vmodel))
    I_interp = (interp1d(Vmodel,Imodel))
    
    Voc = np.append(Voc,V_interp(0))
    Isc = np.append(Isc,I_interp(0))
    FF = np.append(FF,Pmp/(Voc[-1]*Isc[-1]))
    PCE = np.append(PCE,-Pmp/flux/area/1e-3)
    
    #Pd1_interp.append(interp1d(Imodel[j],Pd1model[j]))
    Pd2_interp = (interp1d(Vmodel,Pd2model))
    
    Ps_interp = (interp1d(Vmodel,Psmodel))
    Psh_interp = (interp1d(Vmodel,Pshmodel))
    Pph_interp = (interp1d(Vmodel,Pphmodel))
    
    Pd2_mp = np.append(Pd2_mp,Pd2_interp(Vmp))
    
    Ps_mp = np.append(Ps_mp,Ps_interp(Vmp))
    Psh_mp = np.append(Psh_mp,Psh_interp(Vmp))
    Pph_mp = np.append(Pph_mp,Pph_interp(Vmp))
    
    
#%% Plot
plt.close("all")
color = "purple"
label = "$\Phi$="+str(flux)+r" $\mathrm{{mW}/{cm^2}}$"

if plot_reverse:
    direct = -1
else:
    direct = 1

plt.figure()

plt.subplot(411)
plt.plot(ms,-Isc*1e9,marker='',color="black",linestyle='-')
#plt.grid()
plt.legend()
plt.ylabel(r"$I_{\mathrm{sc}}$ [nA]",fontsize=default_fontsize)

plt.subplot(412)
plt.plot(ms,1e3*Voc,marker='',color="black",linestyle='-')
#plt.grid()
#plt.xlabel("Flux "+r" ${\mu W}/{cm^2}$",fontsize=default_fontsize)
plt.ylabel("$V_{oc}$ [mV]",fontsize=default_fontsize)

plt.subplot(413)
plt.plot(ms,FF*100,marker='',color="black",linestyle='-')
#plt.grid()
#plt.xlabel("Flux "+r" ${\mu W}/{cm^2}$",fontsize=default_fontsize)
plt.ylabel("FF [%]",fontsize=default_fontsize)

plt.subplot(414)
plt.plot(ms,100*PCE,marker='',color="black",linestyle='-')
#plt.grid()
plt.ylabel("PCE [%]",fontsize=default_fontsize)
plt.xlabel("Number of $\mathrm{MoS_2}$ monolayers",fontsize=default_fontsize)

plt.figure()
plt.subplot(211)
plt.plot(ms,-1e9*Pph_mp,label="Photogeneration",color="black",marker='')
plt.plot(ms,1e9*Pd2_mp,label="Recombination",marker='')
plt.plot(ms,1e9*Psh_mp,label="Shunt resistance",marker='')
plt.plot(ms,-1e9*P_mp,label="Output",color="black",linestyle='--',marker='')
plt.plot(ms,1e9*Ps_mp,label="Series resistance",marker='')

plt.ylabel("Power at MPP [nW]",fontsize=default_fontsize)
plt.xlabel("Number of $\mathrm{MoS_2}$ monolayers",fontsize=default_fontsize)
plt.legend()

plt.subplot(212)
plt.plot(ms,-100*Pd2_mp/Pph_mp,label="Recombination "+str(round(min(-100*Pd2_mp/Pph_mp),1))+"-"+str(round(max(-100*Pd2_mp/Pph_mp),1))+"%"
    ,marker='')
plt.plot(ms,-100*Psh_mp/Pph_mp,label="Shunt resistance "+str(round(min(-100*Psh_mp/Pph_mp),1))+"-"+str(round(max(-100*Psh_mp/Pph_mp),1))+"%"
         ,marker='')
plt.plot(ms,100*P_mp/Pph_mp,label="Output "+str(round(min(100*P_mp/Pph_mp),1))+"-"+str(round(max(100*P_mp/Pph_mp),1))+"%"
         ,color="black",linestyle='--',marker='')
plt.plot(ms,-100*Ps_mp/Pph_mp,label="Series resistance "+str(round(min(-100*Ps_mp/Pph_mp),1))+"-"+str(round(max(-100*Ps_mp/Pph_mp),1))+"%"
         ,marker='')

plt.ylabel("Fraction of photogenerated\n power at MPP [%]"
           ,fontsize=default_fontsize)
plt.xlabel("Number of $\mathrm{MoS_2}$ monolayers",fontsize=default_fontsize)
plt.legend()

plt.figure()
plt.subplot(211)
plt.plot(ms,incA_all_useful,color="black",label="Total")
plt.plot(ms,incA_Si_useful,color="red",label="Si")
plt.plot(ms,incA_MoS2_useful,color="blue",label=r"$\mathrm{MoS_2}$")
plt.ylabel(r"Available flux $\left[\mathrm{\dfrac{mW}{cm^2}}\right]$",fontsize=default_fontsize)
plt.legend()
plt.subplot(212)
plt.plot(ms,phCnt_all_suff,color="black",label="Total")
plt.plot(ms,phCnt_Si_suff,color="red",label="Si")
plt.plot(ms,phCnt_MoS2_suff,color="blue",label=r"$\mathrm{MoS_2}$")
plt.ylabel(r"Available photons $\left[\mathrm{\dfrac{photons}{sec \cdot cm^2}}\right]$",fontsize=default_fontsize)
plt.xlabel("Number of $\mathrm{MoS_2}$ monolayers",fontsize=default_fontsize)
plt.legend()