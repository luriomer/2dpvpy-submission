#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 14:57:20 2018

@author: omer
"""

from core.importer import *
sys.path.insert(0,'../..')


plt.close("all")
print str(time.ctime())+"\n"

#%% Global parameters

name = "PVdevice" #name of the sample
lmd = np.arange(350,1000,1.0,dtype=float) # spectral range [nm]
AoIs = [0]
Eg_MoS2 = 1.9 #eV
Eg_Si = 1.14 #eV

#%% Preparation of participating instances

# layers
Void = layer_cls(name="Void" , lmd=lmd , d=0.62857151 , disp_type="Sum" , disp_params={"Baseline":1-0j} , disp_vary=False)

Si = layer_cls(name="Si", lmd=lmd , d=1.0e6 , disp_type="Sum" , 
               disp_params={"Tab":"../../data/Si/c-Si_bulk_Schinke2015.txt"} , disp_vary=False)

MoS2 = layer_cls(name="MoS2",lmd=lmd ,d=0.62857151,disp_type="Sum" ,
                 disp_params={"Baseline":7.01419247-0j,
                              "Lorentz_1":(1.92000000,4.13728777,0.05000000),
                              "Lorentz_2":(2.05838772,10.1774136,0.15740943),
                              "Lorentz_3":(2.41734278,4.63272964,0.18347886),
                              "Lorentz_4":(2.60013054,4.34979139,0.31233556),
                              "Lorentz_5":(2.76251393,2.81090837,0.50690582)
                              })

# stacks
device_stack = stack_cls(name="device_stack",layers=[Void,MoS2,Si,Void],lmd=lmd,AoIs=AoIs)
# model
model = model_cls(name="model",lmd=lmd,AoIs=AoIs,stacks={device_stack:1.0})
model.calc_all()

#%% Use the incident spectrum and calculate the optical performance

spectrum_wavelength_data = np.load("spectrum_wavelength.npy")
spectrum_normalized_data = np.load("spectrum_normalized.npy")
spectrum_0642_data = np.load("spectrum_0642.npy")
spectrum_1243_data = np.load("spectrum_1243.npy")
spectrum_1844_data = np.load("spectrum_1844.npy")
spectrum_2893_data = np.load("spectrum_2893.npy")

# Interpolate and sample to match the spectrum
spectrum_normalized = interp1d(spectrum_wavelength_data,spectrum_normalized_data)(lmd)
spectrum_0642 = interp1d(spectrum_wavelength_data,spectrum_0642_data)(lmd)
spectrum_1243 = interp1d(spectrum_wavelength_data,spectrum_1243_data)(lmd)
spectrum_1844 = interp1d(spectrum_wavelength_data,spectrum_1844_data)(lmd)
spectrum_2893 = interp1d(spectrum_wavelength_data,spectrum_2893_data)(lmd)

incident_spectra = [spectrum_0642,spectrum_1243,spectrum_1844,spectrum_2893]
flux = [0.642,1.243,1.844,2.893] #mW/cm^2
incA = []
incR = []
inc_spectrum_total = []
incA_MoS2_total = []
incA_MoS2_suff = []
incA_Si_total = []
incA_Si_suff = []
incR_total =[]
incA_all_total = []
incA_all_suff = []
E_per_photon = (consts.h*1e3)*(consts.c*1e+9)/lmd # Energy of photon in units of mJ, as a fuction of wavelength
phCnt_spectrum_total = []
phCnt_MoS2_total = []
phCnt_MoS2_suff = []
phCnt_Si_total = []
phCnt_Si_suff = []
phCnt_R_total = []
phCnt_all_total = []
phCnt_all_suff = []
incA_MoS2_useful = []
incA_Si_useful = []
incA_all_useful = []

for i,incident_spectrum in enumerate(incident_spectra):

    incA.append(device_stack.A_glob[0] * incident_spectrum)
    incR.append(device_stack.R[0] * incident_spectrum)
    # Integrating the spectral fluxes to obtain the total and partial
    # fluxes and number of photons.
    # The integration must be with Gaussian quadrature of high order
    # since the data has noise and the function is not smooth. 
    # Simpler methods yeild poor results.
    integ_order = 100
    incA_MoS2_total.append(calc_integral(lmd,incA[i][1],lmd[0],lmd[-1],method="Gauss",order=integ_order))
    incA_MoS2_suff.append(calc_integral(lmd,incA[i][1],lmd[0],eV_nm(Eg_MoS2),method="Gauss",order=integ_order))
    incA_Si_total.append(calc_integral(lmd,incA[i][2],lmd[0],lmd[-1],method="Gauss",order=integ_order))
    incA_Si_suff.append(calc_integral(lmd,incA[i][2],lmd[0],lmd[-1],method="Gauss",order=integ_order))
    incA_all_total.append(incA_MoS2_total[i]+incA_Si_total[i])
    incA_all_suff.append(incA_MoS2_suff[i]+incA_Si_suff[i])
    incR_total.append(calc_integral(lmd,incR[i],lmd[0],lmd[-1]))
    
    # Photons
    phCnt_spectrum_total.append(calc_integral(lmd,incident_spectrum/E_per_photon,lmd[0],lmd[-1]))
    phCnt_MoS2_total.append(calc_integral(lmd,incA[i][1]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
    phCnt_MoS2_suff.append(calc_integral(lmd,incA[i][1]/E_per_photon,lmd[0],eV_nm(Eg_MoS2),method="Gauss",order=integ_order))
    phCnt_Si_total.append(calc_integral(lmd,incA[i][2]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
    phCnt_Si_suff.append(calc_integral(lmd,incA[i][2]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
    phCnt_R_total.append(calc_integral(lmd,incR[i]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
    phCnt_all_total.append(phCnt_MoS2_total[i]+phCnt_Si_total[i])
    phCnt_all_suff.append(phCnt_MoS2_suff[i]+phCnt_Si_suff[i])
    
    incA_MoS2_useful.append(phCnt_MoS2_suff[i]*Eg_MoS2*consts.elementary_charge*1e3)
    incA_Si_useful.append(phCnt_Si_suff[i]*Eg_Si*consts.elementary_charge*1e3)
    incA_all_useful.append(incA_MoS2_useful[i]+incA_Si_useful[i])
    
    #current_density_all
    
#%% Results
plt.close("all")
digits = 5
#plot_optical_config(model)
#RTAplot = plot_flux(model,selected_AoIs=AoIs,horunits="nm")
#absplot = plot_abs_dens(device_stack,horunits="nm")
print_all_messages()
for i,incident_spectrum in enumerate(incident_spectra):
    print "Flux of "+str(flux[i])+" mW/cm^2 , "+"{:.4e}".format(phCnt_spectrum_total[i])+" photons/cm^2*sec:"
    
    print "    MoS2 absorbed "+str(round(incA_MoS2_total[i]*100/flux[i],digits))+"% of the flux , "+str(round(phCnt_MoS2_total[i]*100/phCnt_spectrum_total[i],digits))+"% of the photons\n        above-gap "+str(round(incA_MoS2_suff[i]*100/flux[i],digits))+"% of the flux , "+str(round(phCnt_MoS2_suff[i]*100/phCnt_spectrum_total[i],digits))+"% of the photons\n        useful "+str(round(incA_MoS2_useful[i]*100/flux[i],digits))+" % of the flux"
    
    print "    Si absorbed "+str(round(incA_Si_total[i]*100/flux[i],digits))+"% of the flux , "+str(round(phCnt_Si_total[i]*100/phCnt_spectrum_total[i],digits))+"% of the photons\n        above-gap "+str(round(incA_Si_suff[i]*100/flux[i],digits))+"% of the flux , "+str(round(phCnt_Si_suff[i]*100/phCnt_spectrum_total[i],digits))+"% of the photons\n        useful "+str(round(incA_Si_useful[i]*100/flux[i],digits))+" % of the flux"
    
    print "    MoS2+Si absorbed "+str(round(incA_all_total[i]*100/flux[i],digits))+"% of the flux, "+str(round(phCnt_all_total[i]*100/phCnt_spectrum_total[i],digits))+"% of the photons\n        above-gap "+str(round(incA_all_suff[i]*100/flux[i],digits))+"% of the flux, "+str(round(phCnt_all_suff[i]*100/phCnt_spectrum_total[i],digits))+"% of the photons\n        useful "+str(round(incA_all_useful[i]*100/flux[i],digits))+" % of the flux"
    
    print "    Reflection loss "+str(round(incR_total[i]*100/flux[i],digits))+"% of the flux , "+str(round(phCnt_R_total[i]*100/phCnt_spectrum_total[i],digits))+"% of the photons"
    
    print "    Insufficient energy loss "+str(round((incA_all_total[i]-incA_all_suff[i])*100/flux[i],digits))+"% of the flux , "+str(round((phCnt_all_total[i]-phCnt_all_suff[i])*100/phCnt_spectrum_total[i],digits))+"% of the photons"
    
    print "    Excess energy loss "+str(round((incA_all_suff[i]-incA_all_useful[i])*100/flux[i],digits))+"% of the flux"
    
    print "\n"
    
    plt.figure()
    labels = ['Reflection loss', 'Below bandgap loss', 'Excess energy loss', 'Available for conversion']
    sizes = np.array([(incR_total[i]*100/flux[i]), 
             (incA_all_total[i]-incA_all_suff[i])*100/flux[i], 
             (incA_all_suff[i]-incA_all_useful[i])*100/flux[i], 
             (incA_all_useful[i]*100/flux[i])])
    for j,label in enumerate(labels):
        labels[i]=(label+"\n"+str(round(sizes[j],2)))+"%"    
    explode = (0.0, 0.0, 0.0, 0.1)  # only "explode" the 2nd slice (i.e. 'Hogs')
    
    plt.pie(np.log10(sizes*100), explode=explode, labels=labels, autopct=None,
            labeldistance = 0.5, shadow=False, startangle=90)
    plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
    
    #plt.show()
    
    plt.title("$\Phi_{in}$ = "+str(flux[i])+" $\mathrm{mW/cm^2}$")
    
    
    plt.figure()
    plt.title("$\Phi_{in}$ = "+str(flux[i])+" $\mathrm{mW/cm^2}$")
    plt.plot(lmd,incident_spectrum,label="Incident",color="black",linestyle='-')
    plt.plot(lmd,incA[i][2],label="Absorbed in Si",color="red",linestyle='-')
    plt.plot(lmd,incident_spectrum*device_stack.R[0],label="Reflection loss",color="black",linestyle='--')
    plt.plot(lmd,incA[i][1],label="Absorbed in $\mathrm{MoS_2}$",color="blue",linestyle='-')
    plt.grid()
    plt.legend()
    plt.xlabel("$\lambda$ [nm]",fontsize=12)
    plt.ylabel(r"$\Phi_{\lambda}$ $\left[\mathrm{\dfrac{mW}{cm^2 nm}}\right]$",fontsize=12)
    plt.yscale("log")

