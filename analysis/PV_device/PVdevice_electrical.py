#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 12:57:07 2020

@author: omer
"""

import numpy as np
import matplotlib.pyplot as plt
import lmfit
import scipy.constants as consts
from scipy.optimize import fsolve
from scipy.special import lambertw
from scipy.interpolate import interp1d

global Imodel

plt.rcParams['font.family'] = 'serif'
plt.rcParams['mathtext.fontset'] = 'dejavuserif'
default_fontsize = 14
plt.rc('xtick',labelsize=default_fontsize)
plt.rc('ytick',labelsize=default_fontsize)

#%% Define all functions
def single_solar_cell(V,Is,n,T,Rs,Rsh,Iph):
    ''' Analytical solution of I(V) for a single solar cell using the Lambert W function '''
    Vth = consts.k*T/consts.elementary_charge
    term1 = (Iph+Is-V/Rsh)/(1+Rs/Rsh)
    term2 = n*Vth/Rs
    term3 = (Is*Rs)/(n*Vth*(1+Rs/Rsh))
    term4 = V/(n*Vth) * (1-Rs/(Rs+Rsh))
    term5 = (Iph+Is)*Rs / (n*Vth*(1+Rs/Rsh))
    I = term1 - term2 * lambertw(term3 * np.exp(term4 + term5))
    #if any(np.imag(I)):
        #raise ValueError("complex values in single_solar_cell: check the range and paramters!")
    #else:
    I = np.real(I)
    return -I


def Innershell(x,V,Is1,n1,c1,Is2,n2,Rsh,Is3,n3,c3,T,Rs,Iph):
    ''' Residual of the system of equations for the I-V curve of a solar cell with two rectifying contacts.
        The Innershell is a point function, and used to find the residual of a single votage value. '''
    
    I = x[0]
    V1 = x[1]
    V2 = x[2]
    V3 = x[3]
    
    Vth = consts.k*T/consts.elementary_charge # Thermal voltage in [V]
    
    eq1 = I - c1*Is1*(np.exp(c1*V1/Vth/n1)-1)
    if c1 == 0:
        eq1 = 0
        V1 = 0
    eq2 = I - Is2*(np.exp(V2/Vth/n2)-1) - V2/Rsh + Iph
    eq3 = I - c3*Is3*(np.exp(c3*V3/Vth/n3)-1)
    if c3 == 0:
        eq3 = 0
        V3 = 0
    eq4 = V - V1 - V2 - V3 - I*Rs
    return np.array([eq1,eq2,eq3,eq4])



def Outershell(params,**kwargs):
    global Imodel
    Is1 = params["Is1"]
    n1 = params["n1"]
    c1 = params["c1"]
    
    Is2 = params["Is2"]
    n2 = params["n2"]
    Rsh = params["Rsh"]
    
    Is3 = params["Is3"]
    n3 = params["n3"]
    c3 = params["c3"]
    
    Iph1 = params["Iph1"]
    Iph2 = params["Iph2"]
    Iph3 = params["Iph3"]
    Iph4 = params["Iph4"]
    Iph5 = params["Iph5"]
    
    Iph = [Iph1,Iph2,Iph3,Iph4,Iph5]
    
    Rs = params["Rs"]
    T = params["T"]

    Idata = kwargs["Idata"]
    Vdata = kwargs["Vdata"]
    form = kwargs["form"]
    
    
    Imodel = np.zeros([len(Idata),len(Vdata)])
    residuals = np.zeros_like(Vdata)
    for j,Idataj in enumerate(Idata):
        I0 = single_solar_cell(Vdata,Is2,n2,T,Rs,Rsh,Iph[j])
        if form == "full":
            for i,Vi in enumerate(Vdata):
                res = fsolve(Innershell,[I0[i],Vi*0.01,Vi*0.97,Vi*0.01],args=(Vi,Is1,n1,c1,Is2,n2,Rsh,Is3,n3,c3,T,Rs,Iph[j]),full_output=1)
                Imodel[j][i] = res[0][0]
    
        elif form == "single":
            Imodel[j] = I0
    
        residuals += (Imodel[j]-Idataj)**2
    return np.sqrt(residuals)


def Isc_flux_depend(params,**kwargs):
    C = params["C"]
    D = params["D"]
    m = params["m"]
    fluxes = kwargs["fluxes"]
    Isc = kwargs["Isc"]
    model_depend = C*fluxes**m + D
    residual = abs(model_depend-Isc)**2
    return np.sqrt(residual)


#%% Load data and parameters
data = np.loadtxt("omer34_IV.txt")
voltage_limit = [-0.06,0.06]
form = "full"
plot_reverse = True


Vdata = data[:,0]
I_dark = data[:,1]
I_1 = data[:,2]
I_2 = data[:,3]
I_3 = data[:,4]
I_4 = data[:,5]



if voltage_limit:
    left = np.where(Vdata>voltage_limit[0])[0][0]
    right = np.where(Vdata<voltage_limit[1])[0][-1]
    Vdata = Vdata[left:right]
    I_dark = I_dark[left:right]
    I_1 = I_1[left:right]
    I_2 = I_2[left:right]
    I_3 = I_3[left:right]
    I_4 = I_4[left:right]


params = lmfit.Parameters()

params.add("Is1",value =2.5367e-05, vary=False)
params.add("n1",value = 1.183898 , vary=False)
params.add("c1",value = 0 , vary=False)

params.add("Is2",value = 4.6370e-09 , vary=True)
params.add("n2",value = 0.36357485 , vary=True)
params.add("Rsh",value = 736489.301 , vary=True)

params.add("Is3",value = 2.3956e-07 , vary=True)
params.add("n3",value = 2.30783826 , vary=True)
params.add("c3",value = -1 , vary=False)

params.add("Rs",value = 339654.733 , vary=True)

params.add("Iph1",value = 2.0784e-07 , vary=True)
params.add("Iph2",value = 1.0609e-07 , vary=True)
params.add("Iph3",value = 6.7775e-08 , vary=True)
params.add("Iph4",value = 3.8690e-08 , vary=True)
params.add("Iph5",value = 0.0 , vary=False)

params.add("T",value = 298.0 , vary=False)

Idata = [I_4,I_3,I_2,I_1] #A, in the common convention (photocurrent is negative)
fluxes = [2.893,1.844,1.243,0.642] # mW/cm^2
area = 1.5e-5 # in cm^2
Vmodel = np.linspace(-0.25,0.1,100)
fit_Isc_stars = True

#%% Perform the fit
mini = lmfit.Minimizer(Outershell,params,fcn_kws={"Idata":Idata,"Vdata":Vdata,"form":form})
res = mini.minimize(method="leastsq")
lmfit.report_fit(res)


#%% Load the best-fit model

# Get the optimized parameter values
Is1 = res.params["Is1"]
n1 = res.params["n1"]
c1 = res.params["c1"]

Is2 = res.params["Is2"]
n2 = res.params["n2"]
Rsh = res.params["Rsh"]

Is3 = res.params["Is3"]
n3 = res.params["n3"]
c3 = res.params["c3"]

Rs = res.params["Rs"]
Iph = (res.params["Iph1"],res.params["Iph2"],res.params["Iph3"],res.params["Iph4"],res.params["Iph5"])


T = res.params["T"]
Vth = consts.k*T/consts.elementary_charge

# Prepare all needed variables
Imodel_single = np.zeros([len(Idata),len(Vmodel)])
Imodel = np.zeros([len(Idata),len(Vmodel)])

V1model = np.zeros([len(Idata),len(Vmodel)])
V2model = np.zeros([len(Idata),len(Vmodel)])
V3model = np.zeros([len(Idata),len(Vmodel)])

Pmodel = np.zeros([len(Idata),len(Vmodel)])
Pd1model = np.zeros([len(Idata),len(Vmodel)])
Pd2model = np.zeros([len(Idata),len(Vmodel)])
Pd3model = np.zeros([len(Idata),len(Vmodel)])
Precomb = np.zeros([len(Idata),len(Vmodel)])

Psmodel = np.zeros([len(Idata),len(Vmodel)])
Pshmodel = np.zeros([len(Idata),len(Vmodel)])
Pohmic = np.zeros([len(Idata),len(Vmodel)])

Pphmodel = np.zeros([len(Idata),len(Vmodel)])

i_mp = np.zeros([len(Idata)],dtype=int)
Vmp = np.zeros([len(Idata)])
Imp = np.zeros([len(Idata)])
Pmp = np.zeros([len(Idata)])
Voc = np.zeros([len(Idata)])
Isc = np.zeros([len(Idata)])
Isc_star1 = np.zeros([len(Idata)])
Isc_star2 = np.zeros([len(Idata)])
FF = np.zeros([len(Idata)])


V_interp = []
I_interp = []
Istar1_interp = []
Istar2_interp = []

Pd1_interp = []
Pd2_interp = []
Pd3_interp = []
Ps_interp = []
Psh_interp = []
Pph_interp = []

Pd1_mp = np.zeros([len(Idata)])
Pd2_mp = np.zeros([len(Idata)])
Pd3_mp = np.zeros([len(Idata)])
Ps_mp = np.zeros([len(Idata)])
Psh_mp = np.zeros([len(Idata)])
Pph_mp = np.zeros([len(Idata)])

# Go over all data sets and create a corresponding model sets
for j in range(len(Idata)):
    Imodel_single[j] = single_solar_cell(Vmodel,Is2,n2,T,Rs,Rsh,Iph[j])
    for i,Vi in enumerate(Vmodel):
        resi = fsolve(Innershell,[Imodel_single[j][i],Vi*0.01,Vi*0.97,Vi*0.01],args=(Vi,Is1,n1,c1,Is2,n2,Rsh,Is3,n3,c3,T,Rs,Iph[j]),full_output=1)
        Imodel[j][i] = resi[0][0]
        V1model[j][i] = resi[0][1]
        V2model[j][i] = resi[0][2]
        V3model[j][i] = resi[0][3]
    
    Pmodel[j] = Imodel[j]*Vmodel
    Pd1model[j] = V1model[j] * Imodel[j]
    Pd2model[j] = Is2*(np.exp(V2model[j]/n2/Vth)-1) * V2model[j]
    Pd3model[j] = V3model[j] * Imodel[j]
    Psmodel[j] = Rs * Imodel[j]**2
    Pshmodel[j] = V2model[j] **2 / Rsh
    Pphmodel[j] = -Iph[j] * V2model[j]
    
    Precomb[j] = abs(c1)*Pd1model[j] + Pd2model[j] + abs(c3)*Pd3model[j]
    Pohmic[j] = Psmodel[j] + Pshmodel[j]
    
    # Interpolate to calculate all the quantities at the MPP, SC, OC etc
    
    i_mp[j] = np.argmin(Pmodel[j])
    Vmp[j] = Vmodel[i_mp[j]]
    Imp[j] = Imodel[j][i_mp[j]]
    Pmp[j] = Pmodel[j][i_mp[j]]
    
    V_interp.append(interp1d(Imodel[j],Vmodel))
    I_interp.append(interp1d(Vmodel,Imodel[j]))
    
    if fit_Isc_stars:
        Istar1_interp.append(interp1d(V2model[j]+Imodel[j]*Rs,Imodel[j])) # Central block voltage, indlucing Rs
        Istar2_interp.append(interp1d(V2model[j]+Imodel[j],Imodel[j])) # Central block boltage, excluding Rs
    
    Voc[j] = V_interp[j](0)
    Isc[j] = I_interp[j](0)
    if fit_Isc_stars:
        Isc_star1[j] = Istar1_interp[j](0)
        Isc_star2[j] = Istar2_interp[j](0)
    FF[j] = Pmp[j]/(Voc[j]*Isc[j])

    #Pd1_interp.append(interp1d(Imodel[j],Pd1model[j]))
    Pd2_interp.append(interp1d(Vmodel,Pd2model[j]))
    Pd3_interp.append(interp1d(Vmodel,Pd3model[j]))
    Ps_interp.append(interp1d(Vmodel,Psmodel[j]))
    Psh_interp.append(interp1d(Vmodel,Pshmodel[j]))
    Pph_interp.append(interp1d(Vmodel,Pphmodel[j]))
    
    Pd2_mp[j] = Pd2_interp[j](Vmp[j])
    Pd3_mp[j] = Pd3_interp[j](Vmp[j])
    Ps_mp[j] = Ps_interp[j](Vmp[j])
    Psh_mp[j] = Psh_interp[j](Vmp[j])
    Pph_mp[j] = Pph_interp[j](Vmp[j])
    
    
#%% Fit Isc-flux and Isc_star-flux to a general power law to confirm nearly linear dependence
params_Isc = lmfit.Parameters()
params_Isc.add("C",value =20.0, vary=True)
params_Isc.add("D",value =0.0, vary=True)
params_Isc.add("m",value =1.0, vary=True)

res_Isc = lmfit.minimize(Isc_flux_depend,params_Isc,kws={"Isc":-Isc*1e9,"fluxes":fluxes})
C = res_Isc.params["C"]
D = res_Isc.params["D"]
m = res_Isc.params["m"]
if fit_Isc_stars:
    res_Isc_star1 = lmfit.minimize(Isc_flux_depend,params_Isc,kws={"Isc":-Isc_star1*1e9,"fluxes":fluxes})
    C_star1 = res_Isc_star1.params["C"]
    D_star1 = res_Isc_star1.params["D"]
    m_star1 = res_Isc_star1.params["m"]
    
    res_Isc_star2 = lmfit.minimize(Isc_flux_depend,params_Isc,kws={"Isc":-Isc_star2*1e9,"fluxes":fluxes})
    C_star2 = res_Isc_star2.params["C"]
    D_star2 = res_Isc_star2.params["D"]
    m_star2 = res_Isc_star2.params["m"]

#%% Plot
plt.close("all")
colors = ["purple","blue","green","red"]
labels=[]
for flux in fluxes:
    if type(flux) == str:
        labels.append(flux)
    else:
        labels.append("$\Phi$="+str(flux)+r" $\mathrm{{mW}/{cm^2}}$")
if plot_reverse:
    direct = -1
else:
    direct = 1
plt.figure()
IV_plot = plt.subplot(211)
PV_plot = plt.subplot(212)
for j in range(len(Idata)):
    
    IV_plot.plot(1e3*Vdata,Idata[j]*1e9*direct,label="Data "+labels[j],linestyle='',marker='o',color=colors[j])
    IV_plot.plot(1e3*Vmodel,Imodel[j]*1e9*direct,label='Model fit',linestyle='-',color=colors[j])
    
    PV_plot.plot(1e3*Vdata,Vdata*Idata[j]*1e9*direct,label="Data "+labels[j],linestyle='',marker='o',color=colors[j])
    PV_plot.plot(1e3*Vmodel,Vmodel*Imodel[j]*1e9*direct,label='Model fit',linestyle='-',color=colors[j])
    
    plt.figure()
    plt.suptitle(labels[j])
    plt.subplot(211)
    if c1 != 0:
        plt.plot(1e3*Vmodel,1e3*V1model[j],label="Diode 1")
    plt.plot(1e3*Vmodel,1e3*V2model[j],label="Central block (without $R_{\mathrm{s}}$)")
    if c3 != 0:
        plt.plot(1e3*Vmodel,1e3*V3model[j],label="Contact diode")
    plt.plot(1e3*Vmodel,1e3*Imodel[j]*res.params["Rs"],label="Series resistor")
    
    plt.xlabel("Bias voltage [mV]",fontsize=default_fontsize)
    plt.ylabel("Voltage drop [mV]",fontsize=default_fontsize)
    plt.grid()
    plt.legend()
    
    plt.subplot(212)
    #plt.plot(1e3*Vdata,1e9*Idata[j]*Vdata*direct,label="Data",color="black",linestyle='',marker='o')
    plt.plot(1e3*Vmodel,1e9*Imodel[j]*Vmodel*direct,label="Total output",color="black",linestyle='--')
    plt.plot(1e3*Vmodel,1e9*Pd2model[j]*direct,label="Recombination")
    plt.plot(1e3*Vmodel,1e9*Pd3model[j]*direct,label="Contact barrier")
    plt.plot(1e3*Vmodel,1e9*Psmodel[j]*direct,label="Series resistance")
    plt.plot(1e3*Vmodel,1e9*Pshmodel[j]*direct,label="Shunt resistance")
    plt.plot(1e3*Vmodel,1e9*Pphmodel[j]*direct,label="Photogeneration")
    plt.plot(1e3*Vmp[j],1e9*Pmp[j]*direct,marker='o',markersize=8.0,linestyle='',color="red",label="Max power point")
    
    plt.xlabel("Bias voltage [mV]",fontsize=default_fontsize)
    plt.ylabel("Generated power [nW]",fontsize=default_fontsize)
    plt.grid()
    plt.legend()
    
IV_plot.set_xlabel("Bias voltage [mV]",fontsize=default_fontsize)
IV_plot.set_ylabel("Generated current [nA]",fontsize=default_fontsize)
IV_plot.grid()
IV_plot.legend()

PV_plot.set_xlabel("Bias voltage [mV]",fontsize=default_fontsize)
PV_plot.set_ylabel("Generated power [nW]",fontsize=default_fontsize)
PV_plot.grid()
PV_plot.legend()

plt.figure()

plt.subplot(411)
plt.plot(fluxes,-Isc*1e9,marker='o',color="blue",linestyle='')
plt.plot(fluxes,D+C*fluxes**m,linestyle='--',color="blue",label="$I_{sc} \propto \Phi^{%f}$" %m)
if fit_Isc_stars:
    plt.plot(fluxes,-Isc_star1*1e9,marker='o',color="red",linestyle='')
    plt.plot(fluxes,D_star1+C_star1*fluxes**m_star1,linestyle='--',color="red",label="$I^*_{sc} \propto \Phi^{%f}$" %m_star1)
    #plt.plot(fluxes,-Isc_star2*1e9,marker='o',color="green",linestyle='')
    #plt.plot(fluxes,D_star2+C_star2*fluxes**m_star2,linestyle='--',color="green",label="$I^{**}_{sc} \propto \Phi^{%f}$" %m_star2)

plt.legend()
plt.ylabel(r"$I_{\mathrm{sc}}$ [nA]",fontsize=default_fontsize)

plt.subplot(412)
plt.plot(fluxes,1e3*Voc,marker='o',color="black",linestyle='')

#plt.xlabel("Flux "+r" ${\mu W}/{cm^2}$",fontsize=default_fontsize)
plt.ylabel("$V_{oc}$ [mV]",fontsize=default_fontsize)

plt.subplot(413)
plt.plot(fluxes,FF*100,marker='o',color="black",linestyle='')

#plt.xlabel("Flux "+r" ${\mu W}/{cm^2}$",fontsize=default_fontsize)
plt.ylabel("FF [%]",fontsize=default_fontsize)
plt.xlabel("$\Phi$ "+r" $\left[\mathrm{mW/cm^2}\right]$",fontsize=default_fontsize)

plt.subplot(414)
plt.plot(fluxes,-1e3*Pmp/np.array(fluxes)/area*100,marker='o',color="black",linestyle='')

#plt.xlabel("Flux "+r" ${\mu W}/{cm^2}$",fontsize=default_fontsize)
plt.ylabel("PCE [%]",fontsize=default_fontsize)
plt.xlabel("$\Phi_{\mathrm{in}}$ "+r" $\left[\mathrm{mW/cm^2}\right]$",fontsize=default_fontsize)

plt.figure()
plt.subplot(211)
plt.plot(fluxes,-1e9*Pph_mp,label="Photogeneration",color="black",linestyle='-',marker='o')
plt.plot(fluxes,1e9*Pd2_mp,label="Recombination",marker='o')
plt.plot(fluxes,1e9*Psh_mp,label="Shunt resistance",marker='o')
plt.plot(fluxes,-1e9*Pmp,label="Output",color="black",linestyle='--',marker='o')
plt.plot(fluxes,1e9*Ps_mp,label="Series resistance",marker='o')
plt.plot(fluxes,1e9*Pd3_mp,label="Contact barrier",marker='o')
plt.ylabel("Power at MPP [nW]",fontsize=default_fontsize)
plt.xlabel("$\Phi_{\mathrm{in}}$ "+r" $\left[\mathrm{mW/cm^2}\right]$",fontsize=default_fontsize)
plt.legend()

plt.subplot(212)
plt.plot(fluxes,-100*Pd2_mp/Pph_mp,label="Recombination "+str(round(min(-100*Pd2_mp/Pph_mp),1))+"-"+str(round(max(-100*Pd2_mp/Pph_mp),1))+"%"
    ,marker='o')
plt.plot(fluxes,-100*Psh_mp/Pph_mp,label="Shunt resistance "+str(round(min(-100*Psh_mp/Pph_mp),1))+"-"+str(round(max(-100*Psh_mp/Pph_mp),1))+"%"
         ,marker='o')
plt.plot(fluxes,100*Pmp/Pph_mp,label="Output "+str(round(min(100*Pmp/Pph_mp),1))+"-"+str(round(max(100*Pmp/Pph_mp),1))+"%"
         ,color="black",linestyle='--',marker='o')
plt.plot(fluxes,-100*Ps_mp/Pph_mp,label="Series resistance "+str(round(min(-100*Ps_mp/Pph_mp),1))+"-"+str(round(max(-100*Ps_mp/Pph_mp),1))+"%"
         ,marker='o')
plt.plot(fluxes,-100*Pd3_mp/Pph_mp,label="Contact barrier "+str(round(min(-100*Pd3_mp/Pph_mp),1))+"-"+str(round(max(-100*Pd3_mp/Pph_mp),1))+"%"
         ,marker='o')
plt.ylabel("Fraction of photogenerated\n power at MPP [%]"
           ,fontsize=default_fontsize)
plt.xlabel("$\Phi$ "+r" $\left[\mathrm{mW/cm^2}\right]$"
                               ,fontsize=default_fontsize)
plt.legend()