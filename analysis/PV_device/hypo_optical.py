#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 14 14:57:20 2018

@author: omer
"""

from core.importer import *
sys.path.insert(0,'../..')


plt.close("all")
print str(time.ctime())+"\n"

#%% Global parameters

name = "PVdevice" #name of the sample
lmd = np.arange(280,4000,1.0,dtype=float) # spectral range [nm]
AoIs = [0]
Eg_MoS2 = 1.9 #eV
Eg_Si = 1.14 #eV
m = 310
#%% Preparation of participating instances

# layers
Void = layer_cls(name="Void" , lmd=lmd , d=1.0 , disp_type="Sum" , disp_params={"Baseline":1-0j} , disp_vary=False)

Si = layer_cls(name="Si", lmd=lmd , d=1.0e6 , disp_type="Sum" , 
               disp_params={"Tab":"../../data/Si/c-Si_bulk_Schinke2015_forsolar.txt"} , disp_vary=False)

MgF2 = layer_cls(name="MgF2", lmd=lmd , d=75.0 , disp_type="Sum" , 
               disp_params={"Tab":"../../data/MgF2/MgF2_thinfilm_Rodriguez2017.txt"} , disp_vary=False)

MoS2 = layer_cls(name="MoS2",lmd=lmd ,d=0.62857151*m,disp_type="Sum" ,
                 disp_params={"Baseline":7.01419247-0j,
                              "Lorentz_1":(1.92000000,4.13728777,0.05000000),
                              "Lorentz_2":(2.05838772,10.1774136,0.15740943),
                              "Lorentz_3":(2.41734278,4.63272964,0.18347886),
                              "Lorentz_4":(2.60013054,4.34979139,0.31233556),
                              "Lorentz_5":(2.76251393,2.81090837,0.50690582)
                              })

# stacks
device_stack = stack_cls(name="device_stack",layers=[Void,MgF2,MoS2,Si,Void],lmd=lmd,AoIs=AoIs)
device_stack_filtered = stack_cls(name="device_stack_filtered",coherent=False,coh_stack = device_stack,filt_lmd_range=10,filt_lmd_num=30)
noAR_stack = stack_cls(name="noAR_stack",layers=[Void,MoS2,Si,Void],lmd=lmd,AoIs=AoIs)
noAR_stack_filtered = stack_cls(name="noAR_stack_filtered",coherent=False,coh_stack = noAR_stack,filt_lmd_range=10,filt_lmd_num=30)
# model
model = model_cls(name="model",lmd=lmd,AoIs=AoIs,stacks={device_stack:1.0})
model.calc_all()
noAR_stack.calc_all()
noAR_stack_filtered.calc_all()
device_stack_filtered.calc_all()
#%% Use the incident spectrum and calculate the optical performance
solar_data = np.loadtxt("solar_AM1.5G_global_tilt.txt")
spectrum_wavelength_data = solar_data[:,0]
spectrum_intensity_data = solar_data[:,1]


# Interpolate and sample to match the spectrum
incident_spectrum = interp1d(spectrum_wavelength_data,spectrum_intensity_data)(lmd)


flux = 100.03693557817755 #mW/cm^2

E_per_photon = (consts.h*1e3)*(consts.c*1e+9)/lmd # Energy of photon in units of mJ, as a fuction of wavelength



incA = (device_stack_filtered.A_glob[0] * incident_spectrum)
incR = (device_stack_filtered.R[0] * incident_spectrum)
incT = (device_stack_filtered.T[0] * incident_spectrum)
# Integrating the spectral fluxes to obtain the total and partial
# fluxes and number of photons.
# The integration must be with Gaussian quadrature of high order
# since the data has noise and the function is not smooth. 
# Simpler methods yeild poor results.
integ_order = 1000
incA_MgF2_total = (calc_integral(lmd,incA[1],lmd[0],lmd[-1],method="Gauss",order=integ_order))
incA_MoS2_total = (calc_integral(lmd,incA[2],lmd[0],lmd[-1],method="Gauss",order=integ_order))
incA_MoS2_suff = (calc_integral(lmd,incA[2],lmd[0],eV_nm(Eg_MoS2),method="Gauss",order=integ_order))
incA_Si_total = (calc_integral(lmd,incA[3],lmd[0],lmd[-1],method="Gauss",order=integ_order))
incA_Si_suff = (calc_integral(lmd,incA[3],lmd[0],eV_nm(Eg_Si),method="Gauss",order=integ_order))
incA_all_total = (incA_MoS2_total+incA_Si_total+incA_MgF2_total)
incA_all_suff = (incA_MoS2_suff+incA_Si_suff)
incR_total = (calc_integral(lmd,incR,lmd[0],lmd[-1]))
incT_total = (calc_integral(lmd,incT,lmd[0],lmd[-1]))

# Photons
phCnt_spectrum_total = (calc_integral(lmd,incident_spectrum/E_per_photon,lmd[0],lmd[-1]))
phCnt_MgF2_total = (calc_integral(lmd,incA[1]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
phCnt_MoS2_total = (calc_integral(lmd,incA[2]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
phCnt_MoS2_suff = (calc_integral(lmd,incA[2]/E_per_photon,lmd[0],eV_nm(Eg_MoS2),method="Gauss",order=integ_order))
phCnt_Si_total = (calc_integral(lmd,incA[3]/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
phCnt_Si_suff = (calc_integral(lmd,incA[3]/E_per_photon,lmd[0],eV_nm(Eg_Si),method="Gauss",order=integ_order))
phCnt_R_total = (calc_integral(lmd,incR/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
phCnt_T_total = (calc_integral(lmd,incT/E_per_photon,lmd[0],lmd[-1],method="Gauss",order=integ_order))
phCnt_all_total = (phCnt_MoS2_total+phCnt_Si_total+phCnt_MgF2_total)
phCnt_all_suff = (phCnt_MoS2_suff+phCnt_Si_suff)

incA_MoS2_useful = (phCnt_MoS2_suff*Eg_MoS2*consts.elementary_charge*1e3)
incA_Si_useful = (phCnt_Si_suff*Eg_Si*consts.elementary_charge*1e3)
incA_all_useful = (incA_MoS2_useful+incA_Si_useful)
    
    #current_density_all
    
#%% Results
plt.close("all")
digits = 5
plot_optical_config(model)
RTAplot = plot_flux(device_stack_filtered,noAR_stack_filtered,selected_AoIs=AoIs,horunits="nm")
absplot = plot_abs_dens(device_stack_filtered,horunits="nm")
print_all_messages()

print "Flux of "+str(flux)+" mW/cm^2 , "+"{:.4e}".format(phCnt_spectrum_total)+" photons/cm^2*sec:"

print "    MoS2 absorbed "+str(round(incA_MoS2_total*100/flux,digits))+"% of the flux , "+str(round(phCnt_MoS2_total*100/phCnt_spectrum_total,digits))+"% of the photons\n        above-gap "+str(round(incA_MoS2_suff*100/flux,digits))+"% of the flux , "+str(round(phCnt_MoS2_suff*100/phCnt_spectrum_total,digits))+"% of the photons\n        useful "+str(round(incA_MoS2_useful*100/flux,digits))+" % of the flux"

print "    Si absorbed "+str(round(incA_Si_total*100/flux,digits))+"% of the flux , "+str(round(phCnt_Si_total*100/phCnt_spectrum_total,digits))+"% of the photons\n        above-gap "+str(round(incA_Si_suff*100/flux,digits))+"% of the flux , "+str(round(phCnt_Si_suff*100/phCnt_spectrum_total,digits))+"% of the photons\n        useful "+str(round(incA_Si_useful*100/flux,digits))+" % of the flux"

print "    MoS2+Si absorbed "+str(round(incA_all_total*100/flux,digits))+"% of the flux, "+str(round(phCnt_all_total*100/phCnt_spectrum_total,digits))+"% of the photons\n        above-gap "+str(round(incA_all_suff*100/flux,digits))+"% of the flux, "+str(round(phCnt_all_suff*100/phCnt_spectrum_total,digits))+"% of the photons\n        useful "+str(round(incA_all_useful*100/flux,digits))+" % of the flux"

print "    Reflection loss "+str(round(incR_total*100/flux,digits))+"% of the flux , "+str(round(phCnt_R_total*100/phCnt_spectrum_total,digits))+"% of the photons"

print "    Transmission loss "+str(round(incT_total*100/flux,digits))+"% of the flux , "+str(round(phCnt_T_total*100/phCnt_spectrum_total,digits))+"% of the photons"

print "    Parasitic absorption loss "+str(round(incA_MgF2_total*100/flux,digits))+"% of the flux , "+str(round(phCnt_MgF2_total*100/phCnt_spectrum_total,digits))+"% of the photons"

print "    Insufficient energy loss "+str(round((incA_all_total-incA_all_suff)*100/flux,digits))+"% of the flux , "+str(round((phCnt_all_total-phCnt_all_suff)*100/phCnt_spectrum_total,digits))+"% of the photons"

print "    Excess energy loss "+str(round((incA_all_suff-incA_all_useful)*100/flux,digits))+"% of the flux"

print "\n"

plt.figure()
labels = ['Reflection loss', 'Transmission loss' , 'Parasitic absorption loss', 'Below bandgap loss', 'Excess energy loss', 'Available for conversion']
sizes = 100*np.array([(incR_total/flux),
                  (incT_total/flux),
                  (incA_MgF2_total/flux),
                  (incA_all_total-incA_all_suff)/flux, 
                  (incA_all_suff-incA_all_useful)/flux, 
                  (incA_all_useful/flux)])
for j,label in enumerate(labels):
    labels[j]=(label+"\n"+str(round(sizes[j],2)))+"%"    
explode = (0.0, 0.0, 0.0, 0.0, 0.0, 0.1)

plt.pie((sizes), explode=explode, labels=labels, autopct=None,
        labeldistance = 1.0, shadow=False, startangle=90)
plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

#plt.show()

plt.title("$\Phi_{in}$ = "+str(flux)+" $\mathrm{mW/cm^2}$")


plt.figure()
plt.title("$\Phi_{in}$ = "+str(flux)+" $\mathrm{mW/cm^2}$")
plt.plot(lmd,incident_spectrum,label="Irradiance (AM1.5G global tilt)",color="black",linestyle='-')
plt.plot(lmd,incA[2],label="Absorbed in $\mathrm{MoS_2}$",color="blue",linestyle='-')
plt.plot(lmd,incA[3],label="Absorbed in Si",color="red",linestyle='-')
plt.plot(lmd,incR,label="Reflection loss",color="orange",linestyle='-')
plt.plot(lmd,incT,label="Transmission loss",color="purple",linestyle='-')
plt.plot(lmd,incA[1],label="Parasitic absorption in MgF2",color="green",linestyle='-')
#plt.grid()
plt.legend()
plt.xlabel("$\lambda$ [nm]",fontsize=12)
plt.ylabel(r"$\Phi_{\lambda}$ $\left[\mathrm{\dfrac{mW}{cm^2 nm}}\right]$",fontsize=12)
#plt.yscale("log")

