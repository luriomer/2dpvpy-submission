#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 10:29:33 2020

@author: omer
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps
from scipy.interpolate import interp1d

fontsize=14


def area_ratio(wvl_interval,wvl,counts):
    ''' Calculate the partial area (between two wavelengths) to total area ratio '''
    min_wvl = wvl_interval[0]
    max_wvl = wvl_interval[1]
    min_i = np.where(wvl==min_wvl)[0][0]
    max_i = np.where(wvl==max_wvl)[0][0]
    
    wvl_part = wvl[min_i:max_i]
    counts_part = counts[min_i:max_i]
    
    Atot = simps(counts,wvl)
    Apartial = simps(counts_part,wvl_part)
    return Apartial/Atot


cut_spectrum = True
cut_range = [350,1000]


# Spectrum from spectrophotometer. Use np.clip to remove negative noise values
data = np.loadtxt("probe_station_lamp_spectra.txt")
wvl = data[:,0]
dial1_raw = np.clip(data[:,1],0,np.inf)
dial2_raw = np.clip(data[:,2],0,np.inf)
dial3_raw = np.clip(data[:,3],0,np.inf)
dial4_raw = np.clip(data[:,4],0,np.inf)

if cut_spectrum:
    min_i = np.where(wvl>=350)[0][0]
    max_i = np.where(wvl<=1000)[0][-1] + 1
    wvl = wvl[min_i:max_i]
    dial1_raw = dial1_raw[min_i:max_i]
    dial2_raw = dial2_raw[min_i:max_i]
    dial3_raw = dial3_raw[min_i:max_i]
    dial4_raw = dial4_raw[min_i:max_i]

# Numerical integration using Simpson’s rule
dial1_integ = simps(dial1_raw,wvl)
dial2_integ = simps(dial2_raw,wvl)
dial3_integ = simps(dial3_raw,wvl)
dial4_integ = simps(dial4_raw,wvl)

# Normalize by the max value to have a total flux of 1 mW/cm^2
dial1_norm = dial1_raw/dial1_integ
dial2_norm = dial2_raw/dial2_integ
dial3_norm = dial3_raw/dial3_integ
dial4_norm = dial4_raw/dial4_integ
average_norm = np.average([dial1_norm,dial2_norm,dial3_norm,dial4_norm],axis=0)



# Flux from fluxmeter [mW/cm^2]
flux_dial1 = 0.64171
flux_dial2 = 1.24268
flux_dial3 = 1.84365
flux_dial4 = 2.89280

# Correct the data so the total area is equal to the measured flux [uW/cm^2nm]
dial1_corr = dial1_norm*flux_dial1
dial2_corr = dial1_norm*flux_dial2
dial3_corr = dial1_norm*flux_dial3
dial4_corr = dial1_norm*flux_dial4

np.save("spectrum_wavelength",wvl)
np.save("spectrum_normalized",average_norm)
np.save("spectrum_0642",dial1_corr)
np.save("spectrum_1243",dial2_corr)
np.save("spectrum_1844",dial3_corr)
np.save("spectrum_2893",dial4_corr)

plt.figure()
'''
plt.subplot(311)
plt.title("Raw",fontsize=fontsize)
plt.plot(wvl,dial1_raw,label="Dial 1")
plt.plot(wvl,dial2_raw,label="Dial 2")
plt.plot(wvl,dial3_raw,label="Dial 3")
plt.plot(wvl,dial4_raw,label="Dial 4")
plt.ylabel("Counts [a.u]",fontsize=fontsize)
plt.grid()
plt.legend()
'''
plt.subplot(211)
plt.title("Normalized",fontsize=fontsize)
plt.plot(wvl,dial1_norm,label=r"$\Phi_{in}$ = 0.642 $\mathrm{{mW}/{cm^2}}$")
plt.plot(wvl,dial2_norm,label=r"$\Phi_{in}$ = 1.243 $\mathrm{{mW}/{cm^2}}$")
plt.plot(wvl,dial3_norm,label=r"$\Phi_{in}$ = 1.844 $\mathrm{{mW}/{cm^2}}$")
plt.plot(wvl,dial4_norm,label=r"$\Phi_{in}$ = 2.893 $\mathrm{{mW}/{cm^2}}$")
#plt.plot(wvl,average_norm,label="Average",color="black",linestyle='--')
plt.ylabel(r"$\dfrac{\Phi_{\lambda}}{\Phi_{in}}$ [$\mathrm{nm^{-1}}$]",fontsize=fontsize)
plt.legend()
plt.grid()


plt.subplot(212)
plt.title("Calibrated",fontsize=fontsize)
plt.plot(wvl,dial1_corr,label=r"$\Phi_{in}$ = 0.642 $\mathrm{{mW}/{cm^2}}$")
plt.plot(wvl,dial2_corr,label=r"$\Phi_{in}$ = 1.243 $\mathrm{{mW}/{cm^2}}$")
plt.plot(wvl,dial3_corr,label=r"$\Phi_{in}$ = 1.844 $\mathrm{{mW}/{cm^2}}$")
plt.plot(wvl,dial4_corr,label=r"$\Phi_{in}$ = 2.893 $\mathrm{{mW}/{cm^2}}$")
plt.ylabel(r"$\Phi_{\lambda}$ $\left[\mathrm{\dfrac{mW}{cm^2 nm}}\right]$",fontsize=fontsize)
plt.xlabel("$\lambda$ [nm]",fontsize=fontsize)
plt.grid()
plt.legend()