#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 10:29:33 2020

@author: omer
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps
from scipy.interpolate import interp1d

fontsize=14


def area_ratio(wvl_interval,wvl,counts):
    ''' Calculate the partial area (between two wavelengths) to total area ratio '''
    min_wvl = wvl_interval[0]
    max_wvl = wvl_interval[1]
    min_i = np.where(wvl==min_wvl)[0][0]
    max_i = np.where(wvl==max_wvl)[0][0]
    
    wvl_part = wvl[min_i:max_i]
    counts_part = counts[min_i:max_i]
    
    Atot = simps(counts,wvl)
    Apartial = simps(counts_part,wvl_part)
    return Apartial/Atot



# Spectrum from spectrophotometer. Use np.clip to remove negative noise values
data = np.loadtxt("solar_AM1.5G_global_tilt.txt")
wvl = data[:,0]
irradiance = data[:,1]


# Numerical integration using Simpson’s rule
irradiance_integ = simps(irradiance,wvl)


plt.figure()
plt.plot(wvl,irradiance)
plt.ylabel(r"Spectral irradiance $\left[\dfrac{mW}{cm^2 nm}\right]$",fontsize=fontsize)
plt.xlabel("$\lambda$ [nm]",fontsize=fontsize)
plt.legend()
plt.grid()

