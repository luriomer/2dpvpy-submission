#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 17 12:51:57 2019

@author: omer
"""

from core.importer import *
sys.path.insert(0,'..')


''' Dispersion formulas. All of them return the dielectric function. '''


def Tab_disp(lmd,filename):
    ''' Tabled dispersion data of (n,k) vs wavelength from a datafile.
    The file format is TSV in [wavelength[nm]|n|k] comments (like source link and text titles) start with '#'.
    The lmd in the datafile is overrided by the input lmd by 
    interpolating and resampling.
    1. the range of the input lmd must be contained inside the datafile lmd! 
        (or else an out-of-interpolation-range error will occur)
    2. k values should be *positive*, N=n-ik convention is built-in already! '''
    datafile = np.loadtxt(filename)
    lmd_dat = datafile[:,0]
    if np.min(lmd) < np.min(lmd_dat):
        raise ValueError("Modeled spectrum is outside the data in "+filename.split("/")[-1]+"! Minimal possible wavelenth is "+str(np.min(lmd_dat))+" nm")
    if np.max(lmd) > np.max(lmd_dat):
        raise ValueError("Modeled spectrum is outside the data in "+filename.split("/")[-1]+"! Maximal possible wavelenth is "+str(np.max(lmd_dat))+" nm")
    n_dat = datafile[:,1]
    k_dat = datafile[:,2]
    n = interp1d(lmd_dat,n_dat)(lmd)
    k = interp1d(lmd_dat,k_dat)(lmd)
    return (n - 1j*k)**2


def Cauchy_disp(lmd,*args):
    ''' Cauchy dispersion for transparent dielectrics.
        Taken wavelength lmd in [nm]. 
        Fujiwara and Collins 2018, page 130
        Currently there is a bug somewhere here...'''
    n = np.zeros_like(lmd,dtype=float)
    for i,arg in enumerate(args):
        n += arg/(lmd**(2*(i+1)))
    return (n-0j)**2
        

def Sellmeier_disp(lmd,C,B):
    ''' Sellmeier dispersion for transparent dielectrics.
        Takes wavelength lmd in [nm]
        Fujiwara and Collins 2018, page 128 '''
    return  0j + B*lmd**2/(lmd**2-C**2)
    
   
def Lorentz_disp(E,C,A,G):
    ''' Lorentz dispersion as a simple quantum viscous oscillator.
        Takes energy E in [eV]
        Fujiwara and Collins 2018, page 132 '''
    return A*C*G/(C**2 - E**2 + 1j*G*E)

   
def Drude_disp(E,A,G):
    ''' Drude dispersion for free carrier absorption.
        Takes energy E in [eV]
        Fujiwara 2007, page 175 '''
    return - A/(E**2-1j*G*E)


def CP_disp(E,C,A,G,P,n):
    ''' Critial Point oscillator for crystalline semiconductors.
        Uses central energy, amplitude, damping, phase and exponent terms.
        Takes energy in [eV]
        Fujiwara and Collins 2018, page 143. Note: due to a convention mistake in the book,
        conj is neccesary to keep the N=n-ik convention. '''
    return np.conj(A*np.exp(1j*P)*(G/(2*C-2*E-1j*G))**n)
