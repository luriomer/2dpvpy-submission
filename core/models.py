# -*- coding: utf-8 -*-
"""
@ Omer Luria
Tel-Aviv University
luriomer@gmail.com

This file contains the model class (a set of stacks) and all its internal methods.
"""

from core.misc import *
sys.path.insert(0,'../..')


class model_cls:
    def __init__(self,**kwargs):
        ''' Configuration parameters '''
        self.init_time = time.clock()
        self.class_name = "model_cls"
        self.name = kwargs["name"]
        self.coherent = kwargs.setdefault("coherent",True)
        
        # If the model is coherent, then we assign wavelength, AoIs and stacks from input.
        # If the model is incoherent, we can only take those values from the original coherent model
        if self.coherent:
            # In the coherent case, we define a model by its structure
            for key in ["lmd","AoIs","stacks"]:
                if key not in kwargs:
                    raise ValueError("missing "+key+" to fully define "+self.name)
                setattr(self,key,kwargs[key])
        else:
            # In the incoherent case, we define a model by its coherent version.
            # We use deepcopy to copy the original attributes, to avoid overwrites when we call reset.
            self.coh_model = kwargs.setdefault("coh_model",None)
            self.filt_lmd_num = kwargs.setdefault("filt_lmd_num",20)
            self.filt_lmd_range = kwargs.setdefault("filt_lmd_range",0.5)
            self.filt_lmd_delta = np.linspace(-self.filt_lmd_range,self.filt_lmd_range,self.filt_lmd_num)
            self.coh_model_copy = copy.deepcopy(self.coh_model)
            if self.coh_model == None:
                raise ValueError(self.name+" was defined as incoherent but no original coherent model was set to average!")
            if self.coh_model.class_name != "model_cls":
                raise TypeError(self.name+"'s coh_model is not a valid model object!")
            for key in ["lmd","AoIs","stacks"]:
                if key in kwargs:
                    raise ValueError("If "+self.name+" is incoherent, "+key+" cannot be passed manually!")
                setattr(self,key,copy.deepcopy(getattr(self.coh_model,key)))
                
        for stack in self.stacks:
            if hasattr(self,"S_"+stack.name):
                raise NameError("Duplicate names in stacks!")
            setattr(self,"S_"+stack.name,self.stacks[stack])
            if not stack.coherent:
                raise TypeError(self.name+" can only include coherent stacks! "+stack.name+" is incoherent.")
        
        self.log_all_iteration_values = kwargs.setdefault("log_all_iteration_values",False)
        self.logging = False
        self.printing = False
              
        check_kwargs(kwargs,["name","lmd","AoIs","stacks","log_all_iteration_values","coherent",
                             "coh_model","filt_lmd_num","filt_lmd_range"],self.name)
        
        # We must reset calculation values to have a prepared arrays with appropriate sizes in each dictionary.
        self.reset()
        
    
    def check_validity(self,printing=True):
        ''' A method to check if the model parameters are all valid.
            If anything is invalid, an error is raised. '''
        
        # Check if sum of all SRs is indeed unity
        if abs(sum(self.stacks.values())-1.0) > high_zero_threshold:
            raise ValueError("Sum of surface coverage ratios must be unity!")
        
        # For the coherent case, check that all stacks have different names 
        # and that the starting and final media are consistent.
        if self.coherent:
            for stack in self.stacks:
                if any(stack.layers[0].N != self.N0):
                    raise ValueError("all stacks in "+self.name+" must have the same starting medium!")
                if any(stack.layers[-1].N != self.Nt):
                    raise ValueError("all stacks in "+self.name+" must have the same final medium!")
                    
            if np.any(np.imag(self.Nt)):
                    add_message("Warning: "+stack.name+" has an abosrbing final medium. Global flux values will not take that into account!")                
            
        for AoI in self.AoIs:
            if not num_type(AoI,"real") or AoI<0:
                raise TypeError("All AoIs in "+self.name+" must be non-negative real numbers!")
        if printing:
            print self.name+"'s validity was checked and confirmed\n"
        
    
    def reset(self,printing=True):
        ''' A method to reset all attributes to the appropriate dimensions (wavelengths, AoIs...),
            syncs between the model and its children. Does not change layers attributes.
            Useful when trying to change number of AoIs/layers/wavelengths in real-time.
            Here it also resets all the stacks and handles all the stack values inside model. '''
        self.E = eV_nm(self.lmd)
        self.rs_tot = {}
        self.rp_tot = {}
        self.ts_tot = {}
        self.tp_tot = {}
        self.rho = {}
        self.psi = {}
        self.delta = {}
        
        self.Rs = {}
        self.Rp = {}
        self.Ts = {}
        self.Tp = {}
        self.As = {}
        self.Ap = {}
        self.R = {}
        self.T = {}
        self.A = {}
        
        self.pseudo_N = {}
        self.pseudo_epsilon = {}
        self.pseudo_alpha = {}
        
        # Preparing zeros in the apropriate sizes, to avoid doing it each time - later it's enough to use .fill()
        for AoI in self.AoIs:
            self.rs_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)
            self.rp_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)
            self.ts_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)
            self.tp_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)
            if AoI != 0:
                self.rho[AoI] = np.zeros_like(self.lmd,dtype=complex)
                self.psi[AoI] = np.zeros_like(self.lmd,dtype=float)
                self.delta[AoI] = np.zeros_like(self.lmd,dtype=float)
                self.pseudo_epsilon[AoI] = np.zeros_like(self.lmd,dtype=complex)
            
            self.Rp[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Rs[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.R[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Tp[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Ts[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.T[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Ap[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.As[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.A[AoI] = np.zeros_like(self.lmd,dtype=float)
        # Resetting the stack and layer level attributes
        # Gather all layers in the model into two lists. One contains all, 
        # the other only the layers which has a tunable dispersion according to the model.
        # Both also includes "external" layers, which may not be explicitly specificed in any stack but contribute to
        # attributes of other layers (e.g a component of an EMA layer which does not appear alone in any stack).
        # Note - when a stack and\or layer are included in a model, their wavelength and energy (and AoI in case of stacks)
        # are ovveridden to be that of the model. So their initial values don't affect the calculation. They do affect
        # in case of other layers (ref etc).
        self.layers = []
        self.dispersed_layers = []
        for stack in self.stacks:
            for layer in stack.layers:
                layer.lmd = self.lmd
                layer.E = self.E
                if layer not in self.layers:
                    self.layers.append(layer)
                    if layer.disp_vary:
                        self.dispersed_layers.append(layer)
                        if hasattr(layer,"components"):
                            for component in layer.components:
                                if component not in self.layers:
                                    self.layers.append(component)
                                if component.disp_vary and component not in self.dispersed_layers:
                                    self.dispersed_layers.append(component)
            stack.AoIs = self.AoIs
            stack.lmd = self.lmd
            stack.E = self.E
            stack.reset_all()
        # Sorting both lists by order of init time, to keep things in order.
        self.layers.sort(key=lambda layer: layer.init_time)
        self.dispersed_layers.sort(key=lambda dispersed_layer: dispersed_layer.init_time)
        for layer in self.layers:
                layer.calc_dispersion()
        self.N0 = self.stacks.keys()[0].layers[0].N
        self.Nt = self.stacks.keys()[0].layers[-1].N
        self.identifiers = []
        self.attributes = np.array([])
        self.num_of_constr = 0
        self.corr_dic = {}
        self.FOM = None
        self.FOM_list = []
        self.iterations = None
        self.check_validity(printing)
        if printing:
            print self.name+" has been reset successfully\n"
    
    
    def calc_response(self,flux=True):
        ''' The response of the entire model (amplitude and flux ratios) '''
        if self.coherent:
            for AoI in self.AoIs:
                self.rs_tot[AoI].fill(0)
                self.rp_tot[AoI].fill(0)
                self.ts_tot[AoI].fill(0)
                self.tp_tot[AoI].fill(0)
                for stack in self.stacks:
                    # Currently, to avoid extreme cases, all stack responses are calculated.
                    # This can be changed in the future if the time complexity will be a problem.
                    stack.calc_response(flux)
                    S = getattr(self,"S_"+stack.name)
                    self.rs_tot[AoI] += stack.rs_tot[AoI]*S
                    self.rp_tot[AoI] += stack.rp_tot[AoI]*S
                    self.ts_tot[AoI] += stack.ts_tot[AoI]*S
                    self.tp_tot[AoI] += stack.tp_tot[AoI]*S
                
                # Ellipsometric quantities only calculated for nonzero AoIs
                # rho is conjugated to return to N=n-ik convention.
                if AoI != 0.0:
                        self.rho[AoI] = np.conj(self.rp_tot[AoI]/self.rs_tot[AoI])
                        self.psi[AoI] = np.rad2deg(np.arctan(np.absolute(self.rho[AoI])))
                        self.delta[AoI] = delta_range_set(np.angle(self.rho[AoI],deg=True))
                        self.pseudo_epsilon[AoI]=np.sin(np.deg2rad(AoI))**2*(1+np.tan(np.deg2rad(AoI))**2 * ((1-self.rho[AoI])/(1+self.rho[AoI]))**2)
                if flux:
                    N0 = np.conj(self.N0)
                    Nt = np.conj(self.Nt)
                    cosphi0 = np.cos(self.stacks.keys()[0].phi[AoI][0,:])
                    cosphi0_conj = np.cos(np.conj(self.stacks.keys()[0].phi[AoI][0,:]))
                    cosphit = np.cos(self.stacks.keys()[0].phi[AoI][-1,:])
                    cosphit_conj = np.cos(np.conj(self.stacks.keys()[0].phi[AoI][-1,:]))
                    # According to Hiemann
                    self.Rs[AoI] = np.abs(self.rs_tot[AoI])**2
                    self.Rp[AoI] = np.abs(self.rp_tot[AoI])**2
                    # Generalizing also for transmittance according to the same logic
                    self.Ts[AoI] = (np.abs(self.ts_tot[AoI])**2) * np.real(Nt*cosphit)/np.real(N0*cosphi0) 
                    self.Tp[AoI] = (np.abs(self.tp_tot[AoI])**2) * np.real(Nt*cosphit_conj)/np.real(N0*cosphi0_conj)
                    # Absorption based on energy balance
                    self.As[AoI] = 1.0 - self.Rs[AoI] - self.Ts[AoI]
                    self.Ap[AoI] = 1.0 - self.Rp[AoI] - self.Tp[AoI]
                    # Assuming the incident light is unpolarized, the energy is divided equally between the two polarizations
                    self.R[AoI] = 0.5*(self.Rs[AoI]+self.Rp[AoI])
                    self.T[AoI] = 0.5*(self.Ts[AoI]+self.Tp[AoI])
                    self.A[AoI] = 1.0 - self.R[AoI] - self.T[AoI]
                
        else:
            self.reset(printing=False)
            for delta_lmd in self.filt_lmd_delta:
                self.coh_model_copy.lmd = self.coh_model.lmd+delta_lmd
                self.coh_model_copy.reset(printing=False)
                self.coh_model_copy.calc_response(flux)
                for AoI in self.AoIs:
                    self.rp_tot[AoI] += self.coh_model_copy.rp_tot[AoI]/float(self.filt_lmd_num)
                    self.rs_tot[AoI] += self.coh_model_copy.rs_tot[AoI]/float(self.filt_lmd_num)
                    self.tp_tot[AoI] += self.coh_model_copy.tp_tot[AoI]/float(self.filt_lmd_num)
                    self.ts_tot[AoI] += self.coh_model_copy.ts_tot[AoI]/float(self.filt_lmd_num)
                    if flux:
                        self.Rp[AoI] += self.coh_model_copy.Rp[AoI]/float(self.filt_lmd_num)
                        self.Rs[AoI] += self.coh_model_copy.Rs[AoI]/float(self.filt_lmd_num)
                        self.Tp[AoI] += self.coh_model_copy.Tp[AoI]/float(self.filt_lmd_num)
                        self.Ts[AoI] += self.coh_model_copy.Ts[AoI]/float(self.filt_lmd_num)
            
            # After we have the averaged spectra we can calculate the rest of the response
            for AoI in self.AoIs:
                if AoI != 0.0:
                    self.rho[AoI] = np.conj(self.rp_tot[AoI]/self.rs_tot[AoI])
                    self.psi[AoI] = np.rad2deg(np.arctan(np.absolute(self.rho[AoI])))
                    self.delta[AoI] = delta_range_set(np.angle(self.rho[AoI],deg=True))
                    self.pseudo_epsilon[AoI]=np.sin(np.deg2rad(AoI))**2*(1+np.tan(np.deg2rad(AoI))**2 * ((1-self.rho[AoI])/(1+self.rho[AoI]))**2)
                if flux:
                    self.R[AoI] = 0.5*(self.Rp[AoI]+self.Rs[AoI])
                    self.T[AoI] = 0.5*(self.Tp[AoI]+self.Ts[AoI])
                    self.Ap[AoI] = 1-self.Rp[AoI]-self.Tp[AoI]
                    self.As[AoI] = 1-self.Rs[AoI]-self.Ts[AoI]
                    self.A[AoI] = 1-self.R[AoI]-self.T[AoI]
            

    def calc_all(self):
        ''' A shortcut to run the entire calculation, also for all the stacks '''
        if self.coherent:
            for stack in self.stacks:
                stack.calc_response()
                stack.calc_abs_dens()
        self.calc_response()
   
        
    def add_parameter(self,iden_dic):
        self.identifiers.append(iden_dic)
    
    
    def load_fit_recipe(self):
        ''' A method that loads attributes and bounds from identifiers nested list, (predifined).
            as a list of lists/tuples, in the form of [(obj,attr_name,(bounds)),..]
            (predefined by user as model.identifiers). This method also validates other predefined values, 
            checks for any problematic and extereme cases, and raises errors to prevent problems. '''
        
        # Make sure the user defined clearly what data to fit and what type of FOM to present
        if not hasattr(self,"FOM_type") or self.FOM_type not in ["CHI2","MSE"]:
            raise ValueError(self.name+": FOM type is not defined properly.")
        if not hasattr(self,"data_types_to_fit") or not (set(self.data_types_to_fit)&set(["Ellipsometry","Reflectance"]) == set(self.data_types_to_fit)):
            raise ValueError(self.name+": data types to fit is not defined properly. Should be a list that contains 'Ellipsometry' and\or 'Reflectance'")
        if not hasattr(self,"fit_method") or self.fit_method not in ['dryrun',
                                                                    'leastsq',
                                                                     'lbfgsb',
                                                                     'nelder',
                                                                     'basinhopping',
                                                                     'trust-ncg',
                                                                     'trust-krylov',
                                                                     'trust-constr',
                                                                     'shgo',
                                                                     'cg',
                                                                     'newton',
                                                                     'cobyla',
                                                                     'bfgs',
                                                                     'differential_evolution',
                                                                     'ampgo',
                                                                     'slsqp',
                                                                     'dual_annealing',
                                                                     'tnc',
                                                                     'trust-exact',
                                                                     'dogleg',
                                                                     'powell',
                                                                     'emcee',
                                                                     'brute',
                                                                     'PbP']:
            raise NameError("fit method invalid!")
        if not hasattr(self,"exp"):
            raise ValueError("There is no exp object assigned to the model! Impossible to fit.")
        
        # Define common wavelengths and AoIs
        self.common_lmd = self.lmd
        if "Ellipsometry" in self.data_types_to_fit:
            # Spectrum and AoIs common with ellipsometry data
            self.common_lmd = common_spectral_range(self.exp.ellipso_lmd,self.lmd)
            self.common_ellipso_AoIs = sorted(list((set(self.AoIs).intersection(set(self.exp.ellipso_AoIs)))))
            self.N_ellipso_AoI = len(self.common_ellipso_AoIs)
        if "Reflectance" in self.data_types_to_fit:
            # Spectrum and AoIs common with reflcetance data
            self.common_lmd = common_spectral_range(self.exp.reflect_lmd,self.common_lmd)
            self.common_reflect_AoIs = sorted(list((set(self.AoIs).intersection(set(self.exp.reflect_AoIs)))))
            self.N_reflect_AoI = len(self.common_reflect_AoIs)
        self.common_E = eV_nm(self.common_lmd)
        
        # For all fit methods excluding PbP, we assign all values and create an LMFIT Parameters class.
        if self.fit_method != "PbP":
            # Make sure there are no duplicates in identifiers (that may also be with different bounds)
            temp_iden = np.array(self.identifiers)
            temp_list = []
            for sett in temp_iden:
                temp_list.append(str(sett["obj"])+"."+str(sett["attr"]))
            if len(set(temp_list)) != len(temp_list):
                raise ValueError("Fit parameter list has duplicates!")
            
            self.params = lmfit.Parameters()
            
            # unpack all the identifiers
            for iden in self.identifiers:
                obj = iden["obj"]
                obj_name = obj.name
                attr_name = iden["attr"]
                param_name = obj_name+"_"+attr_name
                param_value = getattr(obj,attr_name)
                if iden.has_key("min"):
                    minvalue = iden["min"]
                else:
                    minvalue = -np.inf
                
                if (obj!=self) and (obj not in self.layers) and (obj not in self.stacks):
                    raise ValueError(obj_name+" defined as fit parameter but is not included in the model!")
                
                if (attr_name in ["d","mu"] or attr_name[0:3] in ["S_","f_"]) and minvalue<0 and not iden.has_key("constr"):
                    raise ValueError("Invalid negative bounds for "+param_name)
                    
                if iden.has_key("max"):
                    maxvalue = iden["max"]
                else:
                    maxvalue = np.inf
                
                if (attr_name in ["mu"] or attr_name[0:3] in ["S_","f_"]) and maxvalue>1.0 and not iden.has_key("constr"):
                    raise ValueError("Invalid bounds larger than unity for "+param_name)
                
                if attr_name == "mu" and obj.disp_type != "EMA General":
                    raise ValueError(obj.name+": mu can only be fitted for EMA General dispersion!")
                
                if minvalue>maxvalue:
                    raise ValueError("Max value of "+param_name+" must be larger than its min value")
                
                if iden.has_key("constr"):
                    constraint = iden["constr"]
                    self.num_of_constr += 1
                else:
                    constraint = None
                
                if param_value < minvalue or param_value > maxvalue:
                    raise ValueError("Initial guess of "+param_name+" must be inside its bounds!")
                
                self.params.add(name = param_name, value=param_value , min=minvalue , max=maxvalue, expr=constraint)
                self.params[param_name].user_data = {"obj":obj,"attr_name":attr_name}
                if obj.class_name == "layer_cls" and attr_name != "d" and not obj.disp_vary:
                    raise ValueError(obj.name+"'s dispersion was fixed but its dispersion parameters are fitted!")
        
        # For the PbP method, we only define one unknown dispersion and start from one of the edges of the spectrum       
        else:
            if not hasattr(self,"PbP_layer") or self.PbP_layer.class_name!="layer_cls":
                raise ValueError(self.name+" has no properly defined PbP_layer!")
            if self.PbP_layer not in self.layers:
                raise ValueError("PbP fit: "+self.PbP_layer.name+" is not part of "+self.name+"!")
            if not hasattr(self,"PbP_start_from") or self.PbP_start_from not in ["First","Last"]:
                raise ValueError("PbP must have start_from defining the edge to start the fit from. Can be either First or Last")
            
            self.PbP_layer.PbP_N = np.zeros_like(self.common_lmd,dtype=complex)
            self.PbP_layer.PbP_epsilon = np.zeros_like(self.common_lmd,dtype=complex)
            self.PbP_layer.PbP_lmd = self.common_lmd
            self.PbP_layer.PbP_E = self.common_E
            self.PbP_FOM = np.zeros_like(self.common_lmd)
            
            PbP_n = sp.interpolate.interp1d(self.PbP_layer.lmd,np.real(self.PbP_layer.N))(self.common_lmd)
            PbP_k = sp.interpolate.interp1d(self.PbP_layer.lmd,-np.imag(self.PbP_layer.N))(self.common_lmd)
            
            self.params = lmfit.Parameters()
            if self.PbP_start_from == "First":
                self.params.add(name=self.PbP_layer.name+"_n", value = PbP_n[0], min=0)
                self.params.add(name=self.PbP_layer.name+"_k", value = PbP_k[0], min=0)
            elif self.PbP_start_from == "Last":
                self.params.add(name=self.PbP_layer.name+"_n", value = PbP_n[-1], min=0)
                self.params.add(name=self.PbP_layer.name+"_k", value = PbP_k[-1], min=0)
        
        
        print "Fit recipe loaded, initial parameters are set\n"

    def calc_FOM(self,exp,P,C):
        ''' FOM calculation with generalization for multiple AoIs of both Ellipsometry and\or Reflectance simultaneously.
            There is an option to use MSE or CHI2, which is the MSE devided by the measurement errors.
            The default FOM is CHI2, and that is what we use for automated optimization.
            For PbP fit we define the FOM without the normalization factor since we only deal with one point each time.
            This function takes only the common spectral range.
            Input: an exp and stack objects, number of fitted parameters and number of constraints.
            Output: the FOM for the entire spectral range that is in common with exp and stack. '''
            
        
        self.ellipso_residuals_sq = 0
        self.ellipso_FOM = 0
        self.reflect_residuals_sq = 0
        self.reflect_FOM = 0
        self.residuals_sq = 0
        self.FOM = 0
        
        # For PbP fit we only consider one wavelength each time, and do not normalize the DOF
        if self.fit_method == "PbP":
            common_lmd = [self.common_lmd[self.PbP_index]]
            N_common_lmd = 1
            DOF_ellipso = 1
            DOF_reflect = 1
        # For usual fits, we take the whole spectrum and define DOF normalization properly
        else:
            common_lmd = self.common_lmd
            N_common_lmd = len(common_lmd)
            if ("Ellipsometry" in self.data_types_to_fit):
                 DOF_ellipso = self.N_ellipso_AoI*N_common_lmd-P+C-1
            if ("Reflectance" in self.data_types_to_fit):
                DOF_reflect = self.N_reflect_AoI*N_common_lmd-P+C-1
        
        for AoI in self.AoIs:
            # We interpolate and sample to get the two arrays with the same legnth.
            if ("Ellipsometry" in self.data_types_to_fit) and (AoI in self.common_ellipso_AoIs):
                exp_rho = sp.interpolate.interp1d(exp.ellipso_lmd,exp.rho[AoI])(common_lmd)
                exp_rho_err = sp.interpolate.interp1d(exp.ellipso_lmd,exp.rho_err[AoI])(common_lmd)
                model_rho = sp.interpolate.interp1d(self.lmd,self.rho[AoI])(common_lmd)
                if self.FOM_type == "CHI2":
                    self.ellipso_residuals_sq += (np.abs((exp_rho-model_rho)/(exp_rho_err))**2) /(DOF_ellipso)
                elif self.FOM_type == "MSE":
                    self.ellipso_residuals_sq += np.abs(exp_rho-model_rho)**2 /(DOF_ellipso)
                    
            if ("Reflectance" in self.data_types_to_fit) and (AoI in self.common_reflect_AoIs):
                exp_R = sp.interpolate.interp1d(exp.reflect_lmd,exp.R[AoI])(common_lmd)
                exp_R_err = sp.interpolate.interp1d(exp.reflect_lmd,exp.R_err[AoI])(common_lmd)
                model_R = sp.interpolate.interp1d(self.lmd,self.R[AoI])(common_lmd)
                if self.FOM_type == "CHI2":
                    self.reflect_residuals_sq += (np.abs((exp_R-model_R)/(exp_R_err))**2) /(DOF_reflect)
                elif self.FOM_type == "MSE":
                    self.reflect_residuals_sq += np.abs(exp_R-model_R)**2 /(DOF_reflect)
        
        # After calculating the residuals_sq for each one, take the average. We don't use average so the calculation
        # is also valid if one of them is zero, and then len(data_types_to_fit)=1.
        self.residuals_sq = (self.ellipso_residuals_sq + self.reflect_residuals_sq)/len(self.data_types_to_fit)
        self.ellipso_FOM = np.sqrt(np.sum(self.ellipso_residuals_sq))
        self.reflect_FOM = np.sqrt(np.sum(self.reflect_residuals_sq))
        self.FOM = np.sqrt(np.sum(self.residuals_sq))
        
    
    def FitInnerShell(self,params):
        ''' The innershell function. This is the function we minimize in most cases. 
            Input: all the system's attributes plus additional parameters
            Output: total MSE of the tested cofiguration '''
        # Updating values for all attributes. Important! It must explicitly include 'attributes'
        # and not 'model.attributes' or else no local fitting is possible. scipy.optimize knows nothing about out wrappings!
        
        if self.fit_method != "PbP":
            for param in self.params:
                setattr(params[param].user_data["obj"],params[param].user_data["attr_name"],params[param].value) 
            for layer in self.dispersed_layers:
                layer.calc_dispersion() #updating the layer dispersion only if needed!
        else:
            self.PbP_layer.N = params[self.PbP_layer.name+"_n"] -1j * params[self.PbP_layer.name+"_k"]
        
        # Calculating the entire model - amplitude and - if needed - flux
        self.calc_response("Reflectance" in self.data_types_to_fit)
        self.calc_FOM(self.exp,len(params),self.num_of_constr)
        
        # We return the effective residuals, which are the sqrt of the residuals sq (CHI or SIGMA=RMSE).
        # We don't return FOM to be consistent with how LMFIT handels it - it must be an array.
        return np.sqrt(self.residuals_sq)

      
    def each_iteration_call(self,params,iteration,resid):
        ''' Call logprint with the current status to allow user monitoring in real-time.
            This is called every fit iteration, thus it is irrelevant for PbP fit. '''
        message = "Iteration #"+str(iteration)+": FOM = "+str(self.FOM)
        if self.fit_method == "brute":
            self.FOM_list.append(self.FOM)
        if self.log_all_iteration_values:
            message += str(params.valuesdict())
        self.logprint(message)
    
    
    def fit(self):
        ''' A global fitting outershell for easy and modular access to the entire procedure.'''
        
        if self.fit_method == "dryrun":
            self.res = np.sum(self.FitInnerShell(self.params))
            print "Model structure was loaded without fitting\nFOM = "+str(self.FOM)
        
        else:
            if self.params.keys() == []:
                raise ValueError(self.name+" has no variables parameters, impossible to fit!")
            self.printing = True
            self.logprint("----------------------------------------------------------------------------\n"
                       +str(time.ctime())+" - starting "+str(self.fit_method)+" fit for "+self.name
                       +":\n\nInitial guess:\n"+str(self.params))
            
            # If fit method is not PbP, the process is identical - call lmfit.minimize once with the
            # appropriate arguments.
            if self.fit_method != "PbP":
                # Next, we set the kwargs to pass to different minimizers. 
                # We have to manually set it since each type of minimizers requires something different
                if self.fit_method in ["leastsq","differential_evolution"]:
                    f_kwargs = {"maxfev":self.local_maxfev,"ftol":self.ftol,"epsfcn":self.epsfcn}
                
                elif self.fit_method in ["nelder","lbfgsb","bfgs","powell","cg",
                                         "tnc","trust-ncg","trust-exact","trust-krylov",
                                         "trust-constr","dogleg","slsqp","ampgo","shgo",
                                         "emcee","dual_annealing","cobyla","newton"]:
                    f_kwargs = {}
                    
                elif self.fit_method == "basinhopping":
                    # basinhopping supports 
                    f_kwargs = {"minimizer_kwargs":{"method":self.local_basin_method,"options":{"maxiter":self.local_maxfev}},"niter":self.global_niter,"T":self.Tmprtr,"stepsize":self.stepsize}
                    
                elif self.fit_method == "brute":
                    f_kwargs = {"Ns":self.brute_Ns}
                    if len(self.params) >= 4:
                        raise ValueError("Brute fit is restricted to 3 variables or less, to avoid system crashes.")
                
                self.minimizer = lmfit.Minimizer(self.FitInnerShell , self.params ,
                    iter_cb = self.each_iteration_call,
                    **f_kwargs)
                self.res = self.minimizer.minimize(method=self.fit_method)
                
                if self.fit_method == "leastsq":
                    f_message = self.res.message
                else:
                    f_message = ""
                self.logprint(str(time.ctime())+"\nMessage: "+str(lmfit.report_fit(self.res))[0:-4]+": "+f_message+"\n FOM = "+str(self.FOM))
            
                if hasattr(self,"res"):
                    for param in self.res.params:
                        setattr(self.res.params[param].user_data["obj"],self.res.params[param].user_data["attr_name"],self.res.params[param].value)
                    self.calc_all()
            
            # For PbP fit, we call lmfit.minimize for each wavelength seperately and 
            # with different initial guesses
            else:
                for i in range(len(self.common_lmd)):
                    if self.PbP_start_from == "First":
                        self.PbP_index = i
                    elif self.PbP_start_from == "Last":
                        self.PbP_index = -i-1
                    self.logprint("Fitting "+str(round(self.common_lmd[self.PbP_index],2))+"nm...",linebreak=False)
                    self.minimizer = lmfit.Minimizer(self.FitInnerShell , self.params)
                    self.res = self.minimizer.scalar_minimize()
                    self.PbP_layer.PbP_N[i] = self.res.params[self.PbP_layer.name+"_n"].value -1j * self.res.params[self.PbP_layer.name+"_k"].value
                    self.params[self.PbP_layer.name+"_n"].value  = self.res.params[self.PbP_layer.name+"_n"].value
                    self.params[self.PbP_layer.name+"_k"].value  = self.res.params[self.PbP_layer.name+"_k"].value
                    self.PbP_FOM[i] = self.FOM
                    self.logprint("FOM = "+str(self.FOM))
                self.PbP_layer.PbP_epsilon = self.PbP_layer.PbP_N**2
                self.PbP_layer.calc_dispersion()
                self.calc_all()
        self.logging = False
        self.printing = False
        
        
    def calc_2d_conf_interval(self,par1,par2,npoints=[30,30]):
        ''' Calculates the confidence interval between two fitted parameters. 
            par1 and par2 are strings by the format of objname_attribute'''
        if not hasattr(self,"res") or not hasattr(self,"minimizer"):
            print "Error: please do leastsq fit before confidence analysis"
        else:
            print "Calculating confidence intervals, please wait..."
            cx, cy, grid = lmfit.conf_interval2d(self.minimizer, self.res, par1, par2, npoints[0], npoints[1])
            self.corr_dic[par1+"--"+par2] = [cx,cy,grid]
    

    def plot_2d_conf_interval(self,par1,par2):
        plt.figure()
        plt.title("Sensitivity analysis")
        cx = self.corr_dic[par1+"--"+par2][0]
        cy = self.corr_dic[par1+"--"+par2][1]
        grid = self.corr_dic[par1+"--"+par2][2]
        
        plt.contourf(cx, cy, grid, np.linspace(0, 1, 22))
        
        plt.grid()
        clb = plt.colorbar(ticks=np.arange(0,1.1,0.05))
        clb.ax.set_title("Probability")
        unit1 = get_units(self.params[par1].user_data["attr_name"])
        unit2 = get_units(self.params[par2].user_data["attr_name"])
        plt.xlabel(par1+" "+unit1)
        plt.ylabel(par2+" "+unit2)
        #conf_90 = plt.contour(cx, cy,grid,levels = [0.9],
        #         colors=('k',),linestyles=('-',),linewidths=(1,))
        #plt.clabel(conf_90, colors = 'k', fontsize=14)
        plt.plot(self.params[par1].value,self.params[par2].value,marker='o',color='red',markersize=5.0)
    
    
    def analyze_2d_conf(self,par1,par2,npoints=[30,30]):
        self.calc_2d_conf_interval(par1,par2,npoints)
        self.plot_2d_conf_interval(par1,par2)
    
    
    def logprint(self,printed_str,**kwargs):
        ''' Log and print strings. The logfile is saved in the script folder.
            The default behavior is to print each time in newline. '''
        output_filename = kwargs.setdefault("output_filename",None)
        linebreak = kwargs.setdefault("linebreak",True)
        if output_filename == None:
            output_filename = self.name+"_logfile.log"
        if self.printing:
            if linebreak:
                print printed_str
            else:
                print printed_str,
        if self.logging:
            logfile=open(output_filename,'a')
            if linebreak:
                printed_str += '\n'
            logfile.write(printed_str)
            logfile.close()
  
    
    def save_PbP_data(self,prefix=None):
        ''' Save the spectral PbP dispersion (lmd|n|k) and residuals (lmd|FOM) '''
        if prefix == None:
            prefix = self.PbP_layer.name
        save_data(prefix+"_PbP_N.txt",[self.PbP_layer.PbP_lmd,np.real(self.PbP_layer.PbP_N),-np.imag(self.PbP_layer.PbP_N)])
        save_data(prefix+"_PbP_FOM.txt",[self.PbP_layer.PbP_lmd,self.PbP_FOM])