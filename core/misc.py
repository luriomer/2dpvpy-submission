# -*- coding: utf-8 -*-
"""
@ Omer Luria
Tel-Aviv University
luriomer@gmail.com

This file contains a set of of miscellaneous tools and functions, used throughout the program.
"""

from core.importer import *

# Precision errors
high_zero_threshold = 1e-4
low_zero_threshold = 1e-20

class user_msg:
    def __init__(self):
        self.messages = []
        

def add_message(message):
    if not hasattr(user_msg,"messages"):
        user_msg.messages = []
    if message not in user_msg.messages:
        user_msg.messages.append(message)


def print_all_messages():
    print "\nUser warnings and messages:\n-----------------------------"
    if hasattr(user_msg,"messages"):
        for message in user_msg.messages:
            print message+"\n"
    else:
        print "There are no recorded messages"


def check_kwargs(kwargs,known_list,func_name):
    for kwarg in kwargs:
        if kwarg not in known_list:
            print "\n"+func_name+": kwarg "+kwarg+" unrecognized\n"


def logisticspace(minvalue,maxvalue,npoints,fac=10):
    ''' Get logistic spaced points between two values. fac is the steepness of the curve '''
    x = np.linspace(-1,1,npoints)
    logistic = 1/(1+np.exp(-fac*x))
    return logistic*(maxvalue-minvalue)+minvalue


def calc_integral(x,y,a,b,method="Simpson",order=5):
    ''' Numerically calculate a definite integral between a and b.
        The default option is to use interpolation and resampling and integrate using Simpson,
        but it's also possible to use the fixed-order Gaussian quadrature. '''
    # Check that a and b are in the range
    if a<np.min(x):
        raise ValueError("a is below the x range")
    if b>np.max(x):
        raise ValueError("b is above the x range")
    y_interp = interp1d(x,y)
    
    if method =="Simpson":
        x_new = x
        # Plug in a and b
        if a not in x:
            x_new = np.sort(np.append(x_new,a))
        if b not in x:
            x_new = np.sort(np.append(x_new,b))
        y_new = y_interp(x_new)
        return sp.integrate.simps(y_new,x_new)
    
    elif method == "Gauss":
        return sp.integrate.fixed_quad(y_interp,a,b,n=order)[0]


def delta_range_set(delta):
    ''' Set delta's range to [-180deg,180deg]. 
    Input: delta in [deg] in arbitrary region 
    Output: delta in [deg] in the region [-180deg,180deg]'''
    for i in range(len(delta)):
        delta[i] = delta[i]%360
        if delta[i] > 180:
            delta[i]-=360
        elif delta[i] <=-180:
            delta[i] +=360
    return delta


def common_spectral_range(lmd1,lmd2,inc="max"):
    ''' When we need a common spectrum between two spectra, this is the range
    between the maximal minimum and minimal maximum. 
    We automatically take the increment to be the max of the two spectra, but it can also be the min '''
    if inc not in ["min","max"]:
        raise TypeError("common_spectral_range inc argument must be either 'max' or 'min'!")
    lmd_min = np.max([np.min(lmd1),np.min(lmd2)])
    lmd_max = np.min([np.max(lmd1),np.max(lmd2)])
    if inc=="max":
        delta_lmd = np.max(np.concatenate([np.diff(lmd1),np.diff(lmd2)]))
    elif inc=="min":
        delta_lmd = np.min(np.concatenate([np.diff(lmd1),np.diff(lmd2)]))
    common_lmd = np.arange(lmd_min,lmd_max,delta_lmd)
    return common_lmd


def eV_nm(E_in_eV):
    ''' Convert photon energy in [eV] to wavelength in [nm] and vice versa '''
    return consts.h*consts.c/(consts.e*E_in_eV*1.0e-9)


def epsilon_to_N(epsilon):
    epsil1 = np.real(epsilon)
    epsil2 = np.imag(epsilon)
    N = np.sqrt(0.5*(epsil1+np.sqrt(epsil1**2+epsil2**2)))-1j*np.sqrt(0.5*(-epsil1+np.sqrt(epsil1**2+epsil2**2)))
    return N


def get_units(attr_name):
    if attr_name == "d":
        return "[nm]"
    else:
        return ""


def num_type(num,test_type):
    ''' A useful function to check whether a number is an integer, real, complex etc '''
    if test_type == "integer":
        return (type(num) in [int, np.int,np.int8,np.int16,np.int32,np.int64])
    elif test_type == "real":
        return (type(num) in [int, np.int,np.int8,np.int16,np.int32,np.int64,
                              float, np.float,np.float16,np.float32,np.float64,np.float])
    elif test_type == "complex":
        return (type(num) in [int, np.int,np.int8,np.int16,np.int32,np.int64,
                              float, np.float,np.float16,np.float32,np.float64,np.float,
                              complex,np.complex,np.complex64])
    

def sort_by_init_time(iterable):
    ''' A function that gets iterable (list or dicts) as in input as returns a list by order of init time of each object. 
        In the case of dicts, it has to be in the form of {object:some_value} '''
    sorted_list = []
    time_list = []
    if type(iterable) == dict:
        for obj in iterable:
            sorted_list.append(obj)
            time_list.append(obj.init_time)
    if type(iterable) == list:
        for obj in iterable:
            sorted_list.append(obj)
            time_list.append(obj.init_time)
    
    time_list,sorted_list = zip(*sorted(zip(time_list,sorted_list)))  
    return sorted_list


def save_data(filename,array_list):
    ''' Saves a file of arrays. array_list will be saved vertically, with the same order,
        for example if we want to save dispersion, then array_list=[layer.lmd,np.real(layer.N),-np.imag(layerN)].
        All arrays must be with the same shape. '''
    array_length = len(array_list[0])
    for array in array_list:
        if len(array) != array_length:
            raise ValueError("save_data: not all arrays are with the same length!")
    data_array = np.array(array_list)
    data_array = np.transpose(data_array)
    np.savetxt(filename,data_array,fmt="%f")
    

def print_attributes(obj,**kwargs):
    ''' Prints all attributes of an object. 
        In the default behavio no multivalues are printed (arrays,lists,tuples,dicts etc). '''
    print_all = kwargs.setdefault("print_all",False)
    precision = kwargs.setdefault("precision",8)
    for attr in obj.__dict__:
        value = getattr(obj,attr)
        if not print_all:
            if type(value) in [int,float,complex,np.float16,np.float32,np.float64]:
                print attr+" : "+str(round(value,precision))
        else:
            print attr+" : "+str(round(value,precision))


def Snell_branch_cuts(chis,Ns):
    ''' A function that gets chi=N*sin(phi) and returns the right phi, 
        taking into account the right branch cut for arcsin. The cases are just according to Byrnes. '''
    phis = sp.arcsin(chis/Ns)
    i=0
    for chi,N,phi in zip(chis,Ns,phis):
        cosphi = np.cos(phi)
        switch_phi = False
        
        # Case D.2 Im(N)>0 and Re(N)>0
        if np.real(N)>0 and np.imag(N)>0 and np.imag(N*cosphi)<0:
            switch_phi = True
        
        # Case D.3
        elif np.imag(N)==0 and np.real(N)>0:
            if chi>np.real(N) and np.real(cosphi)==0 and np.imag(N*cosphi)<0:
                switch_phi = True
            if chi<N and np.imag(cosphi)==0 and N*cosphi<0:
                switch_phi = True
        
        # Case D.4
        elif np.imag(N)<0 and np.real(N)<0 and np.imag(N*cosphi)<0:
            switch_phi = True
        
        if switch_phi:
            phi = np.pi- phi
        
        phis[i] = phi
        i += 1
    return phis