#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@ Omer Luria
Tel-Aviv University
luriomer@gmail.com

This file contains the exp class (experimental data objects).
"""
from core.misc import *
sys.path.insert(0,'..')


class exp_cls:
    def __init__(self,**kwargs):
        ''' Initialize all experimental data for the sample.
            For ellipso and spectro param tuples, there are three sub-tuples: filepaths, 
            AoIs and filter params. For Raman and AFM there are only filepaths and filter params.
            Initializations are made using the following methods.'''
        
        self.init_time = time.clock()
        self.class_name = "exp_cls"
        self.name = kwargs["name"]
        
        if type(self.name)!=str:
            raise TypeError("exp_cls's name must be a string!")
        
        check_kwargs(kwargs,["name"],self.name)
        

    def add_ellipso_data(self,data_filepath,**kwargs):
        ''' Add ellipsometry data from a *.dat file. skiprows=4 is default according to WVASE format '''
        
        check_kwargs(kwargs,["excluded_AoIs","rows_to_skip"],"add_ellipso_data")
        self.ellipso_filepath = data_filepath
        
        rows_to_skip = kwargs.setdefault("rows_to_skip",0)
        
        self.psi = {}
        self.psi_err = {}
        self.delta = {}
        self.delta_err = {}
        self.rho = {}
        self.rho_err = {}
        self.pseudo_epsilon_err = {}
        
        self.ellipso_data = np.loadtxt(self.ellipso_filepath,skiprows=rows_to_skip)
        self.ellipso_lmd = np.array(sorted(list(set(self.ellipso_data[:,0]))))
        self.ellipso_E = (consts.h * consts.c)/(consts.e * self.ellipso_lmd*1e-9)
        self.ellipso_AoIs = sorted(list(set(self.ellipso_data[:,1])))
        self.ellipso_excluded_AoIs = kwargs.setdefault("excluded_AoIs",[])
        
        l = len(self.ellipso_lmd)
        
        for i,AoI in enumerate(self.ellipso_AoIs):
                self.psi[AoI] = self.ellipso_data[i*l:(i+1)*l,2]
                #Make sure delta is in the range [-180deg,180deg], using the delta_range_set function
                self.delta[AoI] = delta_range_set(self.ellipso_data[i*l:(i+1)*l,3])
                self.psi_err[AoI] = self.ellipso_data[i*l:(i+1)*l,4]
                self.delta_err[AoI] = self.ellipso_data[i*l:(i+1)*l,5]
                
                self.rho[AoI]=np.tan(np.deg2rad(self.ellipso_data[i*l:(i+1)*l,2]))*\
                                np.exp(1j*np.deg2rad(self.ellipso_data[i*l:(i+1)*l,3]))
                
                # Calculating the measurement error in rho, using first order partial derivatives
                rho_ind_err = np.exp(1j*np.deg2rad(self.delta[AoI]))*(np.deg2rad(self.psi_err[AoI])/np.cos(np.deg2rad(self.psi[AoI]))**2 + 1j*np.tan(np.deg2rad(self.psi[AoI]))*np.deg2rad(self.delta_err[AoI]))
                self.rho_err[AoI] = rho_ind_err
        
        if self.ellipso_excluded_AoIs != []:
            for ex_AoI in self.ellipso_excluded_AoIs:
                if ex_AoI in self.ellipso_AoIs:
                    self.ellipso_AoIs.remove(ex_AoI)
                    add_message("Warning: Ellipsometry data was manually excluded from exp and will not contribute to fit procedure.")
        
        if self.ellipso_AoIs == []:
            add_message("Warning: "+self.name+"'s ellispo AoI list is empty.")
        
        self.calc_pseudo_dispersion()
        
        # Calculating the measurement erorr in pseudo epsilon, using a first order partial derivative and rho values
        for AoI in self.ellipso_AoIs:
            self.pseudo_epsilon_err[AoI] = -4 * np.sin(np.deg2rad(AoI))**2 * np.tan(np.deg2rad(AoI))**2 * ((1-self.rho[AoI])/(1+self.rho[AoI])**3)*self.rho_err[AoI]


    def add_reflect_data(self,data_filepath,**kwargs):
        ''' Add reflectance data from a *.dat file. 
            skiprows=4 and usecol=(1,2,3) are default according to WVASE format to avoid reading strings. 
            We assume here that all the data is consistent - exactly the same wavelengths and AoIs, or else
            it's messy. '''
        
        check_kwargs(kwargs,["excluded_AoIs","rows_to_skip","usecols"],"add_reflect_data")
        self.reflect_filepath = data_filepath
        
        if type(self.reflect_filepath)!=str:
            raise ValueError("Reflectance data must be supplied, and must be a string!")

        rows_to_skip = kwargs.setdefault("rows_to_skip",0)
        usecols = kwargs.setdefault("usecols",(1,2,3,4))
        
        self.Rp = {}
        self.Rp_err = {}
        self.Rs = {}
        self.Rs_err = {}
        self.R = {}
        self.R_err = {}
        
        self.reflect_data = np.loadtxt(self.reflect_filepath,skiprows=rows_to_skip,usecols=usecols)
        self.reflect_lmd = np.array(sorted(list(set(self.reflect_data[:,0]))))
        self.reflect_E = (consts.h * consts.c)/(consts.e * self.reflect_lmd*1e-9)
        self.reflect_AoIs = sorted(list(set(self.reflect_data[:,1])))
        self.reflect_excluded_AoIs = kwargs.setdefault("excluded_AoIs",[])
        
        l = len(self.reflect_lmd)
        m = len(self.reflect_AoIs)
        for i,AoI in enumerate(self.reflect_AoIs):
                self.Rp[AoI] = self.reflect_data[i*l:(i+1)*l,2]
                self.Rp_err[AoI] = self.reflect_data[i*l:(i+1)*l,3]
                
                self.Rs[AoI] = self.reflect_data[(i+m)*l:(i+1+m)*l,2]
                self.Rs_err[AoI] = self.reflect_data[(i+m)*l:(i+1+m)*l,3]
                
                self.R[AoI] = 0.5*(self.Rp[AoI]+self.Rs[AoI])
                self.R_err[AoI] = 0.5*(self.Rp_err[AoI]+self.Rs_err[AoI])
                
        if self.reflect_excluded_AoIs != []:
            for ex_AoI in self.reflect_excluded_AoIs:
                if ex_AoI in self.reflect_AoIs:
                    self.reflect_AoIs.remove(ex_AoI)
                    add_message("Warning: Reflectance data was manually excluded from exp and will not contribute to fit procedure.")
        
        if self.reflect_AoIs == []:
            add_message("Warning: "+self.name+"'s reflect AoI list is empty.")
    

    def calc_pseudo_dispersion(self):
        ''' Calculates the pseudo dispersion based on ellipsometry data '''
        if hasattr(self,"ellipso_data"):
            self.pseudo_epsilon = {}
            for AoI in self.ellipso_AoIs:
                if AoI != 0.0:
                    self.pseudo_epsilon[AoI]=np.sin(np.deg2rad(AoI))**2*(1+np.tan(np.deg2rad(AoI))**2 * ((1-self.rho[AoI])/(1+self.rho[AoI]))**2)
            
        else:
            add_message("Warning: failed to calculate pseudo dispersion for "+self.name+". No ellipsometry data loaded!")
    
    
    def add_raman_data(self,raman_params):
        ''' Add raman data from file and define a lowess filter,
        using params in the form of [filepaths,(to_filer,fr_filter,t_filter)]
        filepaths is tuple/list '''
        self.raman_filepaths = raman_params[0]
        self.raman_to_filter = raman_params[1][0]
        self.raman_fr_filter = raman_params[1][1]
        self.raman_t_filter = raman_params[1][2]
    
        self.raman_data = []
        self.raman_wvnmbr = []
        self.raman_inten = []
            
        for i in range(len(self.raman_filepaths)):
            self.raman_data.append(np.loadtxt(self.raman_filepaths[i],skiprows=2)) #Raman data  wavnmbr|Raman Shift [cm^-1]
            self.raman_wvnmbr.append(self.raman_data[i][:,0]) # Raman data spectral range in [cm^-1]
            self.raman_inten.append(self.raman_data[i][:,1]) # Raman data scattering intensity in arbitrary units

