#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
@ Omer Luria
Tel-Aviv University
luriomer@gmail.com

This file contains the stack class (a set of layers) and all its internal methods.
The calculations of amplitude and flux attribuites are based on classical electromagnetic equations
and the tmm (transfer-matrix method), and are consistent with
O.S.Heavens,"Optical properties of thin solid films", pages 46-74.
"""

from core.importer import *
from core.misc import *
sys.path.insert(0,'..')


class stack_cls:
    def __init__(self,**kwargs):
        ''' Configuration parameters '''
        self.init_time = time.clock()
        self.class_name = "stack_cls"
        self.name = kwargs["name"]
        self.coherent = kwargs.setdefault("coherent",True)
        
        if self.coherent:
            # In the coherent case, we define a stack by its structure
            for key in ["lmd","AoIs","layers"]:
                if key not in kwargs:
                    raise ValueError("missing "+key+" to fully define "+self.name)
                setattr(self,key,kwargs[key])
                
        else:
            # In the incoherent case, we define a stack by its coherent version.
            # We use deepcopy to copy the original attributes, to avoid overwrites when we call reset methods.
            self.coh_stack = kwargs.setdefault("coh_stack",None)
            self.filt_lmd_num = kwargs.setdefault("filt_lmd_num",20)
            self.filt_lmd_range = kwargs.setdefault("filt_lmd_range",0.5)
            self.filt_lmd_delta = np.linspace(-self.filt_lmd_range,self.filt_lmd_range,self.filt_lmd_num)
            self.coh_stack_copy = copy.deepcopy(self.coh_stack)
            if self.coh_stack == None:
                raise ValueError(self.name+" was defined as incoherent but no original coherent stack was set to average!")
            if self.coh_stack.class_name != "stack_cls":
                raise TypeError(self.name+"'s coh_model is not a valid model object!")
            for key in ["lmd","AoIs","layers"]:
                if key in kwargs:
                    raise ValueError("If "+self.name+" is incoherent, "+key+" cannot be passed manually!")
                setattr(self,key,copy.deepcopy(getattr(self.coh_stack,key)))
            
            print "Warning: incoherent stack calculations have high time complexity, and should only be carried out to obtain an estimation for the absorbed power density in incoherent cases. "
            
        self.E = (consts.h * consts.c)/(consts.e * self.lmd*1e-9)
        if type(self.layers) != list:
            raise TypeError("stack configuration is invalid: layers must be a list of layer instances")
        
        
        check_kwargs(kwargs,["name","lmd","AoIs","layers","coherent","coh_stack","filt_lmd_num","filt_lmd_range"],self.name)

        # We must reset all values to have a prepared arrays with appropriate sizes in each dictionary.
        self.reset_all()
         

    def check_validity(self):
        ''' A method to check if the stack parameters are all valid.
            If anything is invalid, an error is raised. '''
        
        for layer in self.layers:
            if layer.class_name != "layer_cls":
                raise TypeError("Invalid non-layer object in "+self.name+"'s list")
        
        for AoI in self.AoIs:
            if not num_type(AoI,"real") or AoI<0:
                raise TypeError("All AoIs in "+self.name+" must be non-negative real numbers!")
        

    def reset_response(self):
        ''' Reset amplitude calculated values '''
        self.check_validity()
        self.beta = {}
        self.phi = {}
        self.chi = {}
        self.kz = {}
        
        self.rs = {}
        self.rp = {}
        self.ts = {}
        self.tp = {}
        
        self.Msj = [[1,0],[0,1]]
        self.Mpj = [[1,0],[0,1]]
        self.Ms = [[1,0],[0,1]]
        self.Mp = [[1,0],[0,1]]
        self.Ms_tmp = [[1,0],[0,1]]
        self.Mp_tmp = [[1,0],[0,1]]
        
        self.rs_tot = {}
        self.rp_tot = {}
        self.ts_tot = {}
        self.tp_tot = {}
        self.rho = {}
        self.psi = {}
        self.delta = {}
        
        self.pseudo_N = {}
        self.pseudo_epsilon = {}
        
        self.Rs = {}
        self.Rp = {}
        self.Ts = {}
        self.Tp = {}
        self.As = {}
        self.Ap = {}
        self.R = {}
        self.T = {}
        self.A = {}
        
        for AoI in self.AoIs:
            # Layer-level attributes, so 2d arrays for [layer,wavelength]
            self.beta[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.phi[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.rp[AoI] = np.zeros([len(self.layers)-1,len(self.lmd)],dtype=complex)
            self.rs[AoI] = np.zeros([len(self.layers)-1,len(self.lmd)],dtype=complex)
            self.tp[AoI] = np.zeros([len(self.layers)-1,len(self.lmd)],dtype=complex)
            self.ts[AoI] = np.zeros([len(self.layers)-1,len(self.lmd)],dtype=complex)
            self.kz[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)

            self.rp_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)
            self.rs_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)
            self.tp_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)
            self.ts_tot[AoI] = np.zeros_like(self.lmd,dtype=complex)

            self.Rp[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Rs[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.R[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Tp[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Ts[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.T[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.Ap[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.As[AoI] = np.zeros_like(self.lmd,dtype=float)
            self.A[AoI] = np.zeros_like(self.lmd,dtype=float)  

        
    def reset_abs_dens(self):
        ''' Reset absorbed density calculated values '''
        self.check_validity()
        self.vs = {}
        self.ws = {}
        self.vp = {}
        self.wp = {}
        
        self.C1s_loc = {}
        self.C2s_loc = {}
        self.C3s_loc = {}
        self.as_loc = {}
        self.ap_loc = {}
        self.C1p_loc = {}
        self.C2p_loc = {}
        self.C3p_loc = {}
        self.a_loc = {}
        self.As_glob = {}
        self.Ap_glob = {}
        self.A_glob = {}
        for AoI in self.AoIs:
            # Layer-level attributes, so 2d arrays for [layer,wavelength]
            self.vs[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.ws[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.vp[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.wp[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            
            self.C1s_loc[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=float)
            self.C2s_loc[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=float)
            self.C3s_loc[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.C1p_loc[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=float)
            self.C2p_loc[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=float)
            self.C3p_loc[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.As_glob[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.Ap_glob[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)
            self.A_glob[AoI] = np.zeros([len(self.layers),len(self.lmd)],dtype=complex)

            
    def reset_all(self):
        ''' Reset all calculated values. Does not change layer attributes.
            Useful when trying to change number of AoIs/layers/wavelengths in real-time. '''
        self.check_validity()
        self.reset_response()
        self.reset_abs_dens()


    def calc_response(self,flux=True):
        ''' Main calculation of the structure optical response - amplitude ratios.
            This is generally consistent with both O.S. Heavens and S.J. Byrnes. Pay attention
            that we use N=n-ik so N is conjugated to that of Byrnes.
            phi is the propagation angle, beta is the phase thickness (delta in common literature). '''
        
        if self.coherent:
            for AoI in self.AoIs:
                self.phi[AoI][0,:]=np.deg2rad(AoI) # The starting propagation angle is AoI.
                self.chi[AoI] = np.conj(self.layers[0].N)*np.sin(np.deg2rad(AoI)) #Snell term - depends only on AoI.
                
                # Resetting matrices, so AoIs will not affect each other. This is done manually to reduce complexity.
                # Since we need to do it for each AoI again, it's done here and not in reset_amplitude()
                self.Ms[0][0]=1
                self.Ms[0][1]=0
                self.Ms[1][0]=0
                self.Ms[1][1]=1
                
                self.Mp[0][0]=1
                self.Mp[0][1]=0
                self.Mp[1][0]=0
                self.Mp[1][1]=1
                
                for j in range(len(self.layers)-1):
                    # For each interface, setting the fresnel terms. 
                    # We conjugate N to be consistent with Byrnes although we use the opposite convention.
                    # Inteface j is for layers j,j+1. This is unlike O.S.Heavens
                    d1 = self.layers[j].d
                    N1 = np.conj(self.layers[j].N)
                    N2 = np.conj(self.layers[j+1].N)
                    
                    #if j == 0:
                    #    self.phi[AoI][j] = Snell_branch_cuts(self.chi[AoI],N1)
                    #else:
                    self.phi[AoI][j] = sp.arcsin(self.chi[AoI]/N1)
                    
                    #if j == len(self.layers)-2:
                    #    self.phi[AoI][j+1] = Snell_branch_cuts(self.chi[AoI],N2)
                    #else:
                    self.phi[AoI][j+1] = sp.arcsin(self.chi[AoI]/N2)
                    
                    cosphi1 = np.cos(self.phi[AoI][j])
                    cosphi2 = np.cos(self.phi[AoI][j+1])
                    
                    
                    self.kz[AoI][j] = 2.0*np.pi * N1 * cosphi1/self.lmd
                    self.beta[AoI][j] = self.kz[AoI][j] * d1
                    
                    # As was done by S.Byrnes in "tmm" code, for very opaque layers we get very high 
                    # phase thickess,therefore very low transmitance. 
                    # This causes division by zero in the matrices up next. So we truncate the 
                    # imaginary part in 35.0, which only allows 1 photon out or 1e30 in a single pass. 
                    # It does not change the actual values we calculate in any useful manner.
                    self.beta[AoI][j] = np.real(self.beta[AoI][j]) + 1j*np.clip(np.imag(self.beta[AoI][j]),None,35.0)
                    if np.isin(35.0,np.imag(self.beta[AoI][j])):
                        add_message("Note: "+self.layers[j].name+" is opaque in some wavelengths, so its phase thickness was truncated for numerical stability.")
                    
                    self.rs[AoI][j] = (N1*cosphi1-N2*cosphi2)/(N1*cosphi1+N2*cosphi2)
                    
                    self.rp[AoI][j] = (N2*cosphi1-N1*cosphi2)/(N2*cosphi1+N1*cosphi2)
                    
                    self.ts[AoI][j] = (2*N1*cosphi1)/(N1*cosphi1+N2*cosphi2)
    
                    self.tp[AoI][j] = (2*N1*cosphi1)/(N2*cosphi1+N1*cosphi2)
                    
                    # The matrices are defined as lists, multiplication is done manually to save spectral loops.
                    # tmp is to transfer the data properly. 
                    # Note that substitution should be term-by-term to avoid pointer confusion, since lists are mutable!
                    
                    # The current layer s matrix
                    if j == 0:
                        self.Msj[0][0] = 1.0/self.ts[AoI][j]
                        self.Msj[0][1] = self.rs[AoI][0]/self.ts[AoI][0]
                        self.Msj[1][0] = self.rs[AoI][0]/self.ts[AoI][0]
                        self.Msj[1][1] = 1.0/self.ts[AoI][j]
                    else:
                        self.Msj[0][0] = np.exp(-1j*self.beta[AoI][j])/self.ts[AoI][j]
                        self.Msj[0][1] = self.rs[AoI][j]*np.exp(-1j*self.beta[AoI][j])/self.ts[AoI][j]
                        self.Msj[1][0] = self.rs[AoI][j]*np.exp(1j*self.beta[AoI][j])/self.ts[AoI][j]
                        self.Msj[1][1] = np.exp(1j*self.beta[AoI][j])/self.ts[AoI][j]
                    
                    # Matrix multiplication of s polarization
                    self.Ms_tmp[0][0] = self.Ms[0][0]*self.Msj[0][0] + self.Ms[0][1]*self.Msj[1][0]
                    self.Ms_tmp[0][1] = self.Ms[0][0]*self.Msj[0][1] + self.Ms[0][1]*self.Msj[1][1]
                    self.Ms_tmp[1][0] = self.Ms[1][0]*self.Msj[0][0] + self.Ms[1][1]*self.Msj[1][0]
                    self.Ms_tmp[1][1] = self.Ms[1][0]*self.Msj[0][1] + self.Ms[1][1]*self.Msj[1][1]
                    self.Ms[0][0] = self.Ms_tmp[0][0]
                    self.Ms[0][1] = self.Ms_tmp[0][1]
                    self.Ms[1][0] = self.Ms_tmp[1][0]
                    self.Ms[1][1] = self.Ms_tmp[1][1]
                    
                    # The current layer p matrix
                    if j == 0:
                        self.Mpj[0][0] = 1.0/self.tp[AoI][0]
                        self.Mpj[0][1] = self.rp[AoI][0]/self.tp[AoI][0]
                        self.Mpj[1][0] = self.rp[AoI][0]/self.tp[AoI][0]
                        self.Mpj[1][1] = 1.0/self.tp[AoI][0]
                    else:
                        self.Mpj[0][0] = np.exp(-1j*self.beta[AoI][j])/self.tp[AoI][j]
                        self.Mpj[0][1] = self.rp[AoI][j]*np.exp(-1j*self.beta[AoI][j])/self.tp[AoI][j]
                        self.Mpj[1][0] = self.rp[AoI][j]*np.exp(1j*self.beta[AoI][j])/self.tp[AoI][j]
                        self.Mpj[1][1] = np.exp(1j*self.beta[AoI][j])/self.tp[AoI][j]
                    
                    # Matrix multiplication of p polarization
                    self.Mp_tmp[0][0] = self.Mp[0][0]*self.Mpj[0][0] + self.Mp[0][1]*self.Mpj[1][0]
                    self.Mp_tmp[0][1] = self.Mp[0][0]*self.Mpj[0][1] + self.Mp[0][1]*self.Mpj[1][1]
                    self.Mp_tmp[1][0] = self.Mp[1][0]*self.Mpj[0][0] + self.Mp[1][1]*self.Mpj[1][0]
                    self.Mp_tmp[1][1] = self.Mp[1][0]*self.Mpj[0][1] + self.Mp[1][1]*self.Mpj[1][1]
                    self.Mp[0][0] = self.Mp_tmp[0][0]
                    self.Mp[0][1] = self.Mp_tmp[0][1]
                    self.Mp[1][0] = self.Mp_tmp[1][0]
                    self.Mp[1][1] = self.Mp_tmp[1][1]
                
                # Calculating for the substrate - important for the future
                
                self.phi[AoI][-1] = sp.arcsin(self.chi[AoI]/np.conj(self.layers[-1].N))
                self.kz[AoI][-1] = 2.0*np.pi * np.conj(self.layers[-1].N) * np.cos(self.phi[AoI][-1])/self.lmd
                self.beta[AoI][-1] = self.kz[AoI][-1] * self.layers[-1].d
                self.beta[AoI][-1] = np.real(self.beta[AoI][-1]) + 1j*np.clip(np.imag(self.beta[AoI][-1]),None,35.0)
                if np.isin(35.0,np.imag(self.beta[AoI][-1])):
                    add_message("Note: "+self.layers[-1].name+" is opaque, phase thickness was truncated for numerical stability.")
                # Fresnel amplitude ratios of the entire structure
                self.rs_tot[AoI] = self.Ms[1][0]/self.Ms[0][0]
                self.rp_tot[AoI] = self.Mp[1][0]/self.Mp[0][0]
                self.ts_tot[AoI] = 1.0/self.Ms[0][0]
                self.tp_tot[AoI] = 1.0/self.Mp[0][0]
                
    
                # Ellipsometry quantities only calculated for nonzero AoIs
                if AoI != 0.0:
                    # rho is conjugated to return to N=n-ik convention.
                    self.rho[AoI] = np.conj(self.rp_tot[AoI]/self.rs_tot[AoI])
                    self.psi[AoI] = np.rad2deg(np.arctan(np.absolute(self.rho[AoI])))
                    self.delta[AoI] = delta_range_set(np.angle(self.rho[AoI],deg=True))
                    self.pseudo_epsilon[AoI]=np.sin(np.deg2rad(AoI))**2*(1+np.tan(np.deg2rad(AoI))**2 * ((1-self.rho[AoI])/(1+self.rho[AoI]))**2)
                
                if flux:
                    N0 = np.conj(self.layers[0].N)
                    cosphi0 = np.cos(self.phi[AoI][0])
                    cosphi0_cnj = np.cos(np.conj(self.phi[AoI][0]))
                    Nt = np.conj(self.layers[-1].N)
                    cosphit = np.cos(self.phi[AoI][-1])
                    cosphit_cnj = np.cos(np.conj(self.phi[AoI][-1]))
                    self.Rs[AoI] = np.abs(self.rs_tot[AoI])**2
                    self.Rp[AoI] = np.abs(self.rp_tot[AoI])**2
                    
                    self.Ts[AoI] = (np.abs(self.ts_tot[AoI])**2) * np.real(Nt*cosphit)/np.real(N0*cosphi0) 
                    self.Tp[AoI] = (np.abs(self.tp_tot[AoI])**2) * np.real(Nt*cosphit_cnj)/np.real(N0*cosphi0_cnj)
                    
                    self.As[AoI] = 1.0 - self.Rs[AoI] - self.Ts[AoI]
                    self.Ap[AoI] = 1.0 - self.Rp[AoI] - self.Tp[AoI]
                    
                    self.R[AoI] = 0.5*(self.Rs[AoI]+self.Rp[AoI])
                    self.T[AoI] = 0.5*(self.Ts[AoI]+self.Tp[AoI])
                    self.A[AoI] = 1.0 - self.R[AoI] - self.T[AoI]
        else:
            self.reset_response()
            for delta_lmd in self.filt_lmd_delta:
                self.coh_stack_copy.lmd = self.coh_stack.lmd+delta_lmd
                self.coh_stack_copy.reset_response()
                self.coh_stack_copy.calc_response(flux)
                for AoI in self.AoIs:
                    self.rp_tot[AoI] += self.coh_stack_copy.rp_tot[AoI]/float(self.filt_lmd_num)
                    self.rs_tot[AoI] += self.coh_stack_copy.rs_tot[AoI]/float(self.filt_lmd_num)
                    self.tp_tot[AoI] += self.coh_stack_copy.tp_tot[AoI]/float(self.filt_lmd_num)
                    self.ts_tot[AoI] += self.coh_stack_copy.ts_tot[AoI]/float(self.filt_lmd_num)
                    if flux:
                        self.Rp[AoI] += self.coh_stack_copy.Rp[AoI]/float(self.filt_lmd_num)
                        self.Rs[AoI] += self.coh_stack_copy.Rs[AoI]/float(self.filt_lmd_num)
                        self.Tp[AoI] += self.coh_stack_copy.Tp[AoI]/float(self.filt_lmd_num)
                        self.Ts[AoI] += self.coh_stack_copy.Ts[AoI]/float(self.filt_lmd_num)
            # After we have the averaged spectra we can calculate the rest of the response
            for AoI in self.AoIs:
                if AoI != 0.0:
                    self.rho[AoI] = np.conj(self.rp_tot[AoI]/self.rs_tot[AoI])
                    self.psi[AoI] = np.rad2deg(np.arctan(np.absolute(self.rho[AoI])))
                    self.delta[AoI] = delta_range_set(np.angle(self.rho[AoI],deg=True))
                    self.pseudo_epsilon[AoI]=np.sin(np.deg2rad(AoI))**2*(1+np.tan(np.deg2rad(AoI))**2 * ((1-self.rho[AoI])/(1+self.rho[AoI]))**2)
                if flux:
                    self.R[AoI] = 0.5*(self.Rp[AoI]+self.Rs[AoI])
                    self.T[AoI] = 0.5*(self.Tp[AoI]+self.Ts[AoI])
                    self.Ap[AoI] = 1-self.Rp[AoI]-self.Tp[AoI]
                    self.As[AoI] = 1-self.Rs[AoI]-self.Ts[AoI]
                    self.A[AoI] = 1-self.R[AoI]-self.T[AoI]
            
        if flux and np.any(np.imag(self.layers[-1].N)):
            add_message("Warning: "+self.name+" has an abosrbing final medium. Global flux values will not take that into account!")
    
    
    def calc_abs_dens(self,Nz=100):
        ''' Local absorption density calculation '''
        # Here we must reset the relevant values, since the absorbed density results (as_loc,ap_loc,a_loc) 
        # start as complex and then casted to real (while warning the user if the imaginary part is not zero).
        # To avoid having the interpreter alerting the user false positives, we call reset, which returns those
        # variables to the complex type before each calculation. Since this function shouldn't run so many times,
        # it's not so problematic.
        self.reset_abs_dens()
        
        # Meshing the stack with the same number of points in each layer. The mesh is not linear but exponential.
        # Since the absorption acts exponentially, the points are defined in a similar way with 
        # exponentially growing distances. 
        # This is more efficient and allows to calculate the significant information with less points.
        self.Nz = Nz # Number of points for each layer
        self.z = np.zeros([len(self.layers),Nz]) # Depth coordinate
        self.z_all = np.array([])
        self.z1 = 0 # Upper border
        self.z2 = 0 # Lower border
        for i,layer in enumerate(self.layers):
            self.z2 += layer.d
            # In the case of the first layer we artificially select small value and then replace it with 
            # zero afterwards, since zero cannot be added automatically in a logarithmic scale. 
            # Note: the endpoint is excluded to avoid duplicates
            if self.z1 == 0:
                self.z[i] = np.logspace(np.log(1e-10),np.log(self.z2),num=Nz,endpoint=False,base=np.e)
                self.z[i][0]=0
            else:
                self.z[i] = np.logspace(np.log(self.z1),np.log(self.z2),num=Nz,endpoint=False,base=np.e)
            self.z_all = np.concatenate((self.z_all,self.z[i]))
            self.z1 = self.z2
        
        for AoI in self.AoIs:
            self.as_loc[AoI] = np.zeros([len(self.z_all),len(self.lmd)],dtype=complex)
            self.ap_loc[AoI] = np.zeros([len(self.z_all),len(self.lmd)],dtype=complex)
            self.a_loc[AoI] = np.zeros([len(self.z_all),len(self.lmd)],dtype=complex)
        
        if self.coherent:
            for AoI in self.AoIs:
                self.vs[AoI][0].fill(1.0)
                self.ws[AoI][0] = self.rs_tot[AoI]
                self.vp[AoI][0].fill(1.0)
                self.wp[AoI][0] = self.rp_tot[AoI]
                
                self.vs[AoI][-1] = self.ts_tot[AoI]
                self.ws[AoI][-1].fill(0.0)
                self.vp[AoI][-1] = self.tp_tot[AoI]
                self.wp[AoI][-1].fill(0.0)
                
                
                
                for j in reversed(range(2,len(self.layers))):
                    # We run on all intermediate layers (edges were treated separately)
                    # and create the forward and backward wave amplitudes.
                    # Note that the indices in the matrices should be like the indices of v,w in the left side!
                    # This way we also avoid matrix inversion, which will take longer.
                    
                    
                    self.Msj[0][0] = np.exp(-1j*self.beta[AoI][j-1])/self.ts[AoI][j-1]
                    self.Msj[0][1] = self.rs[AoI][j-1]*np.exp(-1j*self.beta[AoI][j-1])/self.ts[AoI][j-1]
                    self.Msj[1][0] = self.rs[AoI][j-1]*np.exp(1j*self.beta[AoI][j-1])/self.ts[AoI][j-1]
                    self.Msj[1][1] = np.exp(1j*self.beta[AoI][j-1])/self.ts[AoI][j-1]
                    
                    self.vs[AoI][j-1] = self.Msj[0][0]*self.vs[AoI][j] + self.Msj[0][1]*self.ws[AoI][j]
                    self.ws[AoI][j-1] = self.Msj[1][0]*self.vs[AoI][j] + self.Msj[1][1]*self.ws[AoI][j]
                    
                    
                    self.Mpj[0][0] = np.exp(-1j*self.beta[AoI][j-1])/self.tp[AoI][j-1]
                    self.Mpj[0][1] = self.rp[AoI][j-1]*np.exp(-1j*self.beta[AoI][j-1])/self.tp[AoI][j-1]
                    self.Mpj[1][0] = self.rp[AoI][j-1]*np.exp(1j*self.beta[AoI][j-1])/self.tp[AoI][j-1]
                    self.Mpj[1][1] = np.exp(1j*self.beta[AoI][j-1])/self.tp[AoI][j-1]
                    
                    self.vp[AoI][j-1] = self.Mpj[0][0]*self.vp[AoI][j] + self.Mpj[0][1]*self.wp[AoI][j]
                    self.wp[AoI][j-1] = self.Mpj[1][0]*self.vp[AoI][j] + self.Mpj[1][1]*self.wp[AoI][j]
                    
                
                for j in range(len(self.layers)):
                    # Calculation of absorption density coefficients for each layer.
                    # Ci are the A1,A2,A3 constants that appear in Byrnes's article, renamed for the sake of clearness.
                    N0 = np.conj(self.layers[0].N)
                    cosphi0 = np.cos(self.phi[AoI][0])
                    cos_phi0conj = np.cos(np.conj(self.phi[AoI][0]))
                    N = np.conj(self.layers[j].N)
                    cosphi = np.cos(self.phi[AoI][j])
                    cos_phiconj = np.cos(np.conj(self.phi[AoI][j]))
                    kz = self.kz[AoI][j]
                    vs = self.vs[AoI][j]
                    ws = self.ws[AoI][j]
                    vp = self.vp[AoI][j]
                    wp = self.wp[AoI][j]
                    
                    
                    self.C1s_loc[AoI][j] = np.imag(N*cosphi*kz) * np.abs(ws)**2 / np.real(N0*cosphi0)
                    
                    self.C2s_loc[AoI][j] = np.imag(N*cosphi*kz) * np.abs(vs)**2 / np.real(N0*cosphi0)
                    
                    self.C3s_loc[AoI][j] = np.imag(N*cosphi*kz) * vs*np.conj(ws) / np.real(N0*cosphi0)
                    
                    self.C1p_loc[AoI][j] = 2*np.imag(kz) * np.real(N*cos_phiconj) * np.abs(wp)**2 / np.real(N0*cos_phi0conj)
                    
                    self.C2p_loc[AoI][j] = 2*np.imag(kz) * np.real(N*cos_phiconj) * np.abs(vp)**2 / np.real(N0*cos_phi0conj)
                    
                    self.C3p_loc[AoI][j] = -2*np.real(kz) * np.imag(N*cos_phiconj) * vp*np.conj(wp) / np.real(N0*cos_phi0conj)
                
                d_prev = 0
                for i,z in enumerate(self.z):
                    # i is layer index. For each layer we have a matching z coordinate array,
                    # which includes all z values for that layer (inside self.z). Also, stack.z_all
                    # is a combination of all those z values.
                    
                    C1s = self.C1s_loc[AoI][i]
                    C2s = self.C2s_loc[AoI][i]
                    C3s = self.C3s_loc[AoI][i]
                    C1p = self.C1p_loc[AoI][i]
                    C2p = self.C2p_loc[AoI][i]
                    C3p = self.C3p_loc[AoI][i]
                    kz = self.kz[AoI][i]
                    d = self.layers[i].d
                    
                    for j,zi in enumerate(z):
                        # Calculate the optical depth. Again, we truncate the imaginary part to be
                        # no more than 35.0, to increase stability.
                        beta1 = (zi-d_prev)*np.real(kz)
                        beta2 = np.clip((zi-d_prev)*np.imag(kz) , None, 35.0)
                        
                        self.as_loc[AoI][int(np.where(self.z_all==zi)[0])] = C1s*np.exp(2*beta2) + C2s*np.exp(-2*beta2)\
                        + C3s*np.exp(2j*beta1) + np.conj(C3s)*np.exp(-2j*beta1)
                        
                        self.ap_loc[AoI][int(np.where(self.z_all==zi)[0])] = C1p*np.exp(2*beta2) + C2p*np.exp(-2*beta2)\
                        + C3p*np.exp(2j*beta1) + np.conj(C3p)*np.exp(-2j*beta1)
                    
                    d_prev += d
                    
                    # Integrating over the absorbed density to get the global absorption in each layer.
                    # We seperate the cases when Im(kz) or Re(kz) are zero, since the integral is analytic.
                    # Di are the whole terms, including the spatial dependency.
                    for k in range(len(self.lmd)):
                        if np.imag(kz[k]) != 0:
                            
                            # Again, the phase thickness is calculated and truncated to avoid numerical instability.
                            beta2 = float(np.clip(d*np.imag(kz[k]),None,35.0))
                            
                            D1s = C1s[k]/2/np.imag(kz[k]) * (np.exp(2*beta2)-1)
                            D2s = C2s[k]/-2/np.imag(kz[k]) * (np.exp(-2*beta2)-1)
                            D1p = C1p[k]/2/np.imag(kz[k]) * (np.exp(2*beta2)-1)
                            D2p = C2p[k]/-2/np.imag(kz[k]) * (np.exp(-2*beta2)-1)
                        else:
                            D1s = C1s[k]*d
                            D2s = C2s[k]*d
                            D1p = C1p[k]*d
                            D2p = C2p[k]*d
                            
                        if np.real(kz[k]) != 0:
                            beta1 = d*np.real(kz[k])
                            D3s = C3s[k]/2j/np.real(kz[k]) * (np.exp(2j*beta1)-1)
                            D4s = np.conj(C3s[k])/-2j/np.real(kz[k]) * (np.exp(-2j*beta1)-1)
                            D3p = C3p[k]/2j/np.real(kz[k]) * (np.exp(2j*beta1)-1)
                            D4p = np.conj(C3p[k])/-2j/np.real(kz[k]) * (np.exp(-2j*beta1)-1)
                        else:
                            D3s = C3s[k]*d
                            D4s = np.conj(C3s[k])*d
                            D3p = C3p[k]*d
                            D4p = np.conj(C3p[k])*d
                            
                            
                        self.As_glob[AoI][i][k] = D1s + D2s + D3s + D4s
                        self.Ap_glob[AoI][i][k] = D1p + D2p + D3p + D4p
                    
                # We make sure there is no imaginary part left in the absorption density. 
                # If there is, we alert the user.
                if np.any(np.imag(self.as_loc[AoI])) or np.any(np.imag(self.ap_loc[AoI])):
                    print "Warning: complex values in local absorption of "+self.name+". Invalid configuration perhaps?"
                else:
                    self.as_loc[AoI] = np.real(self.as_loc[AoI])
                    self.ap_loc[AoI] = np.real(self.ap_loc[AoI])
                
                self.a_loc[AoI] = 0.5 * (self.as_loc[AoI]+self.ap_loc[AoI])
                
                 #We make sure there is no imaginary part left in the global absorption integration.
                 # If there is, we alert the user.
                if np.any(np.imag(self.As_glob[AoI])) or np.any(np.imag(self.Ap_glob[AoI])):
                    print "Warning: complex values in global absorption of "+self.name+". Invalid configuration perhaps?"
                else:
                    self.As_glob[AoI] = np.real(self.As_glob[AoI])
                    self.Ap_glob[AoI] = np.real(self.Ap_glob[AoI])
                    
                self.A_glob[AoI] = 0.5*(self.As_glob[AoI] + self.Ap_glob[AoI])
        else:
            for delta_lmd in self.filt_lmd_delta:
                self.coh_stack_copy.lmd = self.coh_stack.lmd+delta_lmd
                self.coh_stack_copy.calc_all()
                for AoI in self.AoIs:
                    self.a_loc[AoI] += self.coh_stack_copy.a_loc[AoI]/float(self.filt_lmd_num)
                    self.A_glob[AoI] += self.coh_stack_copy.A_glob[AoI]/float(self.filt_lmd_num)
                
                # Making sure the result is real and casting it to real arrays. 
                # The 2nd loop is not very efficient, but for the time being will make do.
                for AoI in self.AoIs:
                    if np.any(np.imag(self.a_loc[AoI])) or np.any(np.imag(self.A_glob[AoI])):
                        print "Warning: complex values in global absorption of "+self.name+". Invalid configuration perhaps?"
                    else:
                        self.a_loc[AoI] = np.real(self.a_loc[AoI])
                        self.A_glob[AoI] = np.real(self.A_glob[AoI])
                    
    def calc_all(self):
        ''' A shortcut to run the entire calculation. Not needed internally in SE optimization '''
        self.reset_all()
        self.calc_response()
        self.calc_abs_dens()