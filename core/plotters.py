# -*- coding: utf-8 -*-
"""
@ Omer Luria
Tel-Aviv University
luriomer@gmail.com

This file contains the plotting tools.
"""

from core.misc import *
sys.path.insert(0,'..')


# Setting global defaults for font, fontsize, figure sizes etc
plt.rcParams['font.family'] = 'serif'
plt.rcParams['mathtext.fontset'] = 'dejavuserif'
default_fontsize = 12
plt.rc('xtick',labelsize=default_fontsize)
plt.rc('ytick',labelsize=default_fontsize)
default_horunits = "eV"
default_linewidth = 1.0
default_markersize = 5.0
decimal_digits = 2

line_color_list = ("black","red","blue","green","purple","brown","pink",
              "cyan","magenta","lime","coral","darkred","grey","seagreen")


def plot_optical_config(model,**kwargs):
    ''' A plotter to present the entire optical model configuration, including nominal values '''
    if model.class_name != "model_cls":
        raise TypeError("plot_optical_config: "+model.name+" is not a model!")
    check_kwargs(kwargs,["hbias","vbias","hrect",
                         "vrect","textsize"],"plot_optical_config")
    
    hbias = kwargs.setdefault("hbias",5)
    vbias = kwargs.setdefault("vbias",1.5)
    hrect = kwargs.setdefault("hrect",5)
    vrect = kwargs.setdefault("vrect",1.5)
    textsize = kwargs.setdefault("textsize",default_fontsize)
    ambient = model.stacks.keys()[0].layers[0].name
    fillcolors = ("darkcyan","lightgray","sandybrown","tomato","palevioletred","olivedrab","peachpuff","aqua","orchid","lime","rovalblue")
    color_dic = {}
    for ii in range(min(len(fillcolors),len(model.layers))):
        color_dic[model.layers[ii]] = fillcolors[ii]
    
    plt.figure()
    tune_str = "Fitted parameters:\n "
    for iden in model.identifiers:
        tune_str += iden["obj"].name+"_"+iden["attr"]+"\n"
    disp_str = "Tunabe dispersion:"
    for disp_layer in model.dispersed_layers:
        disp_str += "\n"+disp_layer.name
    
    plt.suptitle("Configuration scheme of "+model.name+"\nAmbient: "+ambient+"   ;   AoIs: "+str(model.AoIs)[1:-1]+r"$^\circ$",fontsize=textsize+1)
    optical_config = plt.axes()
    plt.text(2.75,3,tune_str,ha="center",va="center",bbox=dict(boxstyle="round",facecolor="lightgrey"))
    plt.text(2.75,6,disp_str,ha="center",va="center",bbox=dict(boxstyle="round",facecolor="lightgrey"))
    i = 0 #stack index
    stacks = sort_by_init_time(model.stacks)
    for j,stack in enumerate(stacks):
        partial_stack_text = stack.name+"\n"+str(round(getattr(model,"S_"+stack.name)*100,2))+"%"
        plt.text(hbias*(i+1.5),0.85,partial_stack_text,size=textsize,ha="center",va="center",bbox=dict(boxstyle="round",facecolor="magenta"))
        for k,layer in enumerate(reversed(stack.layers[1:])):
            if k==0 and j==0:
                rect = plt.Rectangle((hbias*(i+1),vbias*(k+1)),len(model.stacks)*hrect,vrect,facecolor=color_dic[layer],edgecolor="black",linewidth=2.5)
            else:
                rect = plt.Rectangle((hbias*(i+1),vbias*(k+1)),hrect,vrect,facecolor=color_dic[layer],edgecolor="black",linewidth=2.5)
            if not (k==0 and j>0):
                plt.gca().add_patch(rect)
            if k==0:
                d_text = ""
            elif layer.d >=1.0e3 and layer.d<1.0e6:
                d_text = "\nd = "+str(round(layer.d/1.0e3,decimal_digits))+r" $[\mu m]$"
            elif layer.d>=1.0e6:
                d_text = "\nd = "+str(round(layer.d/1.0e6,decimal_digits))+" [mm]"
            else:
                d_text = "\nd = "+str(round(layer.d,decimal_digits))+" [nm]"
            layer_text = layer.name+d_text+"\nDisp = "
            if len(layer.disp_params) == 1:
                layer_text += layer.disp_params.keys()[0]
            elif len(layer.disp_params) > 1:
                layer_text+= layer.disp_type
            if layer.disp_type[0:3] == "EMA":
                if layer.disp_type == "EMA General":
                    eff_text = " $, \mu$ = "+str(round(layer.mu,decimal_digits))+"\n"
                else:
                    eff_text = "\n"
                for component in sort_by_init_time(layer.components):
                    VR_text = str(round(getattr(layer,"f_"+component.name)*100,decimal_digits))
                    eff_text += component.name+" "+VR_text+"%  "
                layer_text += eff_text
            if k==0 and j==0:
                plt.text(7.5+0.5*(len(model.stacks)-1)*hrect,vrect*(k+1.5),layer_text,size=textsize,ha="center",va="center")
            elif not (k==0 and j>0):
                plt.text(hbias*(i+1.5),vbias*(k+1.5),layer_text,size=textsize,ha="center",va="center")
        i += 1
    plt.axis('scaled')
    plt.gca().axes.get_xaxis().set_visible(False)
    plt.gca().axes.get_yaxis().set_visible(False)
    plt.show()
    return optical_config


def plot_ellipso(*args,**kwargs):
    ''' plot ellipsometry data between objects (models, stacks and exps in any order) '''
    
    check_kwargs(kwargs,["ellipso_type","horunits","show_errorbars","selected_AoIs",
                         "fontsize","markersize","markevery","linewidth","alpha","capsize"],
                        "plot_ellipso")
    ellipso_type = kwargs.setdefault("ellipso_type","psidelta")
    if ellipso_type not in ["psidelta","rho","pseudoepsilon"]:
        raise TypeError("plot_ellipso: ellipso_type unrecognized")
    horunits = kwargs.setdefault("horunits",default_horunits)
    if horunits not in ["nm","eV"]:
        raise NameError("plot_ellipso: horunits unrecognized")
    show_errorbars = kwargs.setdefault("show_errorbars",False)
    selected_AoIs = kwargs.setdefault("selected_AoIs",None)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)
    markersize = kwargs.setdefault("markersize",default_markersize)
    markevery = kwargs.setdefault("markevery",1)
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    alpha = kwargs.setdefault("alpha",0.35)
    capsize = kwargs.setdefault("capsize",5.0)

    
    # Organize all the colors, to plot each AoI with different color
    colors = line_color_list
    markers = ['o','x','s','+','*','p','X','D','v','^','<','>']
    linestyles = ['-','--',':']
    filtered_linestyle = '-.'
    marker_dic = {}
    linestyle_dic = {}
    
    AoI_set = []
    for obj in args:
        if obj.class_name in ["model_cls","stack_cls"]:
            AoI_set += obj.AoIs
            linestyle_dic[obj] = linestyles[0]
            linestyles.remove(linestyles[0])
        elif obj.class_name == "exp_cls":
            AoI_set += obj.ellipso_AoIs
            marker_dic[obj] = markers[0]
            markers.remove(markers[0])
        else:
            raise TypeError("plot_ellipso: "+obj.name+" is not a model,stack or exp!")
    if type(selected_AoIs) in [int,float,np.float16,np.float32,np.float64]:
        selected_AoIs = [selected_AoIs]
    elif selected_AoIs == None:
        selected_AoIs = AoI_set
    AoI_set = sorted(list(set(AoI_set).intersection(set(selected_AoIs))))
    # Make sure zero is not included
    if 0 in AoI_set:
        AoI_set.remove(0)
    colors = colors*len(AoI_set) #Make sure we always have a color, even if it starts to repeat for large number of AoIs
            
    color_dic = {}
    for i,AoI in enumerate(AoI_set):
        color_dic[AoI] = colors[i]
    
    if horunits == "nm":
        hor1 = "lmd"
        hor2 = "ellipso_lmd"
    elif horunits == "eV":
        hor1 = "E"
        hor2 = "ellipso_E"
    else:
        raise NameError("plot_ellipso: horunits unrecognized")
        
    ellipso_fig = plt.figure(facecolor="white")
    ellipso_fig.suptitle("Ellipsometry",fontsize=12)
    ellipso_plot1 = plt.subplot(211)
    ellipso_plot2 = plt.subplot(212)
    
    for obj in args:
        
        if obj.class_name in ["model_cls","stack_cls"]:
            for AoI in obj.AoIs:
                if AoI in AoI_set:
                    if ellipso_type == "rho":
                        ellipso_plot1.plot(getattr(obj,hor1),np.real(obj.rho[AoI]), label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                        ellipso_plot2.plot(getattr(obj,hor1),np.imag(obj.rho[AoI]), label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                    elif ellipso_type == "psidelta":
                        ellipso_plot1.plot(getattr(obj,hor1),obj.psi[AoI],label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                        ellipso_plot2.plot(getattr(obj,hor1),obj.delta[AoI],label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                    elif ellipso_type == "pseudoepsilon":
                        ellipso_plot1.plot(getattr(obj,hor1),np.real(obj.pseudo_epsilon[AoI]),label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                        ellipso_plot2.plot(getattr(obj,hor1),-np.imag(obj.pseudo_epsilon[AoI]),label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                    else:
                        raise NameError("plot_ellipso: ellipso_type unrecognized")
        if obj.class_name == "exp_cls":
            for AoI in obj.ellipso_AoIs:
                if AoI in AoI_set:
                    if ellipso_type == "rho":
                        if show_errorbars:
                            err1 = np.real(obj.rho_err[AoI])
                            err2 = np.imag(obj.rho_err[AoI])
                        else:
                            err1 = None
                            err2 = None
                        ellipso_plot1.errorbar(getattr(obj,hor2),np.real(obj.rho[AoI]),yerr=err1,label=obj.name+" "+str(AoI)+"$^{\circ}$",color=color_dic[AoI],marker=marker_dic[obj],capsize=capsize,markersize=markersize,markevery=markevery,linestyle='',markerfacecolor='none',alpha=alpha)
                        ellipso_plot2.errorbar(getattr(obj,hor2),np.imag(obj.rho[AoI]),yerr=err2,label=obj.name+" "+str(AoI)+"$^{\circ}$",color=color_dic[AoI],marker=marker_dic[obj],capsize=capsize,markersize=markersize,markevery=markevery,linestyle='',markerfacecolor='none',alpha=alpha)
                    elif ellipso_type == "psidelta":
                        if show_errorbars:
                            err1 = obj.psi_err[AoI]
                            err2 = obj.delta_err[AoI]
                        else:
                            err1 = None
                            err2 = None
                        ellipso_plot1.errorbar(getattr(obj,hor2),obj.psi[AoI],yerr=err1,label=obj.name+" "+str(AoI)+"$^{\circ}$",color=color_dic[AoI],capsize=capsize,marker=marker_dic[obj],markersize=markersize,markevery=markevery,linestyle='',markerfacecolor='none',alpha=alpha)
                        ellipso_plot2.errorbar(getattr(obj,hor2),obj.delta[AoI],yerr=err2,label=obj.name+" "+str(AoI)+"$^{\circ}$",color=color_dic[AoI],marker=marker_dic[obj],capsize=capsize,markersize=markersize,markevery=markevery,linestyle='',markerfacecolor='none',alpha=alpha)
                    elif ellipso_type == "pseudoepsilon":
                        if show_errorbars:
                            err1 = np.real(obj.pseudo_epsilon_err[AoI])
                            err2 = np.imag(obj.pseudo_epsilon_err[AoI])
                        else:
                            err1 = None
                            err2 = None
                        ellipso_plot1.errorbar(getattr(obj,hor2),np.real(obj.pseudo_epsilon[AoI]),yerr=err1,label=obj.name+" "+str(AoI)+"$^{\circ}$",color=color_dic[AoI],marker=marker_dic[obj],capsize=capsize,markersize=markersize,markevery=markevery,linestyle='',markerfacecolor='none',alpha=alpha)
                        ellipso_plot2.errorbar(getattr(obj,hor2),-np.imag(obj.pseudo_epsilon[AoI]),yerr=err2,label=obj.name+" "+str(AoI)+"$^{\circ}$",color=color_dic[AoI],marker=marker_dic[obj],capsize=capsize,markersize=markersize,markevery=markevery,linestyle='',markerfacecolor='none',alpha=alpha)
    
    if ellipso_type == "rho":
        ellipso_plot1.set_ylabel(r"$\Re(\rho)$",fontsize=fontsize)
        ellipso_plot2.set_ylabel(r"$\Im(\rho)$",fontsize=fontsize)
    elif ellipso_type == "psidelta":
        ellipso_plot1.set_ylabel(r"$\Psi$ [$^{\circ}$]",fontsize=fontsize)
        ellipso_plot2.set_ylabel(r"$\Delta$ [$^{\circ}$]",fontsize=fontsize)
    elif ellipso_type == "pseudoepsilon":
        ellipso_plot1.set_ylabel(r"$\Re(<\varepsilon>)$",fontsize=fontsize)
        ellipso_plot2.set_ylabel(r"$-\Im(<\varepsilon>)$",fontsize=fontsize)
    if horunits == "nm":
        ellipso_plot2.set_xlabel(r"$\lambda$ [nm]",fontsize=fontsize)
    elif horunits == "eV":
        ellipso_plot2.set_xlabel("E [eV]",fontsize=fontsize)
    ellipso_plot1.legend(fontsize=fontsize)
    ellipso_plot1.grid()
    ellipso_plot2.legend(fontsize=fontsize)
    ellipso_plot2.grid()
    
    return (ellipso_plot1,ellipso_plot2)


def plot_flux(*args,**kwargs):
    ''' plot flux data between objects (models, stacks and exps in any order) '''
    
    check_kwargs(kwargs,["show_errorbars","selected_AoIs","horunits","fontsize",
                         "markersize","markevery","errorevery","linewidth","alpha","capsize"],"plot_flux")
    show_errorbars = kwargs.setdefault("show_errorbars",False)
    selected_AoIs = kwargs.setdefault("selected_AoIs",None)
    horunits = kwargs.setdefault("horunits",default_horunits)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)
    markersize = kwargs.setdefault("markersize",default_markersize)
    markevery = kwargs.setdefault("markevery",1)
    errorevery = kwargs.setdefault("errorevery",1)
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    alpha = kwargs.setdefault("alpha",0.35)
    capsize = kwargs.setdefault("capsize",5.0)    
    
    # Organize all the colors, to plot each AoI with different color
    colors = line_color_list
    markers = ['o','x','*','+','s','p','X','D','v','^','<','>']
    linestyles = ['-','--',':']
    filtered_linestyle = '-.'
    marker_dic = {}
    linestyle_dic = {}
    
    AoI_set = []
    for obj in args:
        if obj.class_name in ["model_cls","stack_cls"]:
            AoI_set += obj.AoIs
            linestyle_dic[obj] = linestyles[0]
            linestyles.remove(linestyles[0])
        elif obj.class_name == "exp_cls":
            marker_dic[obj] = markers[0]
            markers.remove(markers[0])
            AoI_set += obj.reflect_AoIs
        else:
            raise TypeError("plot_flux: "+obj.name+" is not a model,stack or exp!")
    if type(selected_AoIs) in [int,float,np.float16,np.float32,np.float64]:
        selected_AoIs = [selected_AoIs]
    elif selected_AoIs == None:
        selected_AoIs = AoI_set
    AoI_set = sorted(list(set(AoI_set).intersection(set(selected_AoIs))))
    colors = colors*len(AoI_set) #Make sure we always have a color, even if it starts to repeat for large number of AoIs
    color_dic = {}
    for i,AoI in enumerate(AoI_set):
        color_dic[AoI] = colors[i]
    
    if horunits == "nm":
        hor1 = "lmd"
        hor2 = "reflect_lmd"
        xlab = r"$\lambda$ [nm]"
    elif horunits == "eV":
        hor1 = "E"
        hor2 = "reflect_E"
        xlab = "$E$ $[eV]$"
    else:
        raise NameError("plot_flux: horunits unrecognized")
        
    flux_fig = plt.figure(facecolor="white")
    flux_fig.suptitle("Flux ratios",fontsize=fontsize)
    flux_Rplot = plt.subplot(311)
    flux_Tplot = plt.subplot(312)
    flux_Aplot = plt.subplot(313)
    
    for obj in args:
        
        if obj.class_name in ["model_cls","stack_cls"]:
            for AoI in obj.AoIs:
                if AoI in AoI_set:
                    flux_Rplot.plot(getattr(obj,hor1),obj.R[AoI], label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                    flux_Tplot.plot(getattr(obj,hor1),obj.T[AoI], label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
                    flux_Aplot.plot(getattr(obj,hor1),obj.A[AoI], label=obj.name+" "+str(AoI)+"$^{\circ}$", color=color_dic[AoI],linestyle=linestyle_dic[obj],linewidth=linewidth)
        
        if obj.class_name == "exp_cls":
            for AoI in obj.reflect_AoIs:
                if AoI in AoI_set:
                    if obj.R[AoI].any():
                        if show_errorbars:
                            err = obj.R_err[AoI]
                        else:
                            err = None
                        flux_Rplot.errorbar(getattr(obj,hor2),obj.R[AoI],yerr=err,label=obj.name+" "+str(AoI)+"$^{\circ}$",color=color_dic[AoI],capsize=capsize,marker=marker_dic[obj],markersize=markersize,markevery=markevery,errorevery=errorevery,linestyle='',markerfacecolor='none',alpha=alpha)
    
    flux_Rplot.set_ylabel("$R_{\lambda}$",fontsize=fontsize)
    flux_Rplot.legend(fontsize=fontsize)
    flux_Rplot.grid()
    flux_Tplot.set_ylabel("$T_{\lambda}$",fontsize=fontsize)
    flux_Tplot.legend(fontsize=fontsize)
    flux_Tplot.grid()
    flux_Aplot.set_ylabel("$A_{\lambda}$",fontsize=fontsize)
    flux_Aplot.set_xlabel(xlab,fontsize=fontsize)
    flux_Aplot.legend(fontsize=fontsize)
    flux_Aplot.grid()
    
    return (flux_Rplot,flux_Tplot,flux_Aplot)


def plot_abs_dens(obj,**kwargs):
    ''' Plot the absorbtion density distribution of a stack/model. If obj is model, it creates separate plots. '''
    
    check_kwargs(kwargs,["horunits","linewidth","fontsize"],"plot_abs_dens")
    horunits = kwargs.setdefault("horunits",default_horunits)
    profiles = kwargs.setdefault("profiles",False)
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)
    
    if horunits == "nm":
        hor = "lmd"
        hor_label = "$\lambda$ $[nm]$"
    elif horunits == "eV":
        hor = "E"
        hor_label = "$E$ $[eV]$"
    else:
        raise NameError("plot_abs_dens: horunits unrecognized.")
    
    if obj.class_name == "model_cls":
        stack_list = obj.stacks.keys()
    elif obj.class_name == "stack_cls":
        stack_list = [obj]
    else:
        raise TypeError("plot_abs_dens: "+obj.name+": is not a model or stack!")
    for stack in stack_list:
        if 0 not in stack.AoIs:
            print("\nNote: "+stack.name+" does not contain AoI=0, plotting power density contour for the first AoI found.")
            AoI = stack.AoIs[0]
        else:
            AoI = 0
        
        if np.any(stack.a_loc[AoI]):
            delta_z = stack.layers[0].d
            plt.figure()
            plt.suptitle("Power density absorption of "+stack.name+"\n AoI = "+str(AoI)+"$^\circ$")
            
            # Global absorptance, both locally integrated and from global energy balance
            if profiles:
                glob_absplot = plt.subplot(221)
            else:
                glob_absplot = plt.subplot(211)
            glob_absplot.plot(getattr(stack,hor),stack.A[AoI],label="Global",linestyle='',marker='o',markevery=int(20.0/np.average(np.diff(stack.lmd))),color="black")
            glob_absplot.plot(getattr(stack,hor),np.sum(stack.A_glob[AoI],axis=0),label="Integrated",linestyle='--',linewidth=linewidth,color="black")
            for j,layer in enumerate(stack.layers):
                # Global absorptance is only presented for absorbing layers
                if np.any(stack.A_glob[AoI][j]):
                    glob_absplot.plot(getattr(stack,hor),stack.A_glob[AoI][j],label=layer.name)

            glob_absplot.legend()
            glob_absplot.grid()
            glob_absplot.set_xlabel(hor_label,fontsize=fontsize)
            glob_absplot.set_ylabel("$A_{\lambda}$",fontsize=fontsize)
            
            # Spatial profiles for some wavelengths
            if profiles:
                spectral_profplot = plt.subplot(222)
                for i in np.linspace(0,len(stack.lmd),10,endpoint=False,dtype=int):
                    spectral_profplot.plot(stack.z_all-delta_z,stack.a_loc[AoI][:,i],label=hor_label[:-5]\
                                           +" = "+str(round(getattr(stack,hor)[i],1))+"\;"+hor_label[-4:-2]+"$",linewidth=linewidth)
                        
                spectral_profplot.legend()
                spectral_profplot.grid()
                spectral_profplot.set_xscale("symlog")
                if not any(np.imag(stack.layers[0].N)):
                    # If the ambient is non-absorbing we only show the positive z values (afer shifting)
                    spectral_profplot.set_xlim(0,None)
                spectral_profplot.set_xlabel("$z$ $[nm]$",fontsize=fontsize)
                spectral_profplot.set_ylabel("$a_{\lambda}$ $[nm^-1]$",fontsize=fontsize)
            
            # A contour map of the entire distribution - spatial, wavelength and absorption density values
            dens_contour = plt.subplot(212)
            minval = np.min(stack.a_loc[AoI][np.nonzero(stack.a_loc[AoI])])
            maxval = np.max(stack.a_loc[AoI][np.nonzero(stack.a_loc[AoI])])
            levs = np.linspace(minval,maxval,500)
            cnt = plt.contourf(getattr(stack,hor),stack.z_all-delta_z,stack.a_loc[AoI],levels=levs,cmap="gnuplot")
            for c in cnt.collections:
                c.set_edgecolor("face")
            plt.colorbar(label="$a_{\lambda}$ $[nm^-1]$")
            plt.gca().invert_yaxis()
            plt.yscale("symlog")
            plt.xlabel(hor_label,fontsize=fontsize)
            plt.ylabel("$z$ $[nm]$",fontsize=fontsize)
            if not any(np.imag(stack.layers[0].N)):
                # If the ambient is non-absorbing we only show the positive z values (afer shifting)
                dens_contour.set_ylim(None,0)
            dens_contour.grid()
            
            # Add boundary lines
            for zi in stack.z:
                if profiles:
                    spectral_profplot.axvline(x=zi[0]-delta_z,linestyle=':',color="black",linewidth=1.0)
                dens_contour.axhline(y=zi[0]-delta_z,linestyle=':',color="green",linewidth=linewidth)
            
        else:
            add_message("Warning: "+stack.name+" is transparent, no local absorption profile can be plotted.")
    if profiles:
        return (spectral_profplot,glob_absplot,dens_contour)
    else:
        return (glob_absplot,dens_contour)


def plot_dispersion(*args,**kwargs):
    ''' Plot optical dispersion and absorption of a layer object 
        It is possible to present the dispersion as the dielectric function '''
    
    check_kwargs(kwargs,["form","horunits","linewidth","fontsize"],"plot_dispersion")
    form = kwargs.setdefault("form","epsilon")
    horunits = kwargs.setdefault("horunits",default_horunits)
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)

    if horunits == "nm":
        hor = "lmd"
        hor_label = "$\lambda$ [nm]"
    elif horunits == "eV":
        hor = "E"
        hor_label = "E [eV]"
    else:
        raise NameError("plot_dispersion: horunits unrecognized.")
    
    if form == "nk":
        ver = "N"
        ver_ax1 = "n"
        ver_ax2 = "k"
    elif form == "epsilon":
        ver = "epsilon"
        ver_ax1 = r"$\Re(\varepsilon)$"
        ver_ax2 = r"-$\Im(\varepsilon)$"
    else:
        raise TypeError("plot_dispersion: form unrecognized.")

    dispersion_fig = plt.figure(facecolor="white")
    dispersion_fig.suptitle("Optical dispersion")
    
    dispersion_plot1 = plt.subplot(311)
    for layer in args:
        if layer.class_name != "layer_cls":
            raise TypeError("plot_dispersion: "+layer.name+" is not a layer!")
        if layer.name[-3:] == "PbP":
            dispersion_plot1.plot(getattr(layer,hor),np.real(getattr(layer,ver)),label=layer.name,linestyle='',marker='o')
        else:
            dispersion_plot1.plot(getattr(layer,hor),np.real(getattr(layer,ver)),label=layer.name,linewidth=linewidth)
        if hasattr(layer,"PbP_N"):
            dispersion_plot1.plot(getattr(layer,"PbP_"+hor),np.real(getattr(layer,"PbP_"+ver)),label=layer.name+" point-by-point",linestyle='',marker='o')
    dispersion_plot1.set_ylabel(ver_ax1,fontsize=fontsize)
    dispersion_plot1.grid()
    dispersion_plot1.legend(fontsize=fontsize)
    dispersion_plot2 = plt.subplot(312)
    for layer in args:
        if layer.name[-3:] == "PbP":
            dispersion_plot2.plot(getattr(layer,hor),-np.imag(getattr(layer,ver)),label=layer.name,linestyle='',marker='o')
        else:
            dispersion_plot2.plot(getattr(layer,hor),-np.imag(getattr(layer,ver)),label=layer.name,linewidth=linewidth)
        if hasattr(layer,"PbP_N"):
            dispersion_plot2.plot(getattr(layer,"PbP_"+hor),-np.imag(getattr(layer,"PbP_"+ver)),label=layer.name+" point-by-point",linestyle='',marker='o')
    dispersion_plot2.set_ylabel(ver_ax2,fontsize=fontsize)
    dispersion_plot2.grid()
    dispersion_plot2.legend(fontsize=fontsize)
    dispersion_plot3 = plt.subplot(313)
    for layer in args:
        if layer.name[-3:] == "PbP":
            dispersion_plot3.plot(getattr(layer,hor),layer.alpha,label=layer.name,linestyle='',marker='o')
        else:
            dispersion_plot3.plot(getattr(layer,hor),layer.alpha,label=layer.name,linewidth=linewidth)
        if hasattr(layer,"PbP_N"):
            dispersion_plot3.plot(getattr(layer,"PbP_"+hor),4*np.pi*-np.imag(layer.PbP_N)/layer.PbP_lmd,label=layer.name+" point-by-point",linestyle='',marker='o')
    dispersion_plot3.set_ylabel(r"$\alpha$"+" [$nm^{-1}$]",fontsize=fontsize)
    dispersion_plot3.set_xlabel(hor_label,fontsize=fontsize)
    dispersion_plot3.grid()
    dispersion_plot3.legend(fontsize=fontsize)
    
    return (dispersion_plot1,dispersion_plot2,dispersion_plot3)


def plot_dispersion_components(layer,**kwargs):
    ''' Plot each component contribution of a layer's optical dispersion (e.g each oscillator). '''
    
    if layer.class_name != "layer_cls":
        raise TypeError("plot_dispersion_components: "+layer.name+" is not a layer!")
    check_kwargs(kwargs,["horunits","linewidth","fontsize"],"plot_dispersion")
    horunits = kwargs.setdefault("horunits",default_horunits)
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)

    if horunits == "nm":
        hor = "lmd"
        hor_label = "$\lambda$ [nm]"
    elif horunits == "eV":
        hor = "E"
        hor_label = "E [eV]"
    else:
        raise NameError("plot_dispersion_components: horunits unrecognized.")
    
    fig = plt.figure(facecolor="white")
    fig.suptitle("Dispersion components of "+str(layer.name))
    plot1 = plt.subplot(211)
    plot2 = plt.subplot(212)
    
    for key in layer.epsilon_comp:
        splitted_key = key.split("_")
        if len(splitted_key)>1:
            keylabel = key.split("_")[0]+" "+key.split("_")[1]
        else:
            keylabel = key.split("_")[0]
        plot1.plot(getattr(layer,hor),np.real(layer.epsilon_comp[key]),label=keylabel,linestyle='--',linewidth=linewidth)
        plot2.plot(getattr(layer,hor),-np.imag(layer.epsilon_comp[key]),label=keylabel,linestyle='--',linewidth=linewidth)
    plot1.plot(getattr(layer,hor),np.real(layer.epsilon),label="Sum",linestyle='-',color="black",linewidth=linewidth)
    plot2.plot(getattr(layer,hor),-np.imag(layer.epsilon),label="Sum",linestyle='-',color="black",linewidth=linewidth)
    
    
    plot1.set_ylabel(r"$\Re(\varepsilon)$",fontsize=fontsize)
    plot1.legend(fontsize=fontsize)
    plot1.grid()
    
    plot2.set_ylabel(r"-$\Im(\varepsilon)$",fontsize=fontsize)
    plot2.set_xlabel("$E$ $[eV]$",fontsize=fontsize)
    plot2.legend(fontsize=fontsize)
    plot2.grid()


def plot_Tauc(layer,m,**kwargs):
    ''' Plot the rescaled absorption spectrum (alpha*E)**(1/m) to estimate bandgap of semiconductors
        assuming interband transitions. m=1/2 for direct transitions, m=2 for indirect transitions. 
        The bandgap can then be manually extrapolated using E-Eg~(alpha*E)**(1/m). '''
    if layer.class_name != "layer_cls":
        raise TypeError("plot_Tauc: "+layer.name+" is not a layer!")
    check_kwargs(kwargs,["reg_range","reg_points","linewidth","show_reg_details","fontsize","show_eq"],"plot_bandgap_analysis")
    reg_range = kwargs.setdefault("reg_range",[])
    reg_points = kwargs.setdefault("reg_points",100)
    show_reg_details = kwargs.setdefault("show_reg_details",True)
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)
    show_eq = kwargs.setdefault("show_eq",False)
    resc_abs = (layer.alpha*layer.E)**(1.0/m)
    
    resc_dispersion = plt.figure(facecolor="white")
    resc_dispersion.suptitle("Tauc plot of "+layer.name)
    resc_abs_coeff = plt.subplot(111)
    resc_abs_coeff.plot(layer.E,resc_abs,linestyle='-',linewidth=linewidth,color="blue",label="Rescaled absorption")
    if reg_range != []:
        Emin,Emax = reg_range[0],reg_range[1]
        E_range = np.linspace(Emin,Emax,reg_points)
        resc_abs_range = sp.interpolate.interp1d(layer.E,resc_abs)(E_range)
        
        reg_result = sp.stats.linregress(E_range,resc_abs_range)
        slope = reg_result.slope
        intercept = reg_result.intercept
        rvalue = reg_result.rvalue
        Eg = -intercept/slope
        if show_eq:
            line_eq = "y=%gx-%g" %(round(slope,decimal_digits+1),round(abs(intercept),decimal_digits+1))
            line_eq += " , $R^2$ = "+str(round(rvalue,decimal_digits+1))
        else:
            line_eq = ""
        if Eg<0:
            raise ValueError("Negative optical bandgap of "+layer.name+"! Check your dispersion!")
        E_extrap = np.linspace(Eg*0.99,E_range[-1]*1.02,1000)
        line_extrap = E_extrap*slope + intercept
    
        resc_abs_coeff.plot(E_range,resc_abs_range,linestyle='-',linewidth=linewidth*5,color="darkorange",markersize=5.0,markevery=5,alpha=0.7,label="Regression range")
        resc_abs_coeff.plot(E_extrap,line_extrap,linestyle='--',linewidth=linewidth,color="black",label="Linear extrapolation "+line_eq)
        resc_abs_coeff.plot([Eg],[0.0],linestyle='',marker='o',markersize=linewidth*4.0,color="red")
        resc_abs_coeff.text(Eg+0.1,0.0,"$Eg$="+str(round(Eg,decimal_digits))+" eV",color="red",fontsize=fontsize)
    resc_abs_coeff.set_xlabel("$E$ $[eV]$",fontsize=fontsize)
    
    if m==0.5:
        yax_str = r"$(\alpha E)^{2} \; \left[\left(\dfrac{eV}{nm}\right)^2\right]$"
    elif m==2.0:
        yax_str = r"$(\alpha E)^{1/2} \; \left[\left(\dfrac{eV}{nm}\right)^{1/2}\right]$"
    else:
        yax_str = r"$(\alpha E)^{%g} \; \left[\left(\dfrac{eV}{nm}\right)^{%g}\right]$" %(1.0/m,1.0/m)
    resc_abs_coeff.set_ylabel(yax_str,fontsize=fontsize)

    resc_abs_coeff.grid()
    resc_abs_coeff.legend(fontsize=fontsize)
    
    if show_reg_details and reg_range != []:
        print "Tauc plot: estimated bandgap is "+str((round(Eg,decimal_digits+1)))+" eV , R^2 = "+str((round(rvalue,decimal_digits+1)))
    
    return resc_abs_coeff
    

def plot_Sigmoid(layer,**kwargs):
    ''' Plot the absorption and fit a Sigmoid function to estimate the onset '''
    check_kwargs(kwargs,["reg_range","reg_points","show_reg_range","print_report","linewidth","fontsize",
                         "saturate","saturation_peak_value","saturation_peak_range","form",
                         "amplitude","center","sigma"],"plot_Step")
    from scipy.signal import find_peaks
    reg_range = kwargs["reg_range"]
    reg_points = kwargs.setdefault("reg_points",1000)
    show_reg_range = kwargs.setdefault("show_reg_range",False)
    form = kwargs["form"] #linear, arctan, erf of logistic
    print_report = kwargs.setdefault("print_report",False)
    saturate = kwargs.setdefault("saturate",False)
    saturation_peak_value = kwargs.setdefault("saturation_peak_value",np.inf)
    saturation_peak_range = kwargs.setdefault("saturation_peak_range",[])
    if saturation_peak_value != np.inf and saturation_peak_range!= []:
        if not saturate:
            print "\nplot_Step: saturate=True must be provided to allow saturation."
        else:
            print "\nplot_Step: a saturation peak value was manually provided, ignoring the range."
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)
    
    Emin,Emax = reg_range[0],reg_range[1]
    E_range = np.linspace(Emin,Emax,reg_points)
    alpha_range = sp.interpolate.interp1d(layer.E,layer.alpha)(E_range)
    
    # If provided, saturate the peak value to easily extract the step onset.
    # To saturate, we can provide a range of a saturation peak (we take the first one).
    # Also, it's possible to manually provide a saturation value, which will override the previous option.
    if saturation_peak_value == np.inf and saturation_peak_range != []:
        saturation_peak_value_index_list = find_peaks(alpha_range)[0]
        if len(saturation_peak_value_index_list)>0:
            saturation_peak_value = alpha_range[saturation_peak_value_index_list[0]]
    if saturate and len(np.where(alpha_range>=saturation_peak_value)[0])>0:
        alpha_range[np.where(alpha_range>=saturation_peak_value)[0][0]:]=min(np.max(alpha_range),saturation_peak_value)
        
    sigmoid_mod = lmfit.models.StepModel(form=form)
    # If an initial guess was provided, use that. If not, guess one.
    if set(["amplitude","center","sigma"]).issubset(set(kwargs.keys())):
        amplitude = kwargs["amplitude"]
        center = kwargs["center"]
        sigma = kwargs["sigma"]
        params = step.make_params(amplitude=amplitude,center=center,sigma=sigma)
    else:
        params=sigmoid_mod.guess(alpha_range,x=E_range)
    res=sigmoid_mod.fit(alpha_range,params,x=E_range)
    if print_report:
        print("\n"+res.fit_report()+"\n")
    
    plt.figure(facecolor="white")
    plt.suptitle("Sigmoid fit of "+layer.name)
    sigmoid_plot = plt.subplot()
    sigmoid_plot.plot(layer.E, layer.alpha,linewidth=linewidth,color="blue")
    if show_reg_range:
        plt.plot(E_range,alpha_range,linestyle='-',linewidth=linewidth*5,color="darkorange",markersize=5.0,markevery=5,alpha=0.7,label="Regression range")
    sigmoid_plot.plot(E_range,res.best_fit,label=form+" fit",linestyle='--',color="black",linewidth=linewidth)
    sigmoid_plot.set_xlabel("$E$ $[eV]$",fontsize=fontsize)
    sigmoid_plot.set_ylabel(r"$\alpha$"+" [$nm^{-1}$]",fontsize=fontsize)
    sigmoid_plot.legend()
    sigmoid_plot.grid()
    return sigmoid_plot 


def plot_CP(layer,**kwargs):
    ''' A critical point plots, using the iflections of epsilon2 '''
    check_kwargs(kwargs,["linewidth","fontsize","CP_guesses","plot_limits","order"],"plot_inflection")
    form = kwargs.setdefault("form","epsilon_Im")
    linewidth = kwargs.setdefault("linewidth",default_linewidth)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)
    CP_guesses = kwargs.setdefault("CP_guesses",[])
    plot_limits = kwargs.setdefault("plot_limits",[])
    order = kwargs.setdefault("order",3)
    
    E = layer.E
    
    if form == "epsilon_Im":
        disp_func = -np.imag(layer.epsilon)
        func_label = r"-$\Im(\varepsilon)$"
        func_d1_label = r"-$\dfrac{d \Im(\varepsilon)}{dE} \; [eV^{-1}]$"
        func_d2_label = r"-$\dfrac{d^2 \Im(\varepsilon)}{dE^2} \; [eV^{-2}]$"
        func_d3_label = r"-$\dfrac{d^3 \Im(\varepsilon)}{dE^3} \; [eV^{-3}]$"
    elif form == "epsilon_Re":
        disp_func = np.real(layer.epsilon)
        func_label = r"$\Re(\varepsilon)$"
        func_d1_label = r"$\dfrac{d \Re(\varepsilon)}{dE} \; [eV^{-1}]$"
        func_d2_label = r"$\dfrac{d^2 \Re(\varepsilon)}{dE^2} \; [eV^{-2}]$"
        func_d3_label = r"$\dfrac{d^3 \Re(\varepsilon)}{dE^3} \; [eV^{-3}]$"
    elif form == "alpha":
        disp_func = layer.alpha
        func_label = r"$\alpha \; [nm^{-1}]$"
        func_d1_label = r"$\dfrac{d \alpha}{dE} \; [nm^{-1}eV^{-1}]$"
        func_d2_label = r"$\dfrac{d^2 \alpha}{dE^2} \; [nm^{-1}eV^{-2}]$"
        func_d3_label = r"$\dfrac{d^3 \alpha}{dE^3} \; [nm^{-1}eV^{-3}]$"
        
    disp_func_d1 = np.gradient(disp_func,E)
    disp_func_d2 = np.gradient(disp_func_d1,E)
    disp_func_d3 = np.gradient(disp_func_d2,E)
    
    plt.figure(facecolor="white")
    plt.suptitle("Critical point analysis of "+layer.name)
    disp_funcplot = plt.subplot(411)
    disp_funcplot.plot(E,disp_func,color='black')
    disp_funcplot.set_ylabel(func_label,fontsize=fontsize)
    disp_funcplot.grid()
    
    disp_func_d1plot = plt.subplot(412)
    disp_func_d1plot.plot(E,disp_func_d1,color='black')
    disp_func_d1plot.set_ylabel(func_d1_label,fontsize=fontsize)
    disp_func_d1plot.grid()
    
    disp_func_d2plot = plt.subplot(413)
    disp_func_d2plot.plot(E,disp_func_d2,color='black')
    disp_func_d2plot.set_ylabel(func_d2_label,fontsize=fontsize)
    disp_func_d2plot.grid()
    
    disp_func_d3plot = plt.subplot(414)
    disp_func_d3plot.plot(E,disp_func_d3,color='black')
    disp_func_d3plot.set_ylabel(func_d3_label,fontsize=fontsize)
    disp_func_d3plot.set_xlabel("$E$ $[eV]$",fontsize=fontsize)
    disp_func_d3plot.grid()
    
    if CP_guesses != {}:
        disp_func_intep = interp1d(E,disp_func)
        disp_func_d1_interp = interp1d(E,disp_func_d1)
        disp_func_d2_interp = interp1d(E,disp_func_d2)
        disp_func_d3_interp = interp1d(E,disp_func_d3)
        
        CP_string = "Critical points:\n"
        for i,order in enumerate(CP_guesses):
            for guess in CP_guesses[order]:
                if order == 1:
                    root = sp.optimize.root(disp_func_d1_interp,guess)
                elif order == 2:
                    root = sp.optimize.root(disp_func_d2_interp,guess)
                elif order == 3:
                    root = sp.optimize.root(disp_func_d3_interp,guess)
                CP_string += "order " + str(order) + ": " + str(round(root.x[0],3))+"eV\n"

                disp_funcplot.plot(root.x[0],disp_func_intep(root.x[0]),marker='o',color=line_color_list[i+1],markersize=linewidth*4.0)
                disp_func_d1plot.plot(root.x[0],disp_func_d1_interp(root.x[0]),marker='o',color=line_color_list[i+1],markersize=linewidth*4.0)
                disp_func_d2plot.plot(root.x[0],disp_func_d2_interp(root.x[0]),marker='o',color=line_color_list[i+1],markersize=linewidth*4.0)
                disp_func_d3plot.plot(root.x[0],disp_func_d3_interp(root.x[0]),marker='o',color=line_color_list[i+1],markersize=linewidth*4.0)
    
    if plot_limits != []:
        left = plot_limits[0]
        right = plot_limits[1]
        disp_funcplot.set_xlim(left,right)
        disp_func_d1plot.set_xlim(left,right)
        disp_func_d2plot.set_xlim(left,right)
        disp_func_d3plot.set_xlim(left,right)
    print CP_string
    return (disp_funcplot,disp_func_d1plot,disp_func_d2plot,disp_func_d3plot)

def plot_brute(model,**kwargs):
    ''' Prints the brute-force search grid of FOM. '''
    check_kwargs(kwargs,["surftype","alpha","mark_min"],"plot_brute")
    surftype = kwargs.setdefault("surftype","surface")
    alpha = kwargs.setdefault("alpha",0.9)
    mark_min = kwargs.setdefault("mark_min",True)
    if model.fit_method != "brute":
        print "Cannot print the searching grid: fit method is not brute!"
    else:
        if len(np.shape(model.res.brute_grid)) == 1:
            plt.figure()
            plt.plot(model.res.brute_grid,np.sqrt(model.res.brute_Jout))
            plt.plot(model.res.params.values()[0].value,model.FOM,linestyle='',marker='o',color='red',markersize=5)
            plt.xlabel(model.params.keys()[0])
            plt.ylabel("FOM")
            plt.grid()
        elif np.shape(model.res.brute_grid)[0] == 2:
            from mpl_toolkits.mplot3d import Axes3D
            from matplotlib import cm
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            if surftype == "surface":
                ax.plot_surface(model.res.brute_grid[0],model.res.brute_grid[1],np.sqrt(model.res.brute_Jout),cmap=cm.Spectral, alpha=alpha)
            elif surftype == "wireframe":
                ax.plot_wireframe(model.res.brute_grid[0],model.res.brute_grid[1],np.sqrt(model.res.brute_Jout))
            if mark_min:
                ax.scatter(model.res.params.values()[0].value,model.res.params.values()[1].value,model.FOM,s=50,c='red')
            ax.set_xlabel(model.res.var_names[0])
            ax.set_ylabel(model.res.var_names[1])
            ax.set_zlabel("FOM")
            
        else:
            print "Cannot print the searching grid: only possible for 1 or 2 variables, here there are "+str(np.shape(model.res.brute_grid)[0])+"!"


def add_data_to_plot(plot,filename,**kwargs):
    ''' Adds data to plot. The datafile must be in the structure of x|y1|y2|y3 and so on... 
        To keep things simple, no line is plotted, only markers.'''
        
    check_kwargs(kwargs,["skiprows","labels","colors","markers","markevery"],"add_data_to_plot")
    skiprows = kwargs.setdefault("skiprows",0)
    labels = kwargs.setdefault("labels",[])
    colors = kwargs.setdefault("colors",[])
    markers = kwargs.setdefault("markers",[])
    markevery = kwargs.setdefault("markevery",1)
    fontsize = kwargs.setdefault("fontsize",default_fontsize)
    
    data = np.loadtxt(filename,skiprows=skiprows)
    for i in range(1,len(data[0,:])):
        if markers == []:
            marker = 'x'
        else:
            marker = markers[i-1]
        if labels != []:
            if colors != []:
                
                plot.plot(data[:,0],data[:,i],label=labels[i-1],color=colors[i-1],marker=marker,markevery=markevery,linestyle='')
            else:
                plot.plot(data[:,0],data[:,i],label=labels[i-1],marker=marker,markevery=markevery,linestyle='')
        else:
            if colors != []:
                plot.plot(data[:,0],data[:,i],color=colors[i-1],marker=marker,markevery=markevery,linestyle='')
            else:
                plot.plot(data[:,0],data[:,i],marker=marker,markevery=markevery,linestyle='')
    plot.legend(fontsize=fontsize)