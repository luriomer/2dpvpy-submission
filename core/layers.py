#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@ Omer Luria
Tel-Aviv University
luriomer@gmail.com

This file contains the layer class (defined by a meterial and a thickness) and all its internal methods.
"""
from core.importer import *
from core.dispersion_formulas import *
from core.misc import *
sys.path.insert(0,'..')


class layer_cls:
    def __init__(self,**kwargs):
        ''' Configuration parameters '''
        self.init_time = time.clock()
        self.class_name = "layer_cls"
        
        self.name = kwargs["name"]
        self.lmd = kwargs["lmd"]
        self.E = eV_nm(self.lmd)
        self.d = kwargs["d"]
        self.disp_type = kwargs["disp_type"]
        self.disp_params = kwargs["disp_params"]
        self.disp_vary = kwargs.setdefault("disp_vary",True)
        self.mu = kwargs.setdefault("mu",None)
        
        check_kwargs(kwargs,["name","lmd","d","disp_type","disp_params","disp_vary","mu"],self.name)
        
        # Input checks
        if not num_type(self.d,"real") or self.d<0:
            raise TypeError("Layer configuration is invalid: d must be a positive real number!")
        if self.d <= high_zero_threshold:
            add_message("Warning: "+self.name+"'s thickness is bellow the high zero threshold.")
        if self.disp_vary and self.disp_type == "Tab":
            add_message("Warning: "+self.name+"'s dispersion is Tabulated but defined as tunable. This will result is repeated unnecessary data loadings.")

        
        # Handling initial dispersion. For each disp_type, disp_params should be different:

        elif self.disp_type == "EMA General":
            self.components = self.disp_params
            if abs(sum(self.components.values())-1.0) > high_zero_threshold:
                raise ValueError(self.name+": sum of fs must be unity!")
            
            for component in self.components:
                if hasattr(self,"f_"+component.name):
                    raise NameError("Duplicate components in "+self.name)
                setattr(self , "f_"+component.name , self.components[component])
                if getattr(self,"f_"+component.name)>1.0 or getattr(self,"f_"+component.name) < 0.0:
                    raise ValueError(self.name+": fs must be in the range [0,1]!")
                if (not self.disp_vary) and (component.disp_vary):
                    add_message("Warning: "+self.name+" has a fixed dispersion but depends on another fitted dispersion.")
            if self.mu is None:
                raise ValueError(self.name+" has a general EMA dispersion but mu is not defined!")
            if self.mu>1.0 or self.mu<0.0:
                raise ValueError(self.name+": mu must be in the range [0,1]!")
        
        elif self.disp_type == "EMA Bruggeman":
            # EMA General - disp_params is {component:fraction}
            self.components = self.disp_params
            f_sum = 0
            
            if len(self.components)>2:
                raise ValueError("Cannot explicitly estimate Bruggeman EMA for non-binary composite layers!")
            
            for component in self.components:
                if hasattr(self,"f_"+component.name):
                    raise NameError("Duplicate components in "+self.name)
                setattr(self , "f_"+component.name , self.components[component])
                if getattr(self,"f_"+component.name)>1.0 or getattr(self,"f_"+component.name)<0.0:
                    raise ValueError(self.name+": fs must be in the range [0,1]!")
                f_sum += self.components[component]
                if (not self.disp_vary) and (component.disp_vary):
                    raise ValueError(self.name+" has a fixed dispersion but depends on another fitted dispersion!")
            if abs(f_sum-1.0) > high_zero_threshold:
                raise ValueError("Sum of fs must be unity!")

        
        elif self.disp_type == "Sum":
            # In Sum, disp_params is a dictionary of terms, in the form of {disp_form:argument}.
            self.epsilon_comp = {}
            for key in self.disp_params:
                disp_form = key.split("_")[0]
                if disp_form == "Tab":
                    if type(self.disp_params[key]) != str:
                        raise TypeError("Tabulated dispersion requires a path string!")
                    self.Tab_file = self.disp_params[key]
                elif disp_form == "Baseline":
                    if type(self.disp_params[key]) not in (int,float,complex,np.float64):
                        raise TypeError("Baseline constant dispersion requires a single numerical value!")
                    self.Baseline_Re = np.real(self.disp_params[key])
                    self.Baseline_Im = -np.imag(self.disp_params[key])
                elif disp_form == "Cauchy":
                    if type(self.disp_params[key]) not in (list,tuple):
                        raise TypeError("Cauchy dispersion requires a list\tuple of coefficients!")
                    self.Cauchy_names = []
                    for i,Cauchy_coeff in enumerate(self.disp_params[key]):
                        setattr(self,key+"_B_"+str(2*(i+1)),Cauchy_coeff)
                        self.Cauchy_names.append(key+"_B_"+str(2*(i+1)))
                elif disp_form == "Sellmeier":
                    if type(self.disp_params[key]) not in (list,tuple):
                        raise TypeError("Sellmeier dispersion requires a list\tuple of C|B !")
                    setattr(self,key+"_C",self.disp_params[key][0])
                    setattr(self,key+"_B",self.disp_params[key][1])
                elif disp_form in ("Lorentz"):
                    if type(self.disp_params[key]) not in (list,tuple):
                        raise TypeError("Lorentz and HOA dispersions require a list\tuple of C|A|G !")
                    setattr(self,key+"_C",self.disp_params[key][0])
                    setattr(self,key+"_A",self.disp_params[key][1])
                    setattr(self,key+"_G",self.disp_params[key][2])
                elif disp_form == "Drude":
                    if type(self.disp_params[key]) not in (list,tuple):
                        raise TypeError("Drude dispersion requires with a list\tuple of rho_opt|tau !")
                    setattr(self,key+"_A",self.disp_params[key][0])
                    setattr(self,key+"_G",self.disp_params[key][1])
                elif disp_form == "CP":
                    if type(self.disp_params[key]) not in (list,tuple):
                        raise TypeError("CP dispersion requires with a list\tuple of C|A|G|P|n !")
                    setattr(self,key+"_C",self.disp_params[key][0])
                    setattr(self,key+"_A",self.disp_params[key][1])
                    setattr(self,key+"_G",self.disp_params[key][2])
                    setattr(self,key+"_P",self.disp_params[key][3])
                    setattr(self,key+"_n",self.disp_params[key][4])
        else:
            raise NameError("Invalid dispersion type for "+self.name)
                
        #In all cases, dispersion is calculated initially
        self.calc_dispersion()
        
    def calc_dispersion(self):
        ''' A dispersion calculation method that calls the apropriate functional method '''
        # First we reset the dispersion, to not carry around the previous values
        self.epsilon = np.zeros_like(self.lmd,dtype=complex)
        
        if self.disp_type == "Sum":
            for key in self.disp_params:
                disp_form = key.split("_")[0]
                if disp_form == "Tab":
                    self.epsilon_comp[key] = Tab_disp(self.lmd,self.Tab_file)
                elif disp_form == "Baseline":
                    self.epsilon_comp[key] = np.full_like(self.lmd,self.Baseline_Re-1j*self.Baseline_Im,dtype=complex)
                elif disp_form == "Cauchy":
                    Cauchy_coeff = []
                    # In Cauchy, we first need to pack-unpack the updated values
                    for Cauchy_name in self.Cauchy_names:
                        Cauchy_coeff.append(getattr(self,Cauchy_name))
                    self.epsilon_comp[key] = Cauchy_disp(self.lmd,*Cauchy_coeff)
                elif disp_form == "Sellmeier":
                    self.epsilon_comp[key] = Sellmeier_disp(self.lmd,getattr(self,key+"_C"),getattr(self,key+"_B"))
                elif disp_form == "Lorentz":
                    self.epsilon_comp[key] = Lorentz_disp(self.E,getattr(self,key+"_C"),getattr(self,key+"_A"),getattr(self,key+"_G"))
                elif disp_form == "Drude":
                    self.epsilon_comp[key] = Drude_disp(self.E,getattr(self,key+"_A"),getattr(self,key+"_G"))
                elif disp_form == "CP":
                    self.epsilon_comp[key] = CP_disp(self.E,getattr(self,key+"_C"),getattr(self,key+"_A"),getattr(self,key+"_G"),getattr(self,key+"_P"),getattr(self,key+"_n"))
                else:
                    raise NameError("Invalid dispersion formula for "+self.name)
                self.epsilon += self.epsilon_comp[key]
                
        elif self.disp_type == "EMA Bruggeman":
            self.epsilon = self.calc_EMA_disp("Bruggeman")
        elif self.disp_type == "EMA General":
            self.epsilon = self.calc_EMA_disp("General")
        else:
            raise NameError("Invalid dispersion type for "+self.name)
        
        self.N = epsilon_to_N(self.epsilon)
        self.alpha = 4*np.pi*-np.imag(self.N)/self.lmd
        
        # Raise error if the dispersion contains negative values (i.e positive imaginary values)
        if any(-np.imag(self.N)<0) or any(-np.imag(self.epsilon)<0) or any(self.alpha<0):
            raise ValueError("Invalid dispersion for "+self.name+": negative absorption values!")

    def calc_EMA_disp(self,EMA_type):
        ''' Effective Medium Dispersion (volume fractions). 
            EMA_type can be either Bruggeman or General. '''
            
        if EMA_type == "Bruggeman":
            # We have a closed form of the Bruggeman EMA only for binary composite layers.
            # the order does not matter, so we take .keys() and just pick them up.
            f1 = getattr(self,"f_"+self.components.keys()[0].name)
            f2 = getattr(self,"f_"+self.components.keys()[1].name)
            if abs(f1+f2 - 1) > high_zero_threshold:
                add_message("Warning: "+self.name+"f's sum is not unity - check your result!")
            epsil1 = self.components.keys()[0].epsilon
            epsil2 = self.components.keys()[1].epsilon
            b = (2*f1-f2)*epsil1 + (2*f2-f1)*epsil2
            return (b+np.sqrt(8*epsil1*epsil2+b**2))/4
            
            
        elif EMA_type == "General":
            # A superposition of series and parallel EMA models, using a screening weight mu
            epsil_Parallel = 0
            epsil_Series_T = 0
            f_sum = 0
            for component in self.components:
                f = getattr(self,"f_"+component.name)
                f_sum += f
                epsil_Parallel += f*component.epsilon
                epsil_Series_T += f*component.epsilon**(-1)
            if abs(f_sum-1) > high_zero_threshold:
                add_message("Warning: "+self.name+"f's sum is not unity - check your result!")
            epsil_Series = epsil_Series_T**(-1)
            return self.mu*epsil_Parallel + (1-self.mu)*epsil_Series